//Dashboard Start

$(document).ready(function () {
    //var apiSite = "10.0.100.192:8309";
    var apiSite = "localhost:62356";

    //Appending Header
    $.get("../ba/header", function (data) {
        $("#header_bg").html(data);
    });

    var index = 10;
    //Starting Dummy Loop
    
    if ($(".generate_invoice").length) {
        $(".loading-bg").fadeIn("fast");
        $.ajax({
            dataType: 'json',
            method: "GET",
            url: "http://" + apiSite + "/api/Report",
            success: populateBatch
        });

        var noExpand = "&nbsp;&nbsp;&nbsp;&nbsp;";
        var subchild = "&nbsp;&nbsp;- @ClientName";
        var subsubchild = "&nbsp;&nbsp;-&nbsp;&nbsp;- @ClientName";

        function populateBatch(data) {

            var invoiceRow = [];
            $.each(data, function (index, value) {
                var jsonObj = data[index];
                if (jsonObj.BatchStatus == "Reviewing") {
                    var newModal = $(".modal.template").clone();
                    $(newModal).removeClass("template");
                    $(newModal).attr("id", "invoice-no-" + jsonObj.BatchID);

                    var newBatch = $(newModal).find(".dashboard-group.template").clone();
                    $(newBatch).removeClass("template");
                    $(newBatch).find(".batchID").html(jsonObj.BatchID);

                    var fromDate = formatDateString(jsonObj.FromDate);
                    var toDate = formatDateString(jsonObj.ToDate);

                    $(newBatch).find(".batchFrom").html(fromDate);
                    $(newBatch).find(".batchTo").html(toDate);


                    var rowObject = {
                        id: jsonObj.BatchID,
                        totalad: 0,
                        totalman: 0,
                        totalexp: 0,
                        totalcharge: 0,
                        start: fromDate,
                        end: toDate
                    };

                    var totalAD = 0;
                    var index = 0;

                    $.each(jsonObj.TempInvoiceDetailList, function (ind, invoice) {

                        var toQuery = "";

                        var subrow = $(newModal).find(".div-table-row.template").clone();
                        $(subrow).removeClass("template");
                        totalAD += parseFloat(invoice.Amount);
                        if (invoice.BillToIDType == "A" || invoice.BillToIDType == "B") {
                            var mainRow = $(newModal).find(".withexpand.template").clone();
                            $(mainRow).removeClass("template");
                            $(mainRow).addClass("parent-row");

                            $(mainRow).find(".view-detail-client").attr("row-type", invoice.BillToIDType);
                            $(mainRow).find(".view-detail-client").attr("parent-type", invoice.BillToIDType);
                            if (invoice.BillToIDType == "A") {
                                $(mainRow).addClass("alliance-parent-" + invoice.AllianceID)
                                $(mainRow).find(".clientName").addClass("alliance-class");
                                $(mainRow).find(".clientName").html(invoice.AllianceName);
                                $(mainRow).find(".view-detail-client").attr("data-target", ".child-row-" + invoice.AllianceID);
                                $(mainRow).find(".view-detail-client").attr("clientID", invoice.AllianceID);
                                $(mainRow).find(".view-detail-client").attr("batchID", jsonObj.BatchID);
                                $(mainRow).find(".client-detail").attr("clientID", invoice.AllianceID);
                                $(mainRow).find(".client-detail .detailInvoice").removeClass("template");

                                toQuery = "alliance-parent-" + invoice.AllianceID;
                            } else {
                                $(mainRow).find(".clientName").html(invoice.BillingClientName);
                                $(mainRow).addClass("billing-parent-" + invoice.BillingClientID)
                                $(mainRow).find(".clientName").addClass("billing-class");
                                $(mainRow).find(".view-detail-client").attr("data-target", ".child-billing-row-" + invoice.BillingClientID);
                                $(mainRow).find(".view-detail-client").attr("clientID", invoice.BillingClientID);
                                $(mainRow).find(".view-detail-client").attr("batchID", jsonObj.BatchID);
                                $(mainRow).find(".client-detail").attr("clientID", invoice.BillingClientID);
                                $(mainRow).find(".client-detail .detailInvoice").removeClass("template");
                                if (invoice.AllianceName != null) {
                                    $(mainRow).find(".client-detail .combineInvoice").removeClass("template");
                                    updateBreakCombine(mainRow, invoice.BillingClientID, "B", jsonObj.BatchID, true);
                                }
                                toQuery = "billing-parent-" + invoice.BillingClientID;

                            }

                            $(mainRow).find(".ad-price").html(addCommas(invoice.Amount));
                            $(mainRow).find(".exp-price").html(addCommas(invoice.Amount));


                            $(subrow).append(mainRow);


                        } else {
                            var mainRow = $(newModal).find(".noexpand.template").clone();
                            $(mainRow).find(".clientName").addClass("market-class");
                            $(mainRow).removeClass("template");
                            $(mainRow).addClass("parent-row");
                            $(mainRow).find(".clientName").html(noExpand + invoice.MarketClientName);
                            $(mainRow).find(".view-detail-client").attr("data-target", ".child-row-" + invoice.MarketClientID);
                            $(mainRow).find(".view-detail-client").attr("clientID", invoice.MarketClientID);
                            $(mainRow).find(".view-detail-client").attr("batchID", jsonObj.BatchID);
                            $(mainRow).find(".client-detail").attr("clientID", invoice.MarketClientID);
                            $(mainRow).find(".client-detail .combineInvoice").removeClass("template");
                            $(mainRow).find(".client-detail .detailInvoice").removeClass("template");

                            $(mainRow).find(".ad-price").html(addCommas(invoice.Amount));
                            $(mainRow).find(".exp-price").html(addCommas(invoice.Amount));
                            $(subrow).append(mainRow);

                            updateBreakCombine(mainRow, invoice.MarketClientID, "M", jsonObj.BatchID, true);

                            //updateViewDetailBtn(mainRow);
                        }

                        newBatch.find(".div-table").append(subrow);
                        updateViewDetailBtn(toQuery);
                    });

                    rowObject.totalad = totalAD;
                    rowObject.totalexp = totalAD;

                    invoiceRow.push(rowObject);

                    $(newModal).append(newBatch);

                    $("#content").find("#modal-group").append(newModal);
                }

            });

            if (invoiceRow.length > 0) {
                $.each(invoiceRow, function (index, value) {
                    var row = invoiceRow[index];

                    var toClone = $(".invoice-list-row.template").clone();
                    toClone.removeClass("template");
                    toClone.attr("data-target", "#invoice-no-" + row.id);
                    var tempDel = "<a class='deleteInvoice' invoiceID=@invoiceID hred='#'><i class='fa fa-times'></i></a>"
                    $(toClone).find(".batchno").html(row.id + tempDel.replace("@invoiceID", row.id));
                    $(toClone).find(".batchtotalexp").html(addCommas(row.totalexp));
                    $(toClone).find(".batchtotalcharge").html(addCommas(row.totalcharge));
                    $(toClone).find(".batchfrom").html(row.start);
                    $(toClone).find(".batchto").html(row.end);

                    $("#left-content > table > tbody").append(toClone);
                });
            } else {
                $("#left-content > table > tbody").append("<tr><td colspan=5>No Invoice Available</td></tr>");
            }

            $(".loading-bg").fadeOut("fast");

            //Expand Btn Handler
        }

        function updateViewDetailBtn(current) {
            if (current.length > 1) {
                console.log($("." + current).find(".view-detail-client"));
                $("." + current).find(".view-detail-client").on("click", function (e) {
                    e.preventDefault();
                    var typeRow = $(this).attr("row-type");
                    var parentType = $(this).attr("parent-type");
                    var clientID = $(this).attr("clientid");
                    var batchID = $(this).attr("batchID");
                    var isShow = $(this).attr("isShow");
                    var isLoaded = $(this).attr("isLoaded");
                    var tempPar = $(this).parent().parent().parent();
                    $(this).attr("aria-expanded", true);
                    if (isShow == "false" && isLoaded != "Loaded") {
                        $(".loading-bg").fadeIn("fast");
                        $(this).attr("isLoaded", "Loaded");
                        $.ajax({
                            dataType: 'json',
                            method: "GET",
                            parentField: $(this).parent().parent().parent(),
                            currentField: $(this),
                            parentID: clientID,
                            typeRow: typeRow,
                            parentType: parentType,
                            data: {
                                batchID: batchID,
                                clientID: clientID,
                                type: typeRow,
                                parenttype: parentType
                            },
                            url: "http://" + apiSite + "/api/Report",
                            success: function (data) {
                                if (this.typeRow == "A") {
                                    populateAllianceChild(data, this.currentField, this.parentField, this.parentID);
                                } else {
                                    populateBillingChild(data, this.currentField, this.parentField, this.parentID);
                                }
                            }
                        });
                        updateExpand(isShow, "." + current, tempPar);
                    } else
                        updateExpand(isShow, "." + current, tempPar);
                });
            }
        }

        function populateAllianceChild(data, current, parent, allianceID) {
            var isShow = $(current).attr("isShow");
            var tempPar = $(current).parent().parent().parent();

            var count = data.length;
            var breakable = false;
            if (count > 1) {
                breakable = true;
            }

            $.each(data, function (ind, val) {
                var mainRow = $(tempPar).find(".withexpand.template").clone();
                $(mainRow).removeClass("template");
                $(mainRow).attr("aria-expanded", true);
                $(mainRow).addClass("sub-table-row");
                $(mainRow).addClass("collapse");
                $(mainRow).addClass("billingclient-" + val.BillingClientID);
                $(mainRow).addClass("in");
                $(mainRow).addClass("child-row-" + allianceID)

                $(mainRow).find(".view-detail-client").attr("row-type", "B");
                $(mainRow).find(".view-detail-client").attr("parent-type", "A");
                $(mainRow).find(".clientName").html(noExpand + val.BillingClientName);
                $(mainRow).find(".view-detail-client").attr("data-target", ".child-billing-row-" + val.BillingClientID);
                $(mainRow).find(".view-detail-client").attr("clientID", val.BillingClientID);
                $(mainRow).find(".view-detail-client").attr("batchID", val.InvoiceBatchID);
                $(mainRow).find(".client-detail").attr("clientID", val.BillingClientID);
                if (breakable == true)
                    $(mainRow).find(".client-detail .separateInvoice").removeClass("template");

                $(mainRow).find(".ad-price").html(addCommas(val.Amount));
                $(mainRow).find(".exp-price").html(addCommas(val.Amount));
                //$(mainRow).appendTo(parent);
                $(tempPar).append(mainRow);

                updateBreakCombine(mainRow, val.BillingClientID, "B", val.InvoiceBatchID, false);

                $("." + "billingclient-" + val.BillingClientID).find(".view-detail-client").on("click", function (e) {
                    e.preventDefault();
                    var typeRow = $(this).attr("row-type");
                    var parentType = $(this).attr("parent-type");
                    var clientID = $(this).attr("clientid");
                    var batchID = $(this).attr("batchID");
                    var isShow = $(this).attr("isShow");
                    var isLoaded = $(this).attr("isLoaded");
                    var tempPar = $(this).parent().parent().parent();
                    if (isShow == "false" && isLoaded != "Loaded") {
                        $(".loading-bg").fadeIn("fast");
                        $(this).attr("isLoaded", "Loaded");
                        $.ajax({
                            dataType: 'json',
                            method: "GET",
                            parentField: $(this).parent().parent().parent(),
                            currentField: $(this),
                            parentID: clientID,
                            typeRow: typeRow,
                            parentType: parentType,
                            data: {
                                batchID: batchID,
                                clientID: clientID,
                                type: typeRow,
                                parenttype: parentType
                            },
                            url: "http://" + apiSite + "/api/Report",
                            success: function (data) {
                                populateBillingChild(data, this.currentField, this.parentField, this.parentID);
                            }
                        });
                    }

                    if (isShow == "false") {
                        $(current).find("i").removeClass("fa-plus-square");
                        $(current).find("i").addClass("fa-minus-square");
                        $(current).attr("isShow", "true");
                        console.log("Opening")
                            //$(".loading-bg").fadeIn("fast");
                    } else {
                        console.log("Closing")
                        $(current).find("i").removeClass("fa-minus-square");
                        $(current).find("i").addClass("fa-plus-square");
                        $(current).attr("isShow", "false");
                    }
                });
            });


            $(".loading-bg").fadeOut("fast");
        }

        function populateBillingChild(data, current, parent, billingID) {
            var isShow = $(current).attr("isShow");
            var tempPar = $(current).parent().parent().parent();
            //console.log(tempPar);
            var count = data.length;
            var breakable = false;
            if (count > 1) {
                breakable = true;
            }

            $.each(data, function (ind, val) {
                var mainRow = $(tempPar).find(".noexpand.template").clone();
                $(mainRow).removeClass("template");
                $(mainRow).attr("aria-expanded", true);
                $(mainRow).addClass("sub-sub-table-row");
                $(mainRow).addClass("collapse");
                $(mainRow).addClass("in");
                $(mainRow).addClass("child-billing-row-" + billingID);
                $(mainRow).addClass("marketclient-" + val.MarketClientID);
                $(mainRow).find(".client-detail").attr("parent-type", "B");
                $(mainRow).find(".client-detail").attr("row-type", "M");
                $(mainRow).find(".clientName").html(noExpand + noExpand + val.MarketClientName);
                $(mainRow).find(".client-detail").attr("clientID", val.MarketClientID);
                if (breakable == true)
                    $(mainRow).find(".client-detail .separateInvoice").removeClass("template");

                $(mainRow).find(".ad-price").html(addCommas(val.Amount));
                $(mainRow).find(".exp-price").html(addCommas(val.Amount));

                var toAppend = "billingclient-" + billingID;
                var whatAppend = "marketclient-" + val.MarketClientID;
                $(tempPar).append(mainRow);
                $("." + whatAppend).insertAfter("." + toAppend);

                updateBreakCombine("." + whatAppend, val.MarketClientID, "M", val.InvoiceBatchID, false);

                $("." + toAppend).find(".view-detail-client").on("click", function (e) {
                    var isShow = $(this).attr("isShow");
                    e.preventDefault();
                    if (isShow == "false") {
                        $(current).find("i").removeClass("fa-plus-square");
                        $(current).find("i").addClass("fa-minus-square");
                        $(current).attr("isShow", "true");
                        console.log("Opening")
                            //$(".loading-bg").fadeIn("fast");
                    } else {
                        console.log("Closing")
                        $(current).find("i").removeClass("fa-minus-square");
                        $(current).find("i").addClass("fa-plus-square");
                        $(current).attr("isShow", "false");
                    }
                });
            });




            

            $(".loading-bg").fadeOut("fast");
        }

        function updateExpand(isShow, current, parent) {
            if (isShow == "false") {
                $(current).find("i").removeClass("fa-plus-square");
                $(current).find("i").addClass("fa-minus-square");
                $(current).attr("isShow", "true");
                //$(".loading-bg").fadeIn("fast");
            } else {
                var classToggle = $(current).attr("data-target");
                $.each($.find(classToggle), function (index, value) {
                    $.each($(value).find(".view-detail-client"), function (index2, value2) {
                        if ($(value2).attr("isShow") != "false") {
                            var selector = $(value2).attr("data-target");
                            $.each($(parent).find(selector), function (index3, value3) {
                                $(value3).collapse('hide');
                            })
                            $(value2).attr("isShow", "false");
                            $(value2).find("i").removeClass("fa-minus-square");
                            $(value2).find("i").addClass("fa-plus-square");
                        }
                    })
                });
                $(current).find("i").removeClass("fa-minus-square");
                $(current).find("i").addClass("fa-plus-square");
                $(current).attr("isShow", "false");
            }

            $(".collapse").on("hidden.bs.collapse", function () {
                $(".loading-bg").fadeOut("fast");
            })

            $(".collapse").on("shown.bs.collapse", function () {
                $(".loading-bg").fadeOut("fast");
            })

            $(".loading-bg").fadeOut("fast");
        }

        function updateBreakCombine(current, clientID, rowtype, batchID, isCombine) {
            if (isCombine == false) {
                $(current).find(".separateInvoice").on("click", function () {
                    var InvoiceObj = {
                        batchID: batchID,
                        clientID: clientID,
                        type: rowtype,
                        isCombine: false
                    };

                    console.log(InvoiceObj);

                    $.ajax({
                        dataType: 'json',
                        type: "POST",
                        url: "http://" + apiSite + "/api/Report",
                        data: InvoiceObj,
                        batchID: batchID,
                        highlight: clientID,
                        highlighttype: rowtype,
                        success: function (data) {
                            console.log(data);
                            refreshInvoiceBatch(this.batchID, this.highlight, this.highlighttype);
                        }
                    });
                })
            } else {
                $(current).find(".combineInvoice").on("click", function () {
                    var InvoiceObj = {
                        batchID: batchID,
                        clientID: clientID,
                        type: rowtype,
                        isCombine: true
                    };

                    console.log(InvoiceObj);

                    $.ajax({
                        dataType: 'json',
                        type: "POST",
                        url: "http://" + apiSite + "/api/Report",
                        data: InvoiceObj,
                        batchID: batchID,
                        highlight: clientID,
                        highlighttype: rowtype,
                        success: function (data) {
                            console.log(data);
                            refreshInvoiceBatch(this.batchID, this.highlight, this.highlighttype);
                        }
                    });
                })
            }
        }

        function refreshInvoiceBatch(batchID, highlight, highlighttype) {
            $(".loading-bg").fadeIn("fast");
            $.ajax({
                dataType: 'json',
                method: "GET",
                highlight: highlight,
                highlighttype: highlighttype,
                data: {
                    batchID: batchID
                },
                url: "http://" + apiSite + "/api/Report",
                success: function (data) {
                    createInvoiceContent(data, this.highlight, this.highlighttype);
                }
            });
        }

        function createInvoiceContent(jsonObj, highlight, highlighttype) {
            var noExpand = "&nbsp;&nbsp;&nbsp;&nbsp;";
            var subchild = "&nbsp;&nbsp;- @ClientName";
            var subsubchild = "&nbsp;&nbsp;-&nbsp;&nbsp;- @ClientName";

            var batchID = jsonObj.BatchID;

            var newModal = $("#invoice-no-" + batchID);
            $(newModal).find(".dashboard-group")[1].remove()

            if (jsonObj.BatchStatus == "Reviewing") {
                var newBatch = $(newModal).find(".dashboard-group.template").clone();
                $(newBatch).removeClass("template");
                $(newBatch).find(".batchID").html(batchID);
                console.log(newBatch);
                var fromDate = formatDateString(jsonObj.FromDate);
                var toDate = formatDateString(jsonObj.ToDate);

                $(newBatch).find(".batchFrom").html(fromDate);
                $(newBatch).find(".batchTo").html(toDate);

                var totalAD = 0;

                $.each(jsonObj.TempInvoiceDetailList, function (ind, invoice) {
                    var subrow = $(newBatch).find(".div-table-row.template").clone();
                    $(subrow).removeClass("template");

                    totalAD += parseFloat(invoice.Amount);

                    if (invoice.BillToIDType == "A" || invoice.BillToIDType == "B") {
                        var mainRow = $(subrow).find(".withexpand.template").clone();
                        $(mainRow).removeClass("template");
                        $(mainRow).addClass("parent-row");
                        $(mainRow).find(".view-detail-client").attr("row-type", invoice.BillToIDType);
                        $(mainRow).find(".view-detail-client").attr("parent-type", invoice.BillToIDType);
                        if (invoice.BillToIDType == "A") {
                            $(mainRow).find(".clientName").addClass("alliance-class");
                            $(mainRow).find(".clientName").html(invoice.AllianceName);
                            $(mainRow).find(".view-detail-client").attr("data-target", ".child-row-" + invoice.AllianceID);
                            $(mainRow).find(".view-detail-client").attr("clientID", invoice.AllianceID);
                            $(mainRow).find(".view-detail-client").attr("batchID", jsonObj.BatchID);
                            $(mainRow).find(".client-detail").attr("clientID", invoice.AllianceID);
                            $(mainRow).find(".client-detail .detailInvoice").removeClass("template");
                        } else {
                            if (highlight == invoice.BillingClientID)
                                $(mainRow).addClass("highlight-class");
                            $(mainRow).find(".clientName").html(invoice.BillingClientName);
                            $(mainRow).find(".clientName").addClass("billing-class");
                            $(mainRow).find(".view-detail-client").attr("data-target", ".child-billing-row-" + invoice.BillingClientID);
                            $(mainRow).find(".view-detail-client").attr("clientID", invoice.BillingClientID);
                            $(mainRow).find(".view-detail-client").attr("batchID", jsonObj.BatchID);
                            $(mainRow).find(".client-detail").attr("clientID", invoice.BillingClientID);
                            $(mainRow).find(".client-detail .detailInvoice").removeClass("template");
                            if (invoice.AllianceName != null) {
                                $(mainRow).find(".client-detail .combineInvoice").removeClass("template");
                                updateBreakCombine(mainRow, invoice.BillingClientID, "B", jsonObj.BatchID, true);
                            }
                        }

                        $(mainRow).find(".ad-price").html(addCommas(invoice.Amount));
                        $(mainRow).find(".exp-price").html(addCommas(invoice.Amount));


                        $(subrow).append(mainRow);
                        updateViewDetailBtn(mainRow);

                    } else {
                        var mainRow = $(subrow).find(".noexpand.template").clone();
                        if (highlight == invoice.MarketClientID)
                            $(mainRow).addClass("highlight-class");
                        $(mainRow).removeClass("template");
                        $(mainRow).addClass("parent-row");
                        $(mainRow).find(".clientName").html(noExpand + invoice.MarketClientName);
                        $(mainRow).find(".clientName").addClass("market-class");
                        $(mainRow).find(".view-detail-client").attr("data-target", ".child-row-" + invoice.MarketClientID);
                        $(mainRow).find(".view-detail-client").attr("clientID", invoice.MarketClientID);
                        $(mainRow).find(".view-detail-client").attr("batchID", jsonObj.BatchID);
                        $(mainRow).find(".client-detail").attr("clientID", invoice.MarketClientID);
                        $(mainRow).find(".client-detail .combineInvoice").removeClass("template");
                        $(mainRow).find(".client-detail .detailInvoice").removeClass("template");

                        $(mainRow).find(".ad-price").html(addCommas(invoice.Amount));
                        $(mainRow).find(".exp-price").html(addCommas(invoice.Amount));
                        $(subrow).append(mainRow);

                        updateBreakCombine(mainRow, invoice.MarketClientID, "M", jsonObj.BatchID, true);

                        updateViewDetailBtn(mainRow);
                    }

                    newBatch.find(".div-table").append(subrow);
                    $(newModal).append(newBatch);
                    $(".loading-bg").fadeOut("fast");
                });
            }
        }
    }

    var clientList = [];

    //Run Report JS Specific
    if ($(".filter-btn").length) {

        var clientTrack = [];

        var filteredItem = "<tr><td class='client-name'>@Name</td><td class='client-amount'>@Amount</td></tr>";

        //Filter Btn Onclick Event
        $(".filter-btn").on("click", function (e) {
            e.preventDefault();
            //var clientValue = $("#clientFilter").attr("clientID");
            $(".loading-bg").fadeIn("fast");
            var dateFrom = $(".dateFrom").val();
            var dateTo = $(".dateTo").val();
            $.ajax({
                dataType: 'json',
                url: "http://" + apiSite + "/api/Report",
                data: {
                    dateFrom: dateFrom,
                    dateTo: dateTo
                },
                success: populateFilterClient
            });
        })

        //Filter Action
        function populateFilterClient(data) {
            var tableDiv = $(".report-client-list > table >tbody:last-child");
            tableDiv.empty();
            if ((data.length == 0)) {
                var row = "<tr><td colspan=4 style='text-align:center'>No Invoice Found</td></tr>";
                tableDiv.append(row);
                $(".batch-generate").hide()
            } else {
                $.each(data, function (index, value) {
                    var row = filteredItem;
                    //row = row.replace("@id", value.ClientID);
                    row = row.replace("@Name", value.ClientName);
                    row = row.replace("@Amount", addCommas(value.InvoiceTotal));

                    tableDiv.append(row);
                });
                $(".batch-generate").show()
            }

            $(".report-client-list").slideDown("fast");

            $(".loading-bg").fadeOut("fast");
        }

        $(".batch-generate").on("click", function (e) {
            e.preventDefault();
            $(".loading-bg").fadeIn("fast");
            var dateFrom = $(".dateFrom").val();
            var dateTo = $(".dateTo").val();
            var dateObj = {
                dateFrom: dateFrom,
                dateTo: dateTo
            };
            $.ajax({
                dataType: 'json',
                type: "PUT",
                url: "http://" + apiSite + "/api/Report",
                data: dateObj,
                success: function () {
                    window.location.replace("index");
                }
            });

        })

        //Process Billing App Client List to ComboBox
        function baListProcess(data) {
            $.each(data, function (index, value) {
                /// do stuff
                clientList.push({
                    value: value.ClientID,
                    label: value.ClientName
                });
            });

            $("#clientFilter").autocomplete({
                source: function (request, response) {
                    var results = $.ui.autocomplete.filter(clientList, request.term);
                    if (results.length > 10) {
                        var sliced = results.slice(0, 10);
                        var leftOver = results.length - 10;
                        var lastRow = "--- There are " + leftOver + " more clients ---";
                        sliced.push(lastRow);
                        response(sliced);
                    } else {
                        response(results);
                    }
                },
                select: function (event, ui) {
                    var id = ui.item.value;
                    var name = ui.item.label;
                    $("#clientFilter").val(name);
                    $("#clientFilter").attr("clientID", id);
                    return false;
                }
            });


            $(".loading-bg").fadeOut("fast");
        }

        function populateOverlayDetail(data) {
            var adtotal = 0;


            $.each(data, function (index, value) {
                /// do stuff
                adtotal += value.ProductTotal;
            });

            $(".ad-total-rate").html(addCommas(adtotal.toFixed(2)));
            $(".ad-total-amount").html(addCommas(adtotal.toFixed(2)));
            $(".ad-total-amount").attr("value", adtotal.toFixed(2));

            calculateInvoiceTotal();
            var scrollHeight = $(document).scrollTop();
            scrollHeight += 50;
            var marginValue = scrollHeight + "px auto 0";

            $("#overlay-content").css({
                margin: marginValue
            });

            calculateManualAmount();
            $("#overlay-container").fadeIn("fast");
            $(".loading-bg").fadeOut("fast");
        }

        function updateTrackedClient() {
            var trackingvalue = $(".info-tracking").attr("currenttrack");
            var tochange = $("input[value=" + trackingvalue + "]").parent().parent().find(".client-amount");
            $(tochange).empty();
            var tosave = parseFloat($(".info-tracking").attr("tosaveamount"));
            $(tochange).html(addCommas(tosave.toFixed(2)));
            $(".info-tracking").attr("tosaveamount", 0);

            $(".template-data-row input").each(function () {
                $(this).val("");
            })

        }


        //Key up check - will modify later when query is available
        $(".manual-rate").keyup(function () {
            calculateManualAmount();
        })

        $(".manual-quantity").keyup(function () {
            calculateManualAmount();
        })

        //Re-Calculate manual amount on key up
        function calculateManualAmount() {
            $(".manual-amount").each(function (index) {
                $(this).empty();
                var quantity = $(this).parent().find(".manual-quantity > input").val();
                var rate = $(this).parent().find(".manual-rate > input").val();
                var amount = quantity * rate;
                $(this).html(addCommas(amount.toFixed(2)));
                $(this).attr("value", amount);
            })

            calculateInvoiceTotal();
        }

        //Calculate the total invoice cost on the fly - ajax
        function calculateInvoiceTotal() {
            var totalcost = 0;

            $('.row-total').each(function (i, obj) {
                //test
                var rowtotal = parseFloat($(this).attr("value"));
                totalcost += rowtotal;
            });

            $(".total-invoice-amount").html(addCommas(totalcost.toFixed(2)));
            $(".info-tracking").attr("tosaveamount", totalcost);
        }

        //Date picker filter populate
        $(function () {
            $(".dateTo").datepicker({
                dateFormat: 'mm/dd/yy',
                changeMonth: true,
                //minDate: new Date(),
                maxDate: new Date(),
                onSelect: function (date) {

                    var selectedDate = new Date(date);
                    var msecsInADay = 86400000;
                    var endDate = new Date(selectedDate.getTime() + msecsInADay);

                    $(".dateFrom").datepicker("option", "minDate", '-2y');
                    $(".dateFrom").datepicker("option", "maxDate", selectedDate);

                }
            });

            $(".dateFrom").datepicker({
                dateFormat: 'mm/dd/yy',
                changeMonth: true,
                maxDate: new Date()
            });
        });

    }


    //Data Management JS Specific
    if ($("#productType").length) {
        //Get Template
        $.get("../ba/productTypeModal", function (data) {
            $("#productType .modal-dialog").html(data);
        });

        $.get("../ba/productModal", function (data) {
            $("#product .modal-dialog").html(data);
        });

        $.get("../ba/customClientModal", function (data) {
            $("#customClient .modal-dialog").html(data);
        });

        //End Get Template

        $("#productType .save-btn").on("click", function () {
            var productType = $("#productType").val();
            var productDes = $("#productDescription").val();

            $.ajax({
                method: "PUT",
                url: "http://" + apiSite + "/api/Data",
                data: {
                    producttype: productType,
                    productdes: productDes
                },
                success: function () {
                    $(this).parent().find(".close-btn").click();
                }
            });

        })
    }

    //Format helper
    function addCommas(nStr) {
        nStr = nStr.toFixed(2);
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return "$ " + (x1 + x2);
    }

    function formatDateString(input) {
        return input.split("T")[0].replace(/(\d{4})-(\d{2})-(\d{2})/, "$2/$3/$1");
    }

})

//End Dashboard