function PopulateUploadBatchList(data) {
    $(".loading-bg h2").html("");
    $(".loading-bg .status-text").html("");
    var BatchListRow = [];
    var dataCount = 0;
    if (data.length > 0) {
        $.each(data, function (batchIndex, batchListRow) {
            //Check If Batch is Active
            if (batchListRow.BatchStatus == "UploadCheck") {
                dataCount++;
                var toClone = $(".invoice-list-row.template").clone();
                toClone.removeClass("template");
                toClone.attr("data-target", "#invoice-no-" + batchListRow.InvoiceBatchID);
                toClone.attr("batchID", batchListRow.InvoiceBatchID);
                var tempDel = "<a class='deleteInvoice' invoiceID=@invoiceID hred='#' data-placement='top' rel='confirmation' data-toggle='tooltip' title='Delete Invoice'><i class='fa fa-times'></i></a>";

                $(toClone).find(".batchno").html(batchListRow.InvoiceBatchID + tempDel.replace("@invoiceID", batchListRow.InvoiceBatchID));
                $(toClone).find(".batchtotalcharge").html(FormatAmount(batchListRow.BatchTotalAmount));
                $(toClone).find(".batchfrom").html(FormatDateString(batchListRow.BatchFromDate));
                $(toClone).find(".batchto").html(FormatDateString(batchListRow.BatchToDate));

                $("#left-content > table > tbody").append(toClone);
            }
        });
        if (dataCount == 0) {
            $("#left-content > table > tbody").append("<tr><td colspan=5>No Invoice Available</td></tr>");
        }
    } else {
        $("#left-content > table > tbody").append("<tr><td colspan=5>No Invoice Available</td></tr>");
    }

    BindDeleteBatchBtn(true);

    $(".invoice-list-row:not(.template)").on("click", function (e) {
        GenerateUploadBatchDetail($(this))
    });

    $("[rel='confirmation']").tooltip();

    TriggerLoading(false);
}

function GenerateUploadBatchDetail(batchRow) {
    var batchID = $(batchRow).attr("batchID");
    currentBatch = batchID;
    if ($("#invoice-no-" + batchID).length == 0) {
        TriggerLoading(true);
        var newModal = $(".modal.template").clone();
        $(newModal).removeClass("template");
        $(newModal).attr("id", "invoice-no-" + batchID);
        $(newModal).attr("batchid", batchID);

        $("#content").find("#modal-group").append(newModal);
        /*$("#invoice-no-" + batchID).insertBefore("#invoice-detail-modal");*/
        $.ajax({
            dataType: 'json',
            method: "GET",
            selector: "invoice-no-" + batchID,
            data: {
                batchID: batchID
            },
            url: "http://" + apiSite + "/api/File",
            success: function (data) {
                CreateUploadBatchContent(data, this.selector);
            },
            error: function (data) {
                if (data.statusText == "error") {
                    ShowError("Connection Error!");
                }
                TriggerLoading(false);
            }
        });
    }
}

function CreateUploadBatchContent(data, selector) {
    var batchModal = $("#" + selector);
    var batchContainer = $(batchModal).find(".dashboard-group.template").clone();
    $(batchContainer).removeClass("template");

    $.each(data, function (invoiceIndex, invoice) {
        var batchRow = $(batchContainer).find(".div-table-row.template").clone();
        $(batchRow).removeClass("template");

        var invoiceRow = $(batchRow).find(".withexpand.template").clone();
        $(invoiceRow).removeClass("template");
        $(invoiceRow).addClass("parent-row");
        $(invoiceRow).find(".expand-row").attr("row-type", invoice.clientType);
        $(invoiceRow).find(".expand-row").attr("isTopLevel", "true");
        if (invoice.clientType == "A") {
            $(invoiceRow).find(".clientName").addClass("alliance-class");
            $(invoiceRow).find(".clientName").html(invoice.clientName);
            $(invoiceRow).find(".expand-row").attr("data-target", ".child-alliance-row-" + invoice.clientID);
        } else if (invoice.clientType == "B") {
            $(invoiceRow).find(".clientName").addClass("billing-class");
            $(invoiceRow).find(".clientName").html(invoice.clientName);
            $(invoiceRow).find(".expand-row").attr("data-target", ".child-billing-row-" + invoice.clientID);
        } else {
            $(invoiceRow).find(".clientName").addClass("market-class");
            $(invoiceRow).find(".clientName").html(invoice.clientName);
            $(invoiceRow).find(".expand-row").attr("data-target", ".child-market-row-" + invoice.clientID);
        }
        if (invoice.isMissingData == 1)
            $(invoiceRow).find(".clientName").after("<span class='missing-data'></span>");
        if (invoice.isSecured == 1)
            $(invoiceRow).find(".clientName").after("<span class='secured-data'></span>");

        $(invoiceRow).find(".refreshFile").attr("batchID", invoice.batchID);
        $(invoiceRow).find(".refreshFile").attr("clientName", invoice.clientName);

        $(batchRow).append(invoiceRow);

        $.each(invoice.filePath, function (pathIndex, path) {
            var fileRow = $(batchRow).find(".noexpand.template").clone();
            $(fileRow).find("i").attr("batchID", invoice.batchID);
            $(fileRow).removeClass("template");
            $(fileRow).addClass("collapse");

            if (invoice.clientType == "A") {
                $(fileRow).addClass("child-alliance-row-" + invoice.clientID);
            } else if (invoice.clientType == "B") {
                $(fileRow).addClass("child-billing-row-" + invoice.clientID);
            } else {
                $(fileRow).addClass("child-market-row-" + invoice.clientID);
            }

            var anchorLink = "<a target='_blank' data-placement='right' rel='previewFile' data-toggle='tooltip' title='Download File' href='" + path + "'>" + GetFileName(path) + "</a>";
            $(fileRow).find(".fileName").html(anchorLink);
            $(batchRow).append(fileRow);
        });

        //Add Upload Button
        var uploadRow = $(batchRow).find(".noexpand.template").clone();
        $(uploadRow).removeClass("template");
        $(uploadRow).addClass("collapse");

        if (invoice.clientType == "A") {
            $(uploadRow).addClass("child-alliance-row-" + invoice.clientID);
        } else if (invoice.clientType == "B") {
            $(uploadRow).addClass("child-billing-row-" + invoice.clientID);
        } else {
            $(uploadRow).addClass("child-market-row-" + invoice.clientID);
        }

        var uploadBtn = "<form action='http://" + apiSite + "/api/File' method='post' enctype='multipart/form-data' class='js-upload-form'><div class='form-inline'><div class='form-group'><input type='file' accept='.pdf,.xls,.xlsx' name='files[]' class='js-upload-files' multiple><input name=batchID type='hidden' value=@batchID><input name=clientName type='hidden' value='@clientName'></div><button type='submit' class='btn btn-sm btn-primary js-upload-submit'>Upload Custom File</button></div></form>";

        uploadBtn = uploadBtn.replace("@batchID", invoice.batchID);
        uploadBtn = uploadBtn.replace("@clientName", invoice.clientName)
        $(uploadRow).find("i").remove();
        $(uploadRow).find(".fileName").html(uploadBtn);
        $(batchRow).append(uploadRow);
        //End Add Upload Button            

        $(invoiceRow).find(".expand-row").on("click", function (e) {
            e.preventDefault();
            UpdateExpandIcon($(this), false);
        })

        $(batchContainer).find(".div-table").append(batchRow);

    });
    $(batchModal).append(batchContainer);

    $("[rel='deleteFile']").tooltip();
    $("[rel='previewFile']").tooltip();
    $("[rel='refreshFile']").tooltip();

    BindUploadSubmitEvent();
    BindUploadDeleteEvent();
    BindRefreshFilesEvent();
    BindDeliverEvent();
    TriggerLoading(false);
}

function BindUploadDeleteEvent() {
    $(".deleteUploadFile").on("click", function (e) {
        e.preventDefault();
        TriggerLoading(true);
        var path = $(this).parent().find("span.fileName a").attr("href");
        var pathArr = path.split('/');
        var protocol = pathArr[0];
        var host = pathArr[2];
        var newPath = protocol + "//" + host + "/";
        var batchID = $(this).find("i").attr("batchID");
        var toDelete = path.replace(newPath, "");
        $.ajax({
            method: "DELETE",
            url: "http://" + apiSite + "/api/File/?filePath=" + toDelete,
            batchID: batchID,
            success: function (data) {
                if (data == "Success") {
                    $.ajax({
                        dataType: 'json',
                        method: "GET",
                        selector: "invoice-no-" + this.batchID,
                        data: {
                            batchID: this.batchID
                        },
                        url: "http://" + apiSite + "/api/File",
                        success: function (data) {
                            var modal = $("#" + this.selector);
                            $(modal).find(".dashboard-group")[1].remove();
                            CreateUploadBatchContent(data, this.selector);
                            TriggerLoading(false);
                        },
                        error: function (data) {
                            if (data.statusText == "error") {
                                ShowError("Connection Error!");
                            }
                            TriggerLoading(false);
                        }
                    });
                } else {
                    console.log(data);
                    TriggerLoading(false);
                }
            },
            error: function (data) {
                if (data.statusText == "error") {
                    ShowError("Connection Error!");
                }
                TriggerLoading(false);
            }
        });
    });
}

function BindUploadSubmitEvent() {
    $(".js-upload-form").on("submit", function (e) {
        e.preventDefault();
        var formData = new FormData($(this)[0]);

        $.ajax({
            url: $(this).attr("action"),
            type: 'POST',
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType,
            batchID: $(this).find("input[name='batchID']").val(),
            data: formData,
            beforeSend: function () {
                TriggerLoading(true);
            },
            success: function (data) {
                if (data == "Success")
                    $.ajax({
                        dataType: 'json',
                        method: "GET",
                        selector: "invoice-no-" + this.batchID,
                        data: {
                            batchID: this.batchID
                        },
                        url: "http://" + apiSite + "/api/File",
                        success: function (data) {
                            var modal = $("#" + this.selector);
                            $(modal).find(".dashboard-group")[1].remove();
                            CreateUploadBatchContent(data, this.selector);
                            TriggerLoading(false);
                        },
                        error: function (data) {
                            if (data.statusText == "error") {
                                ShowError("Connection Error!");
                            }
                            TriggerLoading(false);
                        }
                    });
                else
                    console(data);
            },
            error: function (data) {
                if (data.statusText == "error") {
                    ShowError("Connection Error!");
                }
                TriggerLoading(false);
            }
        });
    });
}

function BindRefreshFilesEvent() {
    $(".refreshFile").on("click", function (e) {
        e.preventDefault();
        var batchID = $(this).attr("batchID");
        var clientName = $(this).attr("clientName");
        $.ajax({
            url: "http://" + apiSite + "/api/File?batchID=" + batchID + "&clientName=" + clientName,
            type: 'POST',
            batchID: batchID,
            beforeSend: function () {
                TriggerLoading(true);
            },
            success: function (data) {
                if (data == "Success")
                    $.ajax({
                        dataType: 'json',
                        method: "GET",
                        selector: "invoice-no-" + this.batchID,
                        data: {
                            batchID: this.batchID
                        },
                        url: "http://" + apiSite + "/api/File",
                        success: function (data) {
                            var modal = $("#" + this.selector);
                            $(modal).find(".dashboard-group")[1].remove();
                            CreateUploadBatchContent(data, this.selector);
                            TriggerLoading(false);
                        },
                        error: function (data) {
                            if (data.statusText == "error") {
                                ShowError("Connection Error!");
                            }
                            TriggerLoading(false);
                        }
                    });
                else
                    console(data);
            },
            error: function (data) {
                if (data.statusText == "error") {
                    ShowError("Connection Error!");
                }
                TriggerLoading(false);
            }
        });
    });
}