//DashBoard

$(document).ready(function () {
    //var apiSite = "localhost:62356";
    var apiSite = "srv-web01-qa:8309";

    var NO_EXPAND = "&nbsp;&nbsp;&nbsp;&nbsp;  ";
    var FILTERED_LAYOUT = "<tr><td class='client-name'>@Name</td><td class='client-amount'>@Amount</td></tr>";
    var InvoiceNeedRefresh = false;
    var currentBatch = -1;

    var clientProfileLoaded = false;
    var billingRelationLoaded = false;

    //Appending Header
    $.get("../ba/header", function (data) {
        $("#header_bg").html(data);
        /*if (!$(".generate_invoice").length)
            $(".generate_batch_nav").remove();*/
    });

    //Pending Invoice JS Start
    if ($(".generate_invoice").length) {
        TriggerLoading(true);
        UpdateLoadingStatus("Status", "Retrieving Batch List");
        //Pulling Batch Data
        $.ajax({
            dataType: 'json',
            method: "GET",
            crossDomain: true,
            xhrFields: {
                withCredentials: true
            },
            url: "http://" + apiSite + "/api/Report",
            success: PopulateBatchList,
            error: function (data) {
                if (data.statusText == "error") {
                    ShowError("Connection Error!");
                }
                TriggerLoading(false);
            }
        });

        $.fn.bootstrapSwitch.defaults.size = 'mini';
        $("[name='EnableGrouping']").bootstrapSwitch();
        //$("[name='EnableGrouping']").bootstrapSwitch();
        $("[name='DisplayOption']").bootstrapSwitch();
        //$("[name='DisplayMarket']").bootstrapSwitch('setOnLabel','Enable');

        $(".hideInvoiceDetail").on("click", function () {
            if (InvoiceNeedRefresh == true) {
                InvoiceNeedRefresh = false;
                TriggerLoading(true);
                $.ajax({
                    dataType: 'json',
                    method: "GET",
                    crossDomain: true,
                    xhrFields: {
                        withCredentials: true
                    },
                    selector: "invoice-no-" + currentBatch,
                    data: {
                        batchID: currentBatch
                    },
                    url: "http://" + apiSite + "/api/Report",
                    success: function (data) {
                        var modal = $("#" + this.selector);
                        $(modal).find(".dashboard-group")[1].remove();
                        CreateBatchContent(data, this.selector);
                        TriggerLoading(false);
                    }
                });
            }
        });
    }
    //Pending Invoice JS End
    //Upload Start
    if ($(".upload-container").length) {
        TriggerLoading(true);
        UpdateLoadingStatus("Status", "Retrieving Checking Batch List ...")
            //Pulling Batch Data
        $.ajax({
            dataType: 'json',
            method: "GET",
            crossDomain: true,
            xhrFields: {
                withCredentials: true
            },
            url: "http://" + apiSite + "/api/File",
            success: PopulateUploadBatchList,
            error: function (data) {
                if (data.statusText == "error") {
                    ShowError("Connection Error!");
                }
                TriggerLoading(false);
            }
        });
    }
    //Upload End
    //Report JS Start
    if ($("#generate_batch").length) {
        $(".batch-generate").on("click", function (e) {
            UpdateLoadingStatus("Status", "Generating Batch Data ...");
            e.preventDefault();
            TriggerLoading(true);
            var dateFrom = $(".dateFrom").val();
            var dateTo = $(".dateTo").val();
            var dateObj = {
                dateFrom: dateFrom,
                dateTo: dateTo
            };

            $.ajax({
                dataType: 'json',
                method: "PUT",
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                url: "http://" + apiSite + "/api/Report",
                data: dateObj,
                success: function (data) {
                    if (data == "Success")
                        window.location.reload();
                    else {
                        $(".tng-alert").removeClass("template");
                        $(".tng-alert").find(".alert-msg").html(data);
                        TriggerLoading(false);
                    }
                },
                error: function (data) {
                    if (data.statusText == "error") {
                        ShowError("Connection Error!");
                    }
                    TriggerLoading(false);
                }
            });
        });

        $(function () {
            $(".dateTo").datepicker({
                dateFormat: 'mm/dd/yy',
                changeMonth: true,
                //minDate: new Date(),
                maxDate: new Date(),
                onSelect: function (date) {

                    var selectedDate = new Date(date);
                    var msecsInADay = 86400000;
                    var endDate = new Date(selectedDate.getTime() + msecsInADay);

                    $(".dateFrom").datepicker("option", "minDate", '-2y');
                    $(".dateFrom").datepicker("option", "maxDate", selectedDate);

                }
            });

            $(".dateFrom").datepicker({
                dateFormat: 'mm/dd/yy',
                changeMonth: true,
                maxDate: new Date()
            });
        });
    }

    if ($("#management-content").length) {
        TriggerLoading(true);
        UpdateLoadingStatus("Status", "Retrieving Management Data ...");

        $(function () {
            if ($("input[name=Phone]").length)
                $("input[name=Phone]").inputmask("mask", {
                    "mask": "(999)-999-9999"
                });
        });

        ReloadProfileManagement();

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var selectedTab = $(e.target).attr("name");
            TriggerLoading(true);
            switch (selectedTab) {
            case "Profile":
                /*if (clientProfileLoaded == false) {
                    clientProfileLoaded = true;
                    ReloadProfileManagement();
                } else*/
                TriggerLoading(false);
                break;
            case "Relationship":
                if (billingRelationLoaded == false) {
                    billingRelationLoaded = true;
                    GetSearchCriteria();
                } else
                    TriggerLoading(false);
                //ReloadRelationshipManagement();
                break;
            default:
                TriggerLoading(false);
                break;
            }
        });

        //Pulling Batch Data        
    }
    //Report JS End

    //PENDING INVOICE FUNCTIONS
    function PopulateBatchList(data) {
        UpdateLoadingStatus("", "");

        $(".invoice-list-row:not(.template)").each(function () {
            $(this).remove();
        });

        var BatchListRow = [];
        var dataCount = 0;
        if (data.length > 0) {
            $.each(data, function (batchIndex, batchListRow) {
                //Check If Batch is Active
                if (batchListRow.BatchStatus == "Reviewing") {
                    dataCount++;
                    var toClone = $(".invoice-list-row.template").clone();
                    toClone.removeClass("template");
                    toClone.attr("data-target", "#invoice-no-" + batchListRow.InvoiceBatchID);
                    toClone.attr("batchID", batchListRow.InvoiceBatchID);
                    var tempDel = "<a class='deleteInvoice' invoiceID=@invoiceID hred='#' data-placement='top' rel='confirmation' data-toggle='tooltip' title='Delete Invoice'><i class='fa fa-times'></i></a>";

                    $(toClone).find(".batchno").html(batchListRow.InvoiceBatchID + tempDel.replace("@invoiceID", batchListRow.InvoiceBatchID));
                    $(toClone).find(".batchtotalcharge").html(FormatAmount(batchListRow.BatchTotalAmount));
                    $(toClone).find(".batchfrom").html(FormatDateString(batchListRow.BatchFromDate));
                    $(toClone).find(".batchto").html(FormatDateString(batchListRow.BatchToDate));
                    $(toClone).find(".created").html(FormatDateString(batchListRow.CreatedDate));
                    $("#left-content > table > tbody").append(toClone);
                }
            });
            if (dataCount == 0) {
                $("#left-content > table > tbody").append("<tr><td colspan=5>No Invoice Available</td></tr>");
            }

        } else {
            $("#left-content > table > tbody").append("<tr><td colspan=5>No Invoice Available</td></tr>");
        }

        BindDeleteBatchBtn(false);
        $(".invoice-list-row:not(.template)").on("click", function (e) {
            GenerateBatchDetail($(this))
        });

        $("[rel='confirmation']").tooltip();

        TriggerLoading(false);
    }

    function BindDeleteBatchBtn(isUpload) {
        $(".invoice-list-row:not(.template)").find(".deleteInvoice").on("click", function (e) {
            var batchID = $(this).parent().parent().attr("batchid");
            var InvoiceObj = {
                batchID: batchID,
                clientID: 0,
                type: "",
                isCombine: false
            };
            var controller = "Report";
            if (isUpload == true) {
                controller = "File";
            }

            e.stopPropagation();
            BootstrapDialog.show({
                title: 'Delete Confirmation',
                message: 'Delete this Batch?',
                type: BootstrapDialog.TYPE_PRIMARY,
                buttons: [{
                    icon: 'glyphicon glyphicon-ok',
                    label: 'Delete',
                    cssClass: 'btn-success',
                    action: function (dialog) {
                        dialog.close();
                        TriggerLoading(true);

                        UpdateLoadingStatus("Status", "Deleting Batch Entry ...");

                        $.ajax({
                            method: "DELETE",
                            crossDomain: true,
                            xhrFields: {
                                withCredentials: true
                            },
                            url: "http://" + apiSite + "/api/" + controller + "/",
                            data: InvoiceObj,
                            success: function (data) {
                                if (data == "Success")
                                    location.reload();
                                else {
                                    $(".tng-alert").removeClass("template");
                                    $(".tng-alert").find(".alert-msg").html(data);
                                    TriggerLoading(false);
                                }
                            },
                            error: function (data) {
                                if (data.statusText == "error") {
                                    ShowError("Connection Error!");
                                }
                                TriggerLoading(false);
                            }
                        });
                    }
            }, {
                    icon: 'glyphicon glyphicon-remove',
                    label: 'Back',
                    action: function (dialog) {
                        dialog.close();
                    }
            }]
            });
        });
    }

    function GenerateBatchDetail(batchRow) {
        var batchID = $(batchRow).attr("batchID");
        currentBatch = batchID;
        if ($("#invoice-no-" + batchID).length == 0) {
            TriggerLoading(true);
            var newModal = $(".modal.template").clone();
            $(newModal).removeClass("template");
            $(newModal).attr("id", "invoice-no-" + batchID);
            $(newModal).attr("batchid", batchID);

            $("#content").find("#modal-group").append(newModal);
            $("#invoice-no-" + batchID).insertBefore("#invoice-detail-modal");
            $.ajax({
                dataType: 'json',
                method: "GET",
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                selector: "invoice-no-" + batchID,
                data: {
                    batchID: batchID
                },
                url: "http://" + apiSite + "/api/Report",
                success: function (data) {
                    CreateBatchContent(data, this.selector);

                    //Turn off and on to refresh background
                    $("#invoice-no-" + batchID).on("hidden.bs.modal", function () {
                        $("#invoice-no-" + batchID).modal("show");
                        $("#invoice-no-" + batchID).unbind("hidden.bs.modal");
                    });
                    $("#invoice-no-" + batchID).modal('hide');
                },
                error: function (data) {
                    if (data.statusText == "error") {
                        ShowError("Connection Error!");
                    }
                    TriggerLoading(false);
                }
            });




        }
    }

    function CreateBatchContent(data, selector, highlight, allianceID) {
        var clientList = [];
        var missingCount = 0;

        $.each(data.TempInvoiceDetailList, function (invoiceIndex, invoice) {
            var ad_amount = 0;
            var adj_amount = 0;

            if (invoice.TransactionType == "Base")
                ad_amount = invoice.ListPrice;
            else
                adj_amount = invoice.ListPrice;

            var listInd = FindObjectInArray(invoice.BillToName, invoice.BillToID, clientList, "BillToName", "BillToID");
            //console.log(invoice.EnableGrouping);
            if (listInd == -1) {
                var rowObject = {
                    InvoiceBatchID: invoice.InvoiceBatchID,
                    BillToIDType: invoice.BillToIDType,
                    BillToName: invoice.BillToName,
                    BillToID: invoice.BillToID,
                    AllianceID: invoice.AllianceID,
                    BillingClientID: invoice.BillingClientID,
                    MarketClientID: invoice.MarketClientID,
                    EnableGrouping: invoice.EnableGrouping,
                    IsMissingData: invoice.IsMissingData,
                    ad_amount: ad_amount,
                    adj_amount: adj_amount
                }
                clientList.push(rowObject);
            } else {
                clientList[listInd].ad_amount += ad_amount;
                clientList[listInd].adj_amount += adj_amount;
            }
        });

        var batchModal = $("#" + selector);
        var batchContainer = $(batchModal).find(".dashboard-group.template").clone();
        $(batchContainer).removeClass("template");
        $(batchContainer).find(".batchID").html(data.InvoiceBatchID);
        $(batchContainer).find(".batchFrom").html(FormatDateString(data.BatchFromDate));
        $(batchContainer).find(".batchTo").html(FormatDateString(data.BatchToDate));

        $.each(clientList, function (index, invoice) {
            var batchRow = $(batchContainer).find(".div-table-row.template").clone();
            $(batchRow).removeClass("template");
            var toQuery = "";

            //Check Alliance || Billing Client
            if (invoice.BillToIDType == "A" || invoice.BillToIDType == "B") {
                var expandclass = "";
                var expandName = "";
                if (invoice.EnableGrouping == 0) {
                    expandclass = ".noexpand";
                    expandName = NO_EXPAND;
                } else
                    expandclass = ".withexpand";


                var invoiceRow = $(batchRow).find(expandclass + ".template").clone();

                $(invoiceRow).removeClass("template");
                $(invoiceRow).addClass("parent-row");
                $(invoiceRow).find(".expand-row").attr("row-type", invoice.BillToIDType);
                $(invoiceRow).find(".expand-row").attr("parent-type", invoice.BillToIDType);
                $(invoiceRow).find(".expand-row").attr("isTopLevel", "true");
                $(invoiceRow).find(".clientName").attr("clientName", invoice.BillToName);
                if (invoice.BillToIDType == "A") {
                    $(invoiceRow).find(".clientName").addClass("alliance-class");
                    $(invoiceRow).find(".clientName").html(expandName + invoice.BillToName);
                    if (invoice.EnableGrouping == 1) {
                        $(invoiceRow).find(".expand-row").attr("data-target", ".child-row-" + invoice.AllianceID);
                        $(invoiceRow).find(".expand-row").attr("clientID", invoice.AllianceID);
                        $(invoiceRow).find(".expand-row").attr("allianceID", 0);
                        $(invoiceRow).find(".expand-row").attr("batchID", invoice.InvoiceBatchID);
                    }
                    $(invoiceRow).find(".client-detail").attr("clientID", invoice.AllianceID);
                    $(invoiceRow).find(".client-detail .detailInvoice").removeClass("template");

                    //toQuery = "alliance-parent-" + invoice.AllianceID;
                } else {
                    if (highlight)
                        if (allianceID) {
                            if (highlight == invoice.BillingClientID && invoice.AllianceID != 0) {
                                $(invoiceRow).addClass("highlight-class");
                            }
                        } else {
                            if (highlight == invoice.BillingClientID && invoice.AllianceID == 0)
                                $(invoiceRow).addClass("highlight-class");
                        }

                    $(invoiceRow).find(".clientName").html(expandName + invoice.BillToName);
                    $(invoiceRow).find(".clientName").addClass("billing-class");

                    if (invoice.EnableGrouping == 1) {
                        $(invoiceRow).find(".expand-row").attr("clientID", invoice.BillingClientID);
                        $(invoiceRow).find(".expand-row").attr("data-target", ".child-billing-row-" + invoice.BillingClientID);
                        if (invoice.AllianceID == 0) {

                            $(invoiceRow).find(".expand-row").attr("allianceID", 0);
                        } else {
                            $(invoiceRow).find(".expand-row").attr("data-target", ".child-parent-" + invoice.AllianceID + "-billing-row-" + invoice.BillingClientID);
                            $(invoiceRow).find(".expand-row").attr("allianceID", invoice.AllianceID);
                        }

                        $(invoiceRow).find(".expand-row").attr("batchID", data.InvoiceBatchID);
                    }

                    $(invoiceRow).find(".client-detail").attr("clientID", invoice.BillingClientID);
                    $(invoiceRow).find(".client-detail .detailInvoice").removeClass("template");
                    if (invoice.BillToName == 'Blue Cross Blue Shield of Massachusetts') {

                    }

                    if (invoice.AllianceID != 0 && invoice.EnableGrouping == 1) {
                        $(invoiceRow).find(".client-detail .combineInvoice").removeClass("template");
                        UpdateBreakCombine(invoiceRow, invoice.BillingClientID, "B", data.InvoiceBatchID, true, invoice.AllianceID);
                    }
                    //toQuery = "billing-parent-" + invoice.BillingClientID;
                }

                $(invoiceRow).find(".ad-price").html(FormatAmount(invoice.ad_amount));
                $(invoiceRow).find(".man-price").html(FormatAmount(invoice.adj_amount));

                if (invoice.IsMissingData == 1) {
                    missingCount++;
                    $(invoiceRow).find(".clientName").after("<span class='missing-data'></span>");
                }

                $(batchRow).append(invoiceRow);

            } else {
                var invoiceRow = $(batchRow).find(".noexpand.template").clone();
                if (highlight)
                    if (allianceID) {
                        if (highlight == invoice.MarketClientID && invoice.AllianceID != 0)
                            $(invoiceRow).addClass("highlight-class");
                    } else {
                        if (highlight == invoice.MarketClientID && invoice.AllianceID == 0)
                            $(invoiceRow).addClass("highlight-class");
                    }

                $(invoiceRow).removeClass("template");
                $(invoiceRow).addClass("parent-row");
                $(invoiceRow).find(".clientName").attr("clientName", invoice.BillToName);
                $(invoiceRow).find(".clientName").html(NO_EXPAND + invoice.BillToName);
                $(invoiceRow).find(".clientName").addClass("market-class");

                $(invoiceRow).find(".client-detail").attr("clientID", invoice.MarketClientID);
                if (invoice.EnableGrouping == 1)
                    $(invoiceRow).find(".client-detail .combineInvoice").removeClass("template");
                $(invoiceRow).find(".client-detail .detailInvoice").removeClass("template");

                $(invoiceRow).find(".ad-price").html(FormatAmount(invoice.ad_amount));
                $(invoiceRow).find(".man-price").html(FormatAmount(invoice.adj_amount));
                if (invoice.IsMissingData == 1) {
                    missingCount++;
                    $(invoiceRow).find(".clientName").after("<span class='missing-data'></span>");
                }
                $(batchRow).append(invoiceRow);

                if (invoice.AllianceID == 0)
                    UpdateBreakCombine(invoiceRow, invoice.MarketClientID, "M", data.InvoiceBatchID, true);
                else
                    UpdateBreakCombine(invoiceRow, invoice.MarketClientID, "M", data.InvoiceBatchID, true, invoice.AllianceID);
            }

            $(batchContainer).find(".div-table").append(batchRow);

        });

        var summary = $(batchContainer).find(".div-table-summary.template").clone();
        $(summary).removeClass("template");

        $(batchContainer).find(".div-table").append(summary);

        BindGenerateInvoiceEvent($(batchContainer).find(".generate_invoice"), missingCount);

        $(batchModal).append(batchContainer);

        BindParentExpandBtn(data.InvoiceBatchID);
        RefreshChargePrice(batchContainer);
        TriggerLoading(false);
    }

    function BindGenerateInvoiceEvent(element, missingCount) {
        $(element).on("click", {
            missingCount: missingCount
        }, function (e) {
            e.preventDefault();
            var dialogMessage = "";
            var btns = [];

            var failed = [{
                icon: 'glyphicon glyphicon-ok',
                label: 'Ignore and Proceed!',
                cssClass: 'btn-danger',
                action: function (dialog) {
                    var batchID = currentBatch;
                    TriggerLoading(true);
                    UpdateLoadingStatus("Status", "Retrieving Status ...");
                    var handle = setInterval(GetStatusText.bind(null, batchID), 5000);
                    e.preventDefault();
                    $.ajax({
                        dataType: 'json',
                        method: "PUT",
                        crossDomain: true,
                        xhrFields: {
                            withCredentials: true
                        },
                        timer: handle,
                        url: "http://" + apiSite + "/api/File?batchID=" + batchID,
                        success: function (data) {
                            //console.log(data);
                            clearInterval(this.timer);
                            if (data == "Success") {
                                UpdateLoadingStatus("Status", "Complete!");
                                window.location.replace("upload");
                            } else
                                console.log(data);
                            UpdateLoadingStatus("", "");
                            TriggerLoading(false);
                        },
                        error: function (data) {
                            clearInterval(this.timer);
                            if (data.statusText == "error") {
                                ShowError("Connection Error!");
                            }
                            TriggerLoading(false);
                        }
                    });

                }
            }, {
                icon: 'glyphicon glyphicon-remove',
                label: 'Back',
                action: function (dialog) {
                    dialog.close();
                }
            }]

            var pass = [{
                icon: 'glyphicon glyphicon-ok',
                label: 'Generate Invoice',
                cssClass: 'btn-success',
                action: function (dialog) {
                    var batchID = currentBatch;
                    TriggerLoading(true);
                    UpdateLoadingStatus("Status", "Retrieving Status ...");
                    var handle = setInterval(GetStatusText.bind(null, batchID), 5000);
                    e.preventDefault();
                    $.ajax({
                        dataType: 'json',
                        method: "PUT",
                        crossDomain: true,
                        xhrFields: {
                            withCredentials: true
                        },

                        timer: handle,
                        url: "http://" + apiSite + "/api/File?batchID=" + batchID,
                        success: function (data) {
                            //console.log(data);
                            clearInterval(this.timer);
                            if (data == "Success") {
                                UpdateLoadingStatus("Status", "Complete!");
                                window.location.replace("upload");
                            } else
                                console.log(data);
                            UpdateLoadingStatus("", "");
                            TriggerLoading(false);
                        },
                        error: function (data) {
                            clearInterval(this.timer);
                            if (data.statusText == "error") {
                                ShowError("Connection Error!");
                            }
                            TriggerLoading(false);
                        }
                    });

                }
            }, {
                icon: 'glyphicon glyphicon-remove',
                label: 'Back',
                action: function (dialog) {
                    dialog.close();
                }
            }]

            if (e.data.missingCount > 0) {
                btns = failed;
                //dialogMessage = 'There is no missing profile found, click <b>Generate Invoice</b> to proceed. <br> <i><b>Note:</b> The longer the date range, the longer it takes to generate invoices</i>.';
                dialogMessage = 'There are <b>' + e.data.missingCount + ' Invoices with Missing Profiles </b>, please correct them in Configuration menu before attempting to generate invoices to prevent incorrect data in generated files.';
            } else {
                btns = pass;
                dialogMessage = 'There is no missing profile, click <b>Generate Invoices</b> to proceed. <br> Note: The longer the date range, the longer it will take to generate invoices.';
            }


            BootstrapDialog.show({
                title: 'Confirmation',
                message: dialogMessage,
                type: BootstrapDialog.TYPE_PRIMARY,
                buttons: btns
            });
        });
    }

    function BindParentExpandBtn(batchID) {
        $("#invoice-no-" + batchID + " .parent-row").find(".expand-row").on("click", function (e) {
            e.preventDefault();
            var allianceID = $(this).attr("allianceID");
            var topLevel = $(this).attr("isTopLevel");
            var rowType = $(this).attr("row-type");
            var parentType = $(this).attr("parent-type");
            var clientID = $(this).attr("clientid");
            var batchID = $(this).attr("batchID");
            var isShow = $(this).attr("isShow");
            var isLoaded = $(this).attr("isLoaded");
            var parent = $(this).parent().parent().parent();
            if (isShow == "false" && isLoaded != "true") {
                TriggerLoading(true);
                UpdateExpandIcon(this, true);
                $(this).attr("isLoaded", true);
                $.ajax({
                    dataType: 'json',
                    method: "GET",
                    crossDomain: true,
                    xhrFields: {
                        withCredentials: true
                    },
                    parentField: $(parent),
                    currentField: $(this),
                    allianceID: allianceID,
                    parentID: clientID,
                    rowType: rowType,
                    parentType: parentType,
                    allianceID: allianceID,
                    topLevel: topLevel,
                    data: {
                        batchID: batchID,
                        clientID: clientID,
                        type: rowType,
                        parenttype: parentType,
                        allianceID: allianceID
                    },
                    url: "http://" + apiSite + "/api/Report",
                    success: function (data) {
                        if (this.rowType == "A") {
                            PopulateAllianceChild(data, this.currentField, this.parentField, this.parentID);
                        } else {
                            if (this.allianceID > 0)
                                PopulateBillingChild(data, this.currentField, this.parentField, this.parentID, this.allianceID, this.topLevel);
                            else
                                PopulateBillingChild(data, this.currentField, this.parentField, this.parentID);
                        }
                        TriggerLoading(false);
                    },
                    error: function (data) {
                        if (data.statusText == "error") {
                            ShowError("Connection Error!");
                        }
                        TriggerLoading(false);
                    }
                });
            } else {
                TriggerLoading(true);
                UpdateExpandIcon(this, true);
            }
        });
    }

    function PopulateAllianceChild(data, current, parent, allianceID) {

        var clientList = [];

        $.each(data, function (invoiceIndex, invoice) {
            var ad_amount = 0;
            var adj_amount = 0;

            if (invoice.TransactionType == "Base")
                ad_amount = parseFloat(invoice.ListPrice);
            else
                adj_amount = parseFloat(invoice.ListPrice);

            var listInd = FindObjectInArray(invoice.BillingClientName, invoice.BillingClientID, clientList, "BillingClientName", "BillingClientID");

            if (listInd == -1) {
                var rowObject = {
                    InvoiceBatchID: invoice.InvoiceBatchID,
                    BillToIDType: invoice.BillToIDType,
                    BillToName: invoice.BillToName,
                    BillToID: invoice.BillToID,
                    AllianceID: invoice.AllianceID,
                    AllianceName: invoice.AllianceName,
                    BillingClientID: invoice.BillingClientID,
                    BillingClientName: invoice.BillingClientName,
                    MarketClientID: invoice.MarketClientID,
                    MarketClientName: invoice.MarketClientName,
                    ad_amount: ad_amount,
                    adj_amount: adj_amount
                }
                clientList.push(rowObject);
            } else {
                clientList[listInd].ad_amount += ad_amount;
                clientList[listInd].adj_amount += adj_amount;
            }
        });

        var breakable = false;
        if (clientList.length > 1) {
            breakable = true;
        }

        //Start Alliance Child Loop
        $.each(clientList, function (billingIndex, billing) {
            var invoiceRow = $(parent).find(".withexpand.template").clone();
            $(invoiceRow).removeClass("template");
            $(invoiceRow).attr("aria-expanded", true);
            $(invoiceRow).addClass("sub-table-row");
            $(invoiceRow).addClass("collapse");
            $(invoiceRow).addClass("alliance-" + allianceID + "-billingclient-" + billing.BillingClientID);
            $(invoiceRow).addClass("in");
            $(invoiceRow).addClass("child-row-" + allianceID)

            $(invoiceRow).find(".expand-row").attr("row-type", "B");
            $(invoiceRow).find(".expand-row").attr("parent-type", "A");
            $(invoiceRow).find(".clientName").html(NO_EXPAND + billing.BillingClientName);
            $(invoiceRow).find(".expand-row").attr("data-target", ".child-alliance-" + allianceID + "-billing-row-" + billing.BillingClientID);
            $(invoiceRow).find(".expand-row").attr("clientID", billing.BillingClientID);
            $(invoiceRow).find(".expand-row").attr("batchID", billing.InvoiceBatchID);
            $(invoiceRow).find(".client-detail").attr("clientID", billing.BillingClientID);
            if (breakable == true)
                $(invoiceRow).find(".client-detail .separateInvoice").removeClass("template");
            $(invoiceRow).find(".ad-price").html(FormatAmount(billing.ad_amount));
            $(invoiceRow).find(".man-price").html(FormatAmount(billing.adj_amount));
            $(invoiceRow).find(".char-price").html(FormatAmount(billing.adj_amount + billing.ad_amount));
            //$(mainRow).appendTo(parent);
            $(parent).append(invoiceRow);

            $(invoiceRow).on("hidden.bs.collapse", function () {
                TriggerLoading(false);
            })

            $(invoiceRow).on("shown.bs.collapse", function () {
                TriggerLoading(false);
            })

            UpdateBreakCombine(invoiceRow, billing.BillingClientID, "B", billing.InvoiceBatchID, false, allianceID);

            $(invoiceRow).find(".expand-row").on("click", function () {
                BindBillingChildExpand($(this), allianceID);
                UpdateExpandIcon($(this), false);
            })
        });
        //End Alliance Child Loop
    }

    function PopulateBillingChild(data, current, parent, billingID, allianceID, topLevel) {
        var clientList = [];

        $.each(data, function (invoiceIndex, invoice) {
            var ad_amount = 0;
            var adj_amount = 0;
            var price = 0;

            if (invoice.ChargedPrice == 0)
                price = invoice.ListPrice;
            else
                price = invoice.ChargedPrice;

            if (invoice.TransactionType == "Base")
                ad_amount = parseInt(invoice.Quantity) * parseFloat(price);
            else
                adj_amount = parseInt(invoice.Quantity) * parseFloat(price);

            var listInd = FindObjectInArray(invoice.MarketClientName, invoice.MarketClientID, clientList, "MarketClientName", "MarketClientID");

            if (listInd == -1) {
                var rowObject = {
                    InvoiceBatchID: invoice.InvoiceBatchID,
                    BillToIDType: invoice.BillToIDType,
                    BillToName: invoice.BillToName,
                    BillToID: invoice.BillToID,
                    AllianceID: invoice.AllianceID,
                    AllianceName: invoice.AllianceName,
                    BillingClientID: invoice.BillingClientID,
                    BillingClientName: invoice.BillingClientName,
                    MarketClientID: invoice.MarketClientID,
                    MarketClientName: invoice.MarketClientName,
                    ad_amount: ad_amount,
                    adj_amount: adj_amount
                }
                clientList.push(rowObject);
            } else {
                clientList[listInd].ad_amount += ad_amount;
                clientList[listInd].adj_amount += adj_amount;
            }
        });

        var breakable = false;
        if (clientList.length > 1) {
            breakable = true;
        }

        $.each(clientList, function (marketIndex, market) {
            var invoiceRow = $(parent).find(".noexpand.template").clone();
            $(invoiceRow).removeClass("template");
            $(invoiceRow).attr("aria-expanded", true);
            $(invoiceRow).addClass("sub-sub-table-row");
            $(invoiceRow).addClass("collapse");
            $(invoiceRow).addClass("in");
            if (allianceID)
                if (topLevel)
                    $(invoiceRow).addClass("child-parent-" + allianceID + "-billing-row-" + billingID);
                else
                    $(invoiceRow).addClass("child-alliance-" + allianceID + "-billing-row-" + billingID);
            else
                $(invoiceRow).addClass("child-billing-row-" + billingID);

            if (allianceID)
                if (topLevel)
                    $(invoiceRow).addClass("parent-" + allianceID + "-marketclient-" + market.MarketClientID);
                else
                    $(invoiceRow).addClass("alliance-" + allianceID + "-marketclient-" + market.MarketClientID);
            else
                $(invoiceRow).addClass("marketclient-" + market.MarketClientID);
            $(invoiceRow).find(".client-detail").attr("parent-type", "B");
            $(invoiceRow).find(".client-detail").attr("row-type", "M");
            $(invoiceRow).find(".clientName").html(NO_EXPAND + NO_EXPAND + market.MarketClientName);
            $(invoiceRow).find(".client-detail").attr("clientID", market.MarketClientID);
            if (breakable == true)
                $(invoiceRow).find(".client-detail .separateInvoice").removeClass("template");

            $(invoiceRow).find(".ad-price").html(FormatAmount(market.ad_amount));
            $(invoiceRow).find(".man-price").html(FormatAmount(market.adj_amount));
            $(invoiceRow).find(".char-price").html(FormatAmount(market.ad_amount + market.adj_amount));

            $(invoiceRow).on("hidden.bs.collapse", function () {
                TriggerLoading(false);
            })

            $(invoiceRow).on("shown.bs.collapse", function () {
                TriggerLoading(false);
            })

            var toAppend = "";
            if (allianceID)
                if (topLevel)
                    toAppend = "parent-" + allianceID + "-billingclient-" + billingID;
                else
                    toAppend = "alliance-" + allianceID + "-billingclient-" + billingID;
            else
                toAppend = "billingclient-" + billingID;

            var whatAppend = "";
            if (allianceID)
                if (topLevel)
                    whatAppend = "parent-" + allianceID + "-marketclient-" + market.MarketClientID;
                else
                    whatAppend = "alliance-" + allianceID + "-marketclient-" + market.MarketClientID;
            else
                whatAppend = "marketclient-" + market.MarketClientID;

            $(parent).append(invoiceRow);
            $("." + whatAppend).insertAfter("." + toAppend);



            if (allianceID)
                UpdateBreakCombine("." + whatAppend, market.MarketClientID, "M", market.InvoiceBatchID, false, allianceID);
            else
                UpdateBreakCombine("." + whatAppend, market.MarketClientID, "M", market.InvoiceBatchID, false);

        });
    }

    function BindBillingChildExpand(current, allianceID) {
        var rowType = $(current).attr("row-type");
        var parentType = $(current).attr("parent-type");
        var clientID = $(current).attr("clientid");
        var batchID = $(current).attr("batchID");
        var isShow = $(current).attr("isShow");
        var isLoaded = $(current).attr("isLoaded");
        var parent = $(current).parent().parent().parent();
        if (isShow == "false" && isLoaded != "true") {
            TriggerLoading(true);
            $(current).attr("isLoaded", true);

            $.ajax({
                dataType: 'json',
                method: "GET",
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                parentField: $(parent),
                currentField: $(current),
                parentID: clientID,
                allianceID: allianceID,
                rowType: rowType,
                parentType: parentType,
                data: {
                    batchID: batchID,
                    clientID: clientID,
                    allianceID: allianceID,
                    type: rowType,
                    parenttype: parentType
                },
                url: "http://" + apiSite + "/api/Report",
                success: function (data) {
                    if (this.allianceID > 0)
                        PopulateBillingChild(data, this.currentField, this.parentField, this.parentID, this.allianceID);
                    else
                        PopulateBillingChild(data, this.currentField, this.parentField, this.parentID);
                    TriggerLoading(false);
                },
                error: function (data) {
                    if (data.statusText == "error") {
                        ShowError("Connection Error!");
                    }
                    TriggerLoading(false);
                }
            });
        } else {
            TriggerLoading(true);
        }
    }

    function UpdateBreakCombine(current, clientID, rowtype, batchID, isCombine, alliance) {

        var allianceid = 0;
        if (alliance)
            allianceid = alliance;


        if (isCombine == false) {
            $(current).find(".separateInvoice").on("click", function () {
                var InvoiceObj = {
                    batchID: batchID,
                    clientID: clientID,
                    allianceID: allianceid,
                    type: rowtype,
                    isCombine: false
                };

                $.ajax({
                    dataType: 'json',
                    method: "POST",
                    crossDomain: true,
                    xhrFields: {
                        withCredentials: true
                    },
                    url: "http://" + apiSite + "/api/Report",
                    data: InvoiceObj,
                    batchID: batchID,
                    highlight: clientID,
                    allianceID: allianceid,
                    success: function (data) {
                        RefreshInvoiceBatch(this.batchID, this.highlight, this.allianceID);
                    },
                    error: function (data) {
                        if (data.statusText == "error") {
                            ShowError("Connection Error!");
                        }
                        TriggerLoading(false);
                    }
                });
            })
        } else {
            $(current).find(".combineInvoice").on("click", function () {
                var InvoiceObj = {
                    batchID: batchID,
                    clientID: clientID,
                    allianceID: allianceid,
                    type: rowtype,
                    isCombine: true
                };

                $.ajax({
                    dataType: 'json',
                    method: "POST",
                    crossDomain: true,
                    xhrFields: {
                        withCredentials: true
                    },
                    url: "http://" + apiSite + "/api/Report",
                    data: InvoiceObj,
                    batchID: batchID,
                    highlight: clientID,
                    success: function (data) {
                        RefreshInvoiceBatch(this.batchID);
                    },
                    error: function (data) {
                        if (data.statusText == "error") {
                            ShowError("Connection Error!");
                        }
                        TriggerLoading(false);
                    }
                });
            })
        }
    }

    function RefreshInvoiceBatch(batchID, highlight, allianceID) {
        var highlightRow = -1;
        if (highlight)
            highlightRow = highlight;
        TriggerLoading(true);
        $.ajax({
            dataType: 'json',
            method: "GET",
            crossDomain: true,
            xhrFields: {
                withCredentials: true
            },
            selector: "invoice-no-" + batchID,
            allianceID: allianceID,
            highlight: highlightRow,
            data: {
                batchID: batchID
            },
            url: "http://" + apiSite + "/api/Report",
            success: function (data) {
                var modal = $("#" + this.selector);
                $(modal).find(".dashboard-group")[1].remove();
                if (this.highlight != -1) {
                    if (this.allianceID == 0)
                        CreateBatchContent(data, this.selector, this.highlight);
                    else
                        CreateBatchContent(data, this.selector, this.highlight, this.allianceID);
                } else
                    CreateBatchContent(data, this.selector);
                TriggerLoading(false);
            },
            error: function (data) {
                if (data.statusText == "error") {
                    ShowError("Connection Error!");
                }
                TriggerLoading(false);
            }
        });
    }

    function RefreshChargePrice(batch) {
        var totalad = 0;
        var totalman = 0;
        var totalexp = 0;
        var totalcharge = 0;

        $.each($(batch).find(".div-table-row:not(.template)"), function (index, row) {
            var rowToUpdate = $(row).find("> div:not(.template)");
            var manual = parseFloat($(rowToUpdate).find(".man-price").text().replace(",", "").replace("$", "").trim());
            var ad = parseFloat($(rowToUpdate).find(".ad-price").text().replace(",", "").replace("$", "").trim());
            totalad += ad;
            totalman += manual;
            var charge = (manual + ad);
            totalcharge += charge;
            $(rowToUpdate).find(".char-price").html(FormatAmount(charge));
            BindDetailInvoice(rowToUpdate);
        });

        var summary = $(batch).find(".div-table-summary:not(.template)");
        $(summary).find(".summary-ad").html(FormatAmount(totalad));
        $(summary).find(".summary-man").html(FormatAmount(totalman));
        $(summary).find(".summary-char").html(FormatAmount(totalcharge));
    }

    function BindDetailInvoice(row) {
        var elementToUpdate = $(row).find(".detailInvoice");
        var clientID = $(elementToUpdate).parent().attr("clientid");
        var nameObj = $(elementToUpdate).parent().parent().find(".clientName");
        var clientName = $(nameObj).attr("clientName");
        var batchID = $(row).parent().parent().parent().parent().attr("batchid");
        var rowType = "";
        if ($(nameObj).hasClass("alliance-class"))
            rowType = "A";
        else if ($(nameObj).hasClass("billing-class"))
            rowType = "B";
        else
            rowType = "M";
        $(elementToUpdate).on("click", function () {
            TriggerLoading(true);
            $("#invoice-detail-modal .modal-header .clientName").html(clientName);
            PullDetailInvoiceData(clientID, rowType, batchID);
            /*$.ajax({
                dataType: 'json',
                method: "GET", crossDomain: true,xhrFields: {                 withCredentials: true             },
                clientID: clientID,
                rowType: rowType,
                data: {
                    clientid: clientID,
                    rowtype: rowType,
                    batchID: batchID
                },
                url: "http://" + apiSite + "/api/Report",
                success: function (data) {
                    RefreshInvoiceDetail(data, this.rowType, this.clientID);
                }
            });*/
        });
    }

    function RefreshInvoiceDetail(data, rowType, clientID) {
        var detailContainer = $("#invoice-detail-modal .template-container");
        var startAdGroup = "#invoice-detail-modal .template-container .advantage-group";
        var startManGroup = "#invoice-detail-modal .template-container .manual-group";
        var clientBatchID = 0;
        $.each($(detailContainer).find(".advantage-row:not(.template)"), function (index, toDelete) {
            $(toDelete).remove();
        });

        $.each($(detailContainer).find(".manual-row:not(.template)"), function (index, toDelete) {
            $(toDelete).remove();
        });

        var grouping = false;
        var showMarket = false;
        if (data.EnableGrouping == 1)
            grouping = true;
        if (data.DisplayOption == "Market Client")
            showMarket = true;

        $(".switch-group input").unbind("switchChange.bootstrapSwitch");
        ChangeSwitchState("EnableGrouping", grouping);
        ChangeSwitchState("DisplayOption", showMarket);
        BindSwitchEvent(rowType, clientID);
        //$("[name='EnableGrouping']").bootstrapSwitch('setState', grouping);
        //$("[name='DisplayMarket']").bootstrapSwitch('setState', showMarket);

        var baseList = [];
        var adjustmentList = [];

        var marketBaseList = [];
        var marketAdjustmentList = [];

        var tempObj = {
            MarketClientID: 0,
            MarketClientName: '',
            ServiceList: []
        }

        //If Display By Market
        if (showMarket) {
            $.each(data.DetailList, function (itemIndex, item) {
                if (clientBatchID == 0)
                    clientBatchID = item.InvoiceBatchID;

                var amount = parseFloat(item.ListPrice);

                var listInd = -1;

                if (item.TransactionType == "Base")
                    listInd = FindObjectInArray(item.MarketClientName, item.MarketClientID, marketBaseList, "MarketClientName", "MarketClientID");
                else
                    listInd = FindObjectInArray(item.MarketClientName, item.MarketClientID, marketAdjustmentList, "MarketClientName", "MarketClientID");

                if (listInd == -1) {
                    var rowObject = {
                        InvoiceBatchID: item.InvoiceBatchID,
                        MarketClientID: item.MarketClientID,
                        MarketClientName: item.MarketClientName,
                        TransactionType: item.TransactionType,
                        TransactionSource: item.TransactionSource,
                        BillToID: item.BillToID,
                        BillToIDType: item.BillToIDType,
                        BillToName: item.BillToName,
                        amount: amount,
                        ServiceList: [
                            {
                                ServiceItemType: item.ServiceItemType,
                                ServiceItemTypeID: item.ServiceItemTypeID,
                                BillToID: item.BillToID,
                                BillToIDType: item.BillToIDType,
                                BillToName: item.BillToName,
                                amount: amount
                            }
                        ]
                    }

                    if (item.TransactionType == "Base")
                        marketBaseList.push(rowObject);
                    else
                        marketAdjustmentList.push(rowObject);

                } else {
                    if (item.TransactionType == "Base") {
                        var serInd = -1;
                        serInd = FindObjectInArray(item.ServiceItemType, item.ServiceItemTypeID, marketBaseList[listInd].ServiceList, "ServiceItemType", "ServiceItemTypeID");
                        if (serInd == -1) {
                            var serObject = {
                                ServiceItemType: item.ServiceItemType,
                                ServiceItemTypeID: item.ServiceItemTypeID,
                                BillToID: item.BillToID,
                                BillToIDType: item.BillToIDType,
                                BillToName: item.BillToName,
                                amount: amount
                            };
                            marketBaseList[listInd].amount += amount;
                            marketBaseList[listInd].ServiceList.push(serObject);
                        } else {
                            marketBaseList[listInd].amount += amount;
                            marketBaseList[listInd].ServiceList[serInd].amount += amount;
                        }
                    } else {
                        var serInd = -1;
                        serInd = FindObjectInArray(item.ServiceItemType, item.ServiceItemTypeID, marketAdjustmentList[listInd].ServiceList, "ServiceItemType", "ServiceItemTypeID");
                        if (serInd == -1) {
                            var serObject = {
                                ServiceItemType: item.ServiceItemType,
                                ServiceItemTypeID: item.ServiceItemTypeID,
                                BillToID: item.BillToID,
                                BillToIDType: item.BillToIDType,
                                BillToName: item.BillToName,
                                amount: amount
                            };
                            marketAdjustmentList[listInd].amount += amount;
                            marketAdjustmentList[listInd].ServiceList.push(serObject);
                        } else {
                            marketAdjustmentList[listInd].amount += amount;
                            marketAdjustmentList[listInd].ServiceList[serInd].amount += amount;
                        }
                    }

                }
            });
            var combinedArray = $.merge(marketBaseList, marketAdjustmentList);
            combinedArray.sort(SortByName);
            $.each(combinedArray, function (index, serRow) {
                if (serRow.TransactionType == "Base") {
                    //var newAdRow = $(detailContainer).find(".display-market-row.template").clone();
                    var newAdRow = $(detailContainer).find(".advantage-row.template").clone();
                    $(newAdRow).removeClass("template");
                    //$(newAdRow).addClass("advantage-row");
                    $(newAdRow).addClass("temp-adrow");
                    var adDes = serRow.MarketClientName;

                    var dlDetail = " <a target='_self' href='@downloadLink' batchID=@batchID clientType=@clientType clientID=@clientID marketID=@marketID marketName='@marketName' class='invoiceRowDetail'><i class='fa fa-download' aria-hidden='true' data-placement='left' rel='serviceDetail' data-toggle='tooltip' title='Download Market Detail'></i></a>"

                    dlDetail = dlDetail.replace("@clientType", serRow.BillToIDType);
                    dlDetail = dlDetail.replace("@clientID", serRow.BillToID);
                    dlDetail = dlDetail.replace("@marketID", serRow.MarketClientID);
                    dlDetail = dlDetail.replace("@marketName", serRow.MarketClientName);
                    dlDetail = dlDetail.replace("@batchID", serRow.InvoiceBatchID);

                    var download = "http://" + apiSite + "/api/Report?batchID=" + serRow.InvoiceBatchID + "&clientID=" + serRow.BillToID + "&clientType=" + serRow.BillToIDType + "&MarketClientID=" + serRow.MarketClientID + "&MarketClientName=" + serRow.MarketClientName.replace(" ", "%20");

                    dlDetail = dlDetail.replace("@downloadLink", download);

                    $(newAdRow).find(".ad-description").html(adDes + dlDetail);
                    $(newAdRow).find(".ad-rate").html(FormatAmount(serRow.amount));
                    $(newAdRow).find(".ad-total").html(FormatAmount(serRow.amount));

                    $(detailContainer).find(".template-content").append(newAdRow);
                    $(".advantage-group").after($(".temp-adrow"));
                    $(".temp-adrow").removeClass("temp-adrow");
                } else if (serRow.TransactionType == "Adjustment" && serRow.TransactionSource != "ManualInput") {
                    //var newAdRow = $(detailContainer).find(".display-market-row.template").clone();
                    var newAdRow = $(detailContainer).find(".advantage-row.template").clone();
                    $(newAdRow).removeClass("template");
                    //$(newAdRow).addClass("advantage-row");
                    $(newAdRow).addClass("temp-adrow");

                    var adDes = serRow.MarketClientName;

                    var dlDetail = " <a target='_self' href='@downloadLink' batchID=@batchID clientType=@clientType clientID=@clientID marketID=@marketID marketName='@marketName' class='invoiceRowDetail'><i class='fa fa-download' aria-hidden='true' data-placement='left' rel='serviceDetail' data-toggle='tooltip' title='Download Market Detail'></i></a>"

                    dlDetail = dlDetail.replace("@clientType", serRow.BillToIDType);
                    dlDetail = dlDetail.replace("@clientID", serRow.BillToID);
                    dlDetail = dlDetail.replace("@marketID", serRow.MarketClientID);
                    dlDetail = dlDetail.replace("@marketName", serRow.MarketClientName);
                    dlDetail = dlDetail.replace("@batchID", serRow.InvoiceBatchID);

                    var download = "http://" + apiSite + "/api/Report?batchID=" + serRow.InvoiceBatchID + "&clientID=" + serRow.BillToID + "&clientType=" + serRow.BillToIDType + "&marketID=" + serRow.MarketClientID + "&marketName=" + serRow.MarketClientName.replace(" ", "%20");

                    dlDetail = dlDetail.replace("@downloadLink", download);


                    $(newAdRow).find(".ad-description").html(adDes + dlDetail);
                    $(newAdRow).find(".ad-rate").html(FormatAmount(serRow.amount));
                    $(newAdRow).find(".ad-total").html(FormatAmount(serRow.amount));

                    $(detailContainer).find(".template-content").append(newAdRow);
                    $(".manual-group").after($(".temp-adrow"));

                    $(".temp-adrow").removeClass("temp-adrow");
                } else {
                    $.each(serRow.ServiceList, function (serviceIndex, serviceItem) {
                        var newAdRow = $(detailContainer).find(".advantage-row.template").clone();
                        $(newAdRow).removeClass("template");
                        //$(newAdRow).addClass("advantage-row");
                        $(newAdRow).addClass("temp-adrow");
                        var adDes = serviceItem.ServiceItemType;
                        var tempDel = "<a class='deleteAdjustment' clientType=@clientType clientID=@clientID batchID=@batchID amount=@amount description='@description' hred='#' data-placement='top' rel='deleteAdjustment' data-toggle='tooltip' title='Delete This Adjustment'><i class='fa fa-times'></i></a>";
                        tempDel = tempDel.replace("@clientType", serRow.BillToIDType);
                        tempDel = tempDel.replace("@clientID", serRow.BillToID);
                        tempDel = tempDel.replace("@batchID", serRow.InvoiceBatchID);
                        tempDel = tempDel.replace("@amount", serviceItem.amount);
                        tempDel = tempDel.replace("@description", serviceItem.ServiceItemType);

                        $(newAdRow).find(".ad-quantity").html(tempDel);
                        $(newAdRow).find(".ad-description").html(adDes);
                        $(newAdRow).find(".ad-rate").html(FormatAmount(serviceItem.amount));
                        $(newAdRow).find(".ad-total").html(FormatAmount(serviceItem.amount));

                        $(detailContainer).find(".template-content").append(newAdRow);
                        $(".manual-group").after($(".temp-adrow"));
                        $(".temp-adrow").removeClass("temp-adrow");
                    });
                }
            });
        } else {
            //If Display By Service Item Type
            $.each(data.DetailList, function (itemIndex, item) {
                if (clientBatchID == 0)
                    clientBatchID = item.InvoiceBatchID;

                var amount = parseFloat(item.ListPrice);

                var listInd = -1;

                if (item.TransactionType == "Base")
                    listInd = FindObjectInArray(item.ServiceItemType, item.ServiceItemTypeID, baseList, "ServiceItemType", "ServiceItemTypeID");
                else
                    listInd = FindObjectInArray(item.ServiceItemType, item.ServiceItemTypeID, adjustmentList, "ServiceItemType", "ServiceItemTypeID");

                if (listInd == -1) {
                    var rowObject = {
                        InvoiceBatchID: item.InvoiceBatchID,
                        BillToIDType: item.BillToIDType,
                        BillToName: item.BillToName,
                        BillToID: item.BillToID,
                        AllianceID: item.AllianceID,
                        AllianceName: item.AllianceName,
                        BillingClientID: item.BillingClientID,
                        BillingClientName: item.BillingClientName,
                        MarketClientID: item.MarketClientID,
                        MarketClientName: item.MarketClientName,
                        ServiceItemType: item.ServiceItemType,
                        ServiceItemTypeID: item.ServiceItemTypeID,
                        TransactionType: item.TransactionType,
                        TransactionSource: item.TransactionSource,
                        amount: amount
                    }

                    if (item.TransactionType == "Base")
                        baseList.push(rowObject);
                    else
                        adjustmentList.push(rowObject);

                } else {
                    if (item.TransactionType == "Base")
                        baseList[listInd].amount += amount;
                    else
                        adjustmentList[listInd].amount += amount;
                }
            });
            var combinedArray = $.merge(baseList, adjustmentList);
            $.each(combinedArray, function (index, serRow) {

                if (serRow.TransactionType == "Base") {
                    var newAdRow = $(detailContainer).find(".advantage-row.template").clone();
                    $(newAdRow).removeClass("template");
                    $(newAdRow).addClass("temp-adrow");

                    var adDes = serRow.ServiceItemType;

                    var dlDetail = " <a target='_self' href='@downloadLink' batchID=@batchID clientType=@clientType clientID=@clientID serviceTypeID=@typeID serviceType='@typeName' class='invoiceRowDetail'><i class='fa fa-download' aria-hidden='true' data-placement='left' rel='serviceDetail' data-toggle='tooltip' title='Download Service Detail'></i></a>"

                    dlDetail = dlDetail.replace("@clientType", serRow.BillToIDType);
                    dlDetail = dlDetail.replace("@clientID", serRow.BillToID);
                    dlDetail = dlDetail.replace("@batchID", serRow.InvoiceBatchID);
                    dlDetail = dlDetail.replace("@typeID", serRow.ServiceItemTypeID);
                    dlDetail = dlDetail.replace("@typeName", serRow.ServiceItemType);

                    var download = "http://" + apiSite + "/api/Report?batchID=" + serRow.InvoiceBatchID + "&clientID=" + serRow.BillToID + "&clientType=" + serRow.BillToIDType + "&serviceTypeID=" + serRow.ServiceItemTypeID + "&serviceType=" + serRow.ServiceItemType.replace(" ", "%20");

                    dlDetail = dlDetail.replace("@downloadLink", download);

                    $(newAdRow).find(".ad-description").html(adDes /*+ updateDetail*/ + dlDetail);
                    $(newAdRow).find(".ad-rate").html(FormatAmount(serRow.amount));
                    $(newAdRow).find(".ad-total").html(FormatAmount(serRow.amount));
                    $(detailContainer).find(".template-content").append(newAdRow);
                    $(".advantage-group").after($(".temp-adrow"));
                    $(".temp-adrow").removeClass("temp-adrow");
                } else {
                    var newAdRow = $(detailContainer).find(".advantage-row.template").clone();
                    $(newAdRow).removeClass("template");
                    $(newAdRow).addClass("temp-adjrow");

                    var adDes = serRow.ServiceItemType;
                    var tempDel = "<a class='deleteAdjustment' clientType=@clientType clientID=@clientID batchID=@batchID amount=@amount description='@description' hred='#' data-placement='top' rel='deleteAdjustment' data-toggle='tooltip' title='Delete This Adjustment'><i class='fa fa-times'></i></a>";
                    tempDel = tempDel.replace("@clientType", serRow.BillToIDType);
                    tempDel = tempDel.replace("@clientID", serRow.BillToID);
                    tempDel = tempDel.replace("@batchID", serRow.InvoiceBatchID);
                    tempDel = tempDel.replace("@amount", serRow.amount);
                    tempDel = tempDel.replace("@description", serRow.ServiceItemType);



                    var dlDetail = "";

                    if (serRow.TransactionSource != "ManualInput") {
                        dlDetail = " <a target='_self' href='@downloadLink' batchID=@batchID clientType=@clientType clientID=@clientID serviceTypeID=@typeID serviceType='@typeName' class='invoiceRowDetail'><i class='fa fa-download' aria-hidden='true' data-placement='left' rel='serviceDetail' data-toggle='tooltip' title='Download Service Detail'></i></a>"

                        dlDetail = dlDetail.replace("@clientType", serRow.BillToIDType);
                        dlDetail = dlDetail.replace("@clientID", serRow.BillToID);
                        dlDetail = dlDetail.replace("@batchID", serRow.InvoiceBatchID);
                        dlDetail = dlDetail.replace("@typeID", serRow.ServiceItemTypeID);
                        dlDetail = dlDetail.replace("@typeName", serRow.ServiceItemType);


                        var download = "http://" + apiSite + "/api/Report?batchID=" + serRow.InvoiceBatchID + "&clientID=" + serRow.BillToID + "&clientType=" + serRow.BillToIDType + "&serviceTypeID=" + serRow.ServiceItemTypeID + "&serviceType=" + serRow.ServiceItemType.replace(" ", "%20");

                        dlDetail = dlDetail.replace("@downloadLink", download);
                    }

                    $(newAdRow).find(".ad-quantity").html(tempDel);
                    $(newAdRow).find(".ad-description").html(adDes /*+ updateDetail*/ + dlDetail);
                    $(newAdRow).find(".ad-rate").html(FormatAmount(serRow.amount));
                    $(newAdRow).find(".ad-total").html(FormatAmount(serRow.amount));
                    $(detailContainer).find(".template-content").append(newAdRow);
                    $(".manual-group").after($(".temp-adjrow"));
                    $(".temp-adjrow").removeClass("temp-adjrow");
                }
            });
        }

        $("[rel='serviceDetail']").tooltip();
        $("[rel='deleteAdjustment']").tooltip();
        RefreshInvoiceDetailTotal();
        BindAdjustmentDelete();
        $(".add-new-adjustment").unbind();
        $(".add-new-adjustment").on("click", function () {
            var newManRow = $(".manual-row.template").clone();
            $(newManRow).addClass("tempManRow");
            $(newManRow).removeClass("template");
            $(detailContainer).find(".template-content").append(newManRow);
            $(".add-new-btn-row").before($(".tempManRow"));
            $(".tempManRow").removeClass("tempManRow");

            $(newManRow).find(".row-option > a").on("click", function (e) {
                TriggerLoading(true);
                e.preventDefault();
                var rowObj = $(this).parent().parent();

                var AdjustmentObj = {
                    batchID: clientBatchID,
                    clientID: clientID,
                    clientType: rowType,
                    serviceQuantity: 1,
                    serviceDescription: $(rowObj).find(".manual-description > input").val(),
                    serviceRate: parseFloat($(rowObj).find(".manual-rate > input").val())
                }

                $.ajax({
                    dataType: 'json',
                    method: "PUT",
                    crossDomain: true,
                    xhrFields: {
                        withCredentials: true
                    },
                    url: "http://" + apiSite + "/api/Adjustment",
                    data: AdjustmentObj,
                    rowType: rowType,
                    clientID: clientID,
                    batchID: clientBatchID,
                    success: function (data) {
                        if (data == "Success") {
                            //Update Invoice
                            InvoiceNeedRefresh = true;
                            PullDetailInvoiceData(this.clientID, this.rowType, this.batchID);
                        } else {
                            TriggerLoading(false);
                        }
                    },
                    error: function (data) {
                        if (data.statusText == "error") {
                            ShowError("Connection Error!");
                        }
                        TriggerLoading(false);
                    }
                });
            });
        });
        //BindInvoiceRowDetail();
        TriggerLoading(false);
    }

    function BindSwitchEvent(rowType, clientID) {
        $(".switch-group input").unbind("switchChange.bootstrapSwitch");
        $(".switch-group input").on("switchChange.bootstrapSwitch", function (event, state) {
            TriggerLoading(true);
            var type = this.name;
            if (type == "EnableGrouping")
                InvoiceNeedRefresh = true;
            var switchVal = state ? 1 : 0;
            var query = "?clientID=" + clientID;
            query += "&clientType=" + rowType;
            query += "&fieldChange=" + type;
            query += "&fieldValue=" + switchVal;
            $.ajax({
                dataType: 'json',
                method: "POST",
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                url: "http://" + apiSite + "/api/Report" + query,
                rowType: rowType,
                clientID: clientID,
                batchID: currentBatch,
                success: function (data) {
                    if (data == "Success") {
                        //Update Invoice
                        PullDetailInvoiceData(this.clientID, this.rowType, this.batchID);
                    }
                },
                error: function (data) {
                    if (data.statusText == "error") {
                        ShowError("Connection Error!");
                    }
                    TriggerLoading(false);
                }
            });
        });
    }

    function BindAdjustmentDelete() {
        var detailContainer = $("#invoice-detail-modal .template-container");
        $(detailContainer).find(".deleteAdjustment").on("click", function () {
            var clientID = $(this).attr("clientID");
            var clientType = $(this).attr("clientType");
            var batchID = $(this).attr("batchID");
            var description = $(this).attr("description");

            var AdjustmentObj = {
                batchID: batchID,
                clientID: clientID,
                clientType: clientType,
                serviceQuantity: 1,
                serviceDescription: description,
                serviceRate: 0
            }


            TriggerLoading(true);
            $.ajax({
                method: "DELETE",
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                url: "http://" + apiSite + "/api/Adjustment/",
                data: AdjustmentObj,
                rowType: clientType,
                clientID: clientID,
                batchID: batchID,
                success: function (data) {
                    if (data == "Success") {
                        InvoiceNeedRefresh = true;
                        PullDetailInvoiceData(this.clientID, this.rowType, this.batchID);
                    } else {
                        TriggerLoading(false);
                    }
                },
                error: function (data) {
                    if (data.statusText == "error") {
                        ShowError("Connection Error!");
                    }
                    TriggerLoading(false);
                }
            });


        });
    }

    function RefreshInvoiceDetailTotal() {
        var detailContainer = $("#invoice-detail-modal .template-container");
        var totalElement = $(detailContainer).find(".total-group .invoice-total-row");
        var totalAD = 0;
        $.each($(detailContainer).find(".row-total:not(.template)"), function (index, row) {
            var temp = parseFloat($(row).text().replace(/,/g, "").replace("$", "").trim());
            if ($.isNumeric(temp))
                totalAD += temp;
        });

        $(totalElement).html(FormatAmount(totalAD));
    }

    function PullDetailInvoiceData(clientID, rowType, batchID) {
        $.ajax({
            dataType: 'json',
            method: "GET",
            crossDomain: true,
            xhrFields: {
                withCredentials: true
            },
            clientID: clientID,
            batchID: batchID,
            rowType: rowType,
            data: {
                clientID: clientID,
                rowType: rowType,
                batchID: batchID
            },
            url: "http://" + apiSite + "/api/Report",
            success: function (data) {
                RefreshInvoiceDetail(data, this.rowType, this.clientID);
            },
            error: function (data) {
                if (data.statusText == "error") {
                    ShowError("Connection Error!");
                }
                TriggerLoading(false);
            }
        });
    }


    //UPLOAD SCREEN FUNCTIONS
    function PopulateUploadBatchList(data) {
        UpdateLoadingStatus("", "");

        $(".invoice-list-row:not(.template)").each(function () {
            $(this).remove();
        });

        var BatchListRow = [];
        var dataCount = 0;
        var historyCount = 0;
        if (data.length > 0) {
            $.each(data, function (batchIndex, batchListRow) {
                //Check If Batch is Active
                if (batchListRow.BatchStatus == "UploadCheck") {
                    dataCount++;
                    var toClone = $("#upload-check-table > tbody").find(".invoice-list-row.template").clone();
                    toClone.removeClass("template");
                    toClone.attr("data-target", "#invoice-no-" + batchListRow.InvoiceBatchID);
                    toClone.attr("batchID", batchListRow.InvoiceBatchID);
                    var tempDel = "<a class='deleteInvoice' invoiceID=@invoiceID hred='#' data-placement='top' rel='confirmation' data-toggle='tooltip' title='Delete Invoice'><i class='fa fa-times'></i></a>";

                    $(toClone).find(".batchno").html(batchListRow.InvoiceBatchID + tempDel.replace("@invoiceID", batchListRow.InvoiceBatchID));
                    $(toClone).find(".batchtotalcharge").html(FormatAmount(batchListRow.BatchTotalAmount));
                    $(toClone).find(".batchfrom").html(FormatDateString(batchListRow.BatchFromDate));
                    $(toClone).find(".batchto").html(FormatDateString(batchListRow.BatchToDate));
                    $(toClone).find(".created").html(FormatDateString(batchListRow.CreatedDate));

                    $("#upload-check-table > tbody").append(toClone);
                } else {
                    historyCount++;
                    var toClone = $("#history-table > tbody").find(".invoice-list-row.template").clone();
                    toClone.removeClass("template");
                    toClone.attr("data-target", "#invoice-no-" + batchListRow.InvoiceBatchID);
                    toClone.attr("batchID", batchListRow.InvoiceBatchID);

                    $(toClone).find(".batchno").html(batchListRow.InvoiceBatchID);
                    $(toClone).find(".batchtotalcharge").html(FormatAmount(batchListRow.BatchTotalAmount));
                    $(toClone).find(".batchfrom").html(FormatDateString(batchListRow.BatchFromDate));
                    $(toClone).find(".batchto").html(FormatDateString(batchListRow.BatchToDate));
                    $(toClone).find(".created").html(FormatDateString(batchListRow.CreatedDate));

                    $("#history-table > tbody").append(toClone);
                }
            });
            if (dataCount == 0) {
                $("#upload-check-table > tbody").append("<tr><td colspan=5>No Invoice Available</td></tr>");
            }
            if (historyCount == 0) {
                $("#history-table > tbody").append("<tr><td colspan=5>No Invoice Available</td></tr>");
            }
        } else {
            $("#upload-check-table > tbody").append("<tr><td colspan=5>No Invoice Available</td></tr>");
            $("#history-table > tbody").append("<tr><td colspan=5>No Invoice Available</td></tr>");
        }

        BindDeleteBatchBtn(true);

        $("#upload-check-table > tbody").find(".invoice-list-row:not(.template)").on("click", function (e) {
            GenerateUploadBatchDetail($(this))
        });

        $("#history-table > tbody").find(".invoice-list-row:not(.template)").on("click", function (e) {
            GenerateHistoryBatchDetail($(this))
        });

        $("[rel='confirmation']").tooltip();

        TriggerLoading(false);
    }

    function GenerateUploadBatchDetail(batchRow) {
        var batchID = $(batchRow).attr("batchID");
        currentBatch = batchID;
        if ($("#invoice-no-" + batchID).length == 0) {
            TriggerLoading(true);
            var newModal = $(".upload-modal.template").clone();
            $(newModal).removeClass("template");
            $(newModal).attr("id", "invoice-no-" + batchID);
            $(newModal).attr("batchid", batchID);

            $("#content").find("#modal-group").append(newModal);
            /*$("#invoice-no-" + batchID).insertBefore("#invoice-detail-modal");*/
            $.ajax({
                dataType: 'json',
                method: "GET",
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                selector: "invoice-no-" + batchID,
                data: {
                    batchID: batchID
                },
                url: "http://" + apiSite + "/api/File",
                success: function (data) {
                    CreateUploadBatchContent(data, this.selector);

                    //Turn off and on to refresh background
                    $("#invoice-no-" + batchID).on("hidden.bs.modal", function () {
                        $("#invoice-no-" + batchID).modal("show");
                        $("#invoice-no-" + batchID).unbind("hidden.bs.modal");
                    });
                    $("#invoice-no-" + batchID).modal('hide');
                },
                error: function (data) {
                    if (data.statusText == "error") {
                        ShowError("Connection Error!");
                    }
                    TriggerLoading(false);
                }
            });
        }
    }

    function GenerateHistoryBatchDetail(batchRow) {
        var batchID = $(batchRow).attr("batchID");
        currentBatch = batchID;
        if ($("#invoice-no-" + batchID).length == 0) {
            TriggerLoading(true);
            var newModal = $(".history-modal.template").clone();
            $(newModal).removeClass("template");
            $(newModal).attr("id", "invoice-no-" + batchID);
            $(newModal).attr("batchid", batchID);

            $("#content").find("#modal-group").append(newModal);
            /*$("#invoice-no-" + batchID).insertBefore("#invoice-detail-modal");*/
            $.ajax({
                dataType: 'json',
                method: "GET",
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                selector: "invoice-no-" + batchID,
                data: {
                    batchID: batchID
                },
                url: "http://" + apiSite + "/api/File",
                success: function (data) {
                    CreateHistoryBatchContent(data, this.selector);

                    //Turn off and on to refresh background
                    $("#invoice-no-" + batchID).on("hidden.bs.modal", function () {
                        $("#invoice-no-" + batchID).modal("show");
                        $("#invoice-no-" + batchID).unbind("hidden.bs.modal");
                    });
                    $("#invoice-no-" + batchID).modal('hide');
                },
                error: function (data) {
                    if (data.statusText == "error") {
                        ShowError("Connection Error!");
                    }
                    TriggerLoading(false);
                }
            });
        }
    }

    function CreateUploadBatchContent(data, selector) {
        var batchModal = $("#" + selector);
        var batchContainer = $(batchModal).find(".dashboard-group.template").clone();
        $(batchContainer).removeClass("template");

        var component = $("[data-target='#" + selector + "']");
        $(batchContainer).find(".batchID").html($(component).find(".batchno").text());
        $(batchContainer).find(".batchFrom").html($(component).find(".batchfrom").text());
        $(batchContainer).find(".batchTo").html($(component).find(".batchto").text());

        $.each(data, function (invoiceIndex, invoice) {
            var batchRow = $(batchContainer).find(".div-table-row.template").clone();
            $(batchRow).removeClass("template");

            var invoiceRow = $(batchRow).find(".withexpand.template").clone();
            $(invoiceRow).removeClass("template");
            $(invoiceRow).addClass("parent-row");
            $(invoiceRow).find(".expand-row").attr("row-type", invoice.clientType);
            $(invoiceRow).find(".expand-row").attr("isTopLevel", "true");
            if (invoice.clientType == "A") {
                $(invoiceRow).find(".clientName").addClass("alliance-class");
                $(invoiceRow).find(".clientName").html(invoice.clientName);
                $(invoiceRow).find(".expand-row").attr("data-target", ".child-alliance-row-" + invoice.clientID);
            } else if (invoice.clientType == "B") {
                $(invoiceRow).find(".clientName").addClass("billing-class");
                $(invoiceRow).find(".clientName").html(invoice.clientName);
                $(invoiceRow).find(".expand-row").attr("data-target", ".child-billing-row-" + invoice.clientID);
            } else {
                $(invoiceRow).find(".clientName").addClass("market-class");
                $(invoiceRow).find(".clientName").html(invoice.clientName);
                $(invoiceRow).find(".expand-row").attr("data-target", ".child-market-row-" + invoice.clientID);
            }
            /*if (invoice.isMissingData == 1)
                $(invoiceRow).find(".clientName").after("<span class='missing-data'></span>");*/
            if (invoice.isSecured == 1)
                $(invoiceRow).find(".clientName").after("<span class='secured-data'></span>");

            $(invoiceRow).find(".refreshFile").attr("batchID", invoice.batchID);
            $(invoiceRow).find(".refreshFile").attr("clientName", invoice.clientName);

            $(batchRow).append(invoiceRow);

            $.each(invoice.filePath, function (pathIndex, path) {
                var fileRow = $(batchRow).find(".noexpand.template").clone();
                $(fileRow).find("i").attr("batchID", invoice.batchID);
                $(fileRow).removeClass("template");
                $(fileRow).addClass("collapse");

                if (invoice.clientType == "A") {
                    $(fileRow).addClass("child-alliance-row-" + invoice.clientID);
                } else if (invoice.clientType == "B") {
                    $(fileRow).addClass("child-billing-row-" + invoice.clientID);
                } else {
                    $(fileRow).addClass("child-market-row-" + invoice.clientID);
                }

                var anchorLink = "<a target='_blank' data-placement='right' rel='previewFile' data-toggle='tooltip' title='Download File' href='" + path + "'>" + GetFileName(path) + "</a>";
                $(fileRow).find(".fileName").html(anchorLink);
                $(batchRow).append(fileRow);
            });

            //Add Upload Button
            var uploadRow = $(batchRow).find(".noexpand.template").clone();
            $(uploadRow).removeClass("template");
            $(uploadRow).addClass("collapse");

            if (invoice.clientType == "A") {
                $(uploadRow).addClass("child-alliance-row-" + invoice.clientID);
            } else if (invoice.clientType == "B") {
                $(uploadRow).addClass("child-billing-row-" + invoice.clientID);
            } else {
                $(uploadRow).addClass("child-market-row-" + invoice.clientID);
            }

            var uploadBtn = "<form action='http://" + apiSite + "/api/File' method='post' enctype='multipart/form-data' class='js-upload-form'><div class='form-inline'><div class='form-group'><input type='file' accept='.pdf,.xls,.xlsx' name='files[]' class='js-upload-files' multiple><input name=batchID type='hidden' value=@batchID><input name=clientName type='hidden' value='@clientName'></div><button type='submit' class='btn btn-sm btn-primary js-upload-submit'>Upload Custom File</button></div></form>";

            uploadBtn = uploadBtn.replace("@batchID", invoice.batchID);
            uploadBtn = uploadBtn.replace("@clientName", invoice.clientName)
            $(uploadRow).find("i").remove();
            $(uploadRow).find(".fileName").html(uploadBtn);
            $(batchRow).append(uploadRow);
            //End Add Upload Button            

            $(invoiceRow).find(".expand-row").on("click", function (e) {
                e.preventDefault();
                UpdateExpandIcon($(this), false);
            })

            $(batchContainer).find(".div-table").append(batchRow);

        });
        $(batchModal).append(batchContainer);

        $("[rel='deleteFile']").tooltip();
        $("[rel='previewFile']").tooltip();
        $("[rel='refreshFile']").tooltip();

        BindUploadSubmitEvent();
        BindUploadDeleteEvent();
        BindRefreshFilesEvent();
        BindDeliverEvent();
        TriggerLoading(false);
    }

    function CreateHistoryBatchContent(data, selector) {
        var batchModal = $("#" + selector);
        var batchContainer = $(batchModal).find(".dashboard-group.template").clone();
        $(batchContainer).removeClass("template");
        var component = $("[data-target='#" + selector + "']");
        $(batchContainer).find(".batchID").html($(component).find(".batchno").text());
        $(batchContainer).find(".batchFrom").html($(component).find(".batchfrom").text());
        $(batchContainer).find(".batchTo").html($(component).find(".batchto").text());

        $.each(data, function (invoiceIndex, invoice) {
            var batchRow = $(batchContainer).find(".div-table-row.template").clone();
            $(batchRow).removeClass("template");

            var invoiceRow = $(batchRow).find(".withexpand.template").clone();
            $(invoiceRow).removeClass("template");
            $(invoiceRow).addClass("parent-row");
            $(invoiceRow).find(".expand-row").attr("row-type", invoice.clientType);
            $(invoiceRow).find(".expand-row").attr("isTopLevel", "true");
            if (invoice.clientType == "A") {
                $(invoiceRow).find(".clientName").addClass("alliance-class");
                $(invoiceRow).find(".clientName").html(invoice.clientName);
                $(invoiceRow).find(".expand-row").attr("data-target", ".child-alliance-row-" + invoice.clientID);
            } else if (invoice.clientType == "B") {
                $(invoiceRow).find(".clientName").addClass("billing-class");
                $(invoiceRow).find(".clientName").html(invoice.clientName);
                $(invoiceRow).find(".expand-row").attr("data-target", ".child-billing-row-" + invoice.clientID);
            } else {
                $(invoiceRow).find(".clientName").addClass("market-class");
                $(invoiceRow).find(".clientName").html(invoice.clientName);
                $(invoiceRow).find(".expand-row").attr("data-target", ".child-market-row-" + invoice.clientID);
            }
            /*if (invoice.isMissingData == 1)
                $(invoiceRow).find(".clientName").after("<span class='missing-data'></span>");*/
            if (invoice.isSecured == 1)
                $(invoiceRow).find(".clientName").after("<span class='secured-data'></span>");

            $(batchRow).append(invoiceRow);

            $.each(invoice.filePath, function (pathIndex, path) {
                var fileRow = $(batchRow).find(".noexpand.template").clone();
                $(fileRow).find("i").attr("batchID", invoice.batchID);
                $(fileRow).removeClass("template");
                $(fileRow).addClass("collapse");

                if (invoice.clientType == "A") {
                    $(fileRow).addClass("child-alliance-row-" + invoice.clientID);
                } else if (invoice.clientType == "B") {
                    $(fileRow).addClass("child-billing-row-" + invoice.clientID);
                } else {
                    $(fileRow).addClass("child-market-row-" + invoice.clientID);
                }

                var anchorLink = "<a target='_blank' data-placement='right' rel='previewFile' data-toggle='tooltip' title='Download File' href='" + path + "'>" + GetFileName(path) + "</a>";
                $(fileRow).find(".fileName").html(anchorLink);
                $(batchRow).append(fileRow);
            });

            $(invoiceRow).find(".expand-row").on("click", function (e) {
                e.preventDefault();
                UpdateExpandIcon($(this), false);
            })

            $(batchContainer).find(".div-table").append(batchRow);

        });
        $(batchModal).append(batchContainer);

        $("[rel='previewFile']").tooltip();

        TriggerLoading(false);
    }

    function BindUploadDeleteEvent() {
        $(".deleteUploadFile").on("click", function (e) {
            e.preventDefault();
            TriggerLoading(true);
            var path = $(this).parent().find("span.fileName a").attr("href");
            var pathArr = path.split('/');
            var protocol = pathArr[0];
            var host = pathArr[2];
            var newPath = protocol + "//" + host + "/";
            var batchID = $(this).find("i").attr("batchID");
            var toDelete = path.replace(newPath, "");
            $.ajax({
                method: "DELETE",
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                url: "http://" + apiSite + "/api/File/?filePath=" + toDelete,
                batchID: batchID,
                success: function (data) {
                    if (data == "Success") {
                        $.ajax({
                            dataType: 'json',
                            method: "GET",
                            crossDomain: true,
                            xhrFields: {
                                withCredentials: true
                            },
                            selector: "invoice-no-" + this.batchID,
                            data: {
                                batchID: this.batchID
                            },
                            url: "http://" + apiSite + "/api/File",
                            success: function (data) {
                                var modal = $("#" + this.selector);
                                $(modal).find(".dashboard-group")[1].remove();
                                CreateUploadBatchContent(data, this.selector);
                                TriggerLoading(false);
                            },
                            error: function (data) {
                                if (data.statusText == "error") {
                                    ShowError("Connection Error!");
                                }
                                TriggerLoading(false);
                            }
                        });
                    } else {
                        TriggerLoading(false);
                    }
                },
                error: function (data) {
                    if (data.statusText == "error") {
                        ShowError("Connection Error!");
                    }
                    TriggerLoading(false);
                }
            });
        });
    }

    function BindUploadSubmitEvent() {
        $(".js-upload-form").on("submit", function (e) {
            e.preventDefault();
            var formData = new FormData($(this)[0]);

            $.ajax({
                url: $(this).attr("action"),
                method: "POST",
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                processData: false, // tell jQuery not to process the data
                contentType: false, // tell jQuery not to set contentType,
                batchID: $(this).find("input[name='batchID']").val(),
                data: formData,
                beforeSend: function () {
                    TriggerLoading(true);
                },
                success: function (data) {
                    if (data == "Success")
                        $.ajax({
                            dataType: 'json',
                            method: "GET",
                            crossDomain: true,
                            xhrFields: {
                                withCredentials: true
                            },
                            selector: "invoice-no-" + this.batchID,
                            data: {
                                batchID: this.batchID
                            },
                            url: "http://" + apiSite + "/api/File",
                            success: function (data) {
                                var modal = $("#" + this.selector);
                                $(modal).find(".dashboard-group")[1].remove();
                                CreateUploadBatchContent(data, this.selector);
                                TriggerLoading(false);
                            },
                            error: function (data) {
                                if (data.statusText == "error") {
                                    ShowError("Connection Error!");
                                }
                                TriggerLoading(false);
                            }
                        });
                    else
                        console(data);
                },
                error: function (data) {
                    if (data.statusText == "error") {
                        ShowError("Connection Error!");
                    }
                    TriggerLoading(false);
                }
            });
        });
    }

    function BindRefreshFilesEvent() {
        $(".refreshFile").on("click", function (e) {
            e.preventDefault();
            var batchID = $(this).attr("batchID");
            var clientName = $(this).attr("clientName");
            $.ajax({
                url: "http://" + apiSite + "/api/File?batchID=" + batchID + "&clientName=" + clientName,
                method: "POST",
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                batchID: batchID,
                beforeSend: function () {
                    TriggerLoading(true);
                },
                success: function (data) {
                    if (data == "Success")
                        $.ajax({
                            dataType: 'json',
                            method: "GET",
                            crossDomain: true,
                            xhrFields: {
                                withCredentials: true
                            },
                            selector: "invoice-no-" + this.batchID,
                            data: {
                                batchID: this.batchID
                            },
                            url: "http://" + apiSite + "/api/File",
                            success: function (data) {
                                var modal = $("#" + this.selector);
                                $(modal).find(".dashboard-group")[1].remove();
                                CreateUploadBatchContent(data, this.selector);
                                TriggerLoading(false);
                            },
                            error: function (data) {
                                if (data.statusText == "error") {
                                    ShowError("Connection Error!");
                                }
                                TriggerLoading(false);
                            }
                        });
                    else
                        console(data);
                },
                error: function (data) {
                    if (data.statusText == "error") {
                        ShowError("Connection Error!");
                    }
                    TriggerLoading(false);
                }
            });
        });
    }

    function BindDeliverEvent() {
        $(".deliver_invoice").on("click", function (e) {
            e.preventDefault();
            BootstrapDialog.show({
                title: 'Confirmation',
                message: 'All deliver is final, you cannot modify any file after this point.',
                type: BootstrapDialog.TYPE_PRIMARY,
                buttons: [{
                    icon: 'glyphicon glyphicon-ok',
                    label: 'Accept And Send',
                    cssClass: 'btn-success',
                    action: function (dialog) {
                        dialog.close();
                        TriggerLoading(true);
                        UpdateLoadingStatus("Status", "Processing Deliver Event! ...")
                        var handle = setInterval(GetStatusText.bind(null, currentBatch), 5000);
                        $.ajax({
                            dataType: 'json',
                            method: "POST",
                            crossDomain: true,
                            xhrFields: {
                                withCredentials: true
                            },
                            timer: handle,
                            url: "http://" + apiSite + "/api/File?batchID=" + currentBatch,
                            success: function (data) {
                                //console.log(data);
                                clearInterval(this.timer);
                                TriggerLoading(false);
                                location.reload();
                            },
                            error: function (data) {
                                if (data.statusText == "error") {
                                    ShowError("Connection Error! - " + data);
                                }
                                clearInterval(this.timer);
                                TriggerLoading(false);
                            }
                        });
                    }
            }, {
                    icon: 'glyphicon glyphicon-remove',
                    label: 'Back',
                    action: function (dialog) {
                        dialog.close();
                    }
            }]
            });
        });
    }


    var showActiveOnly = false;
    var clientProfileData;
    //DATA MANAGEMENT FUNCTIONS
    function ReloadProfileManagement() {
        $.ajax({
            dataType: 'json',
            method: "GET",
            crossDomain: true,
            xhrFields: {
                withCredentials: true
            },
            data: {
                type: "Profile"
            },
            url: "http://" + apiSite + "/api/Data",
            success: function (data) {
                clientProfileData = data;
                showActiveOnly = true;
                ChangeSwitchState("showActive", true);
                PopulateClientProfileTable(clientProfileData);
                TriggerLoading(false);
            },
            error: function (data) {
                if (data.statusText == "error") {
                    ShowError("Connection Error!");
                }
                TriggerLoading(false);
            }
        });
    }

    function PopulateClientProfileTable(data) {

        $("#client-profile-data").dataTable().fnDestroy();

        var table = $("#client-profile-data > tbody")
        $.each($(table).find("tr:not(.template)"), function (dindex, drow) {
            $(drow).remove();
        });

        var tempdata = data;
        if (showActiveOnly)
            tempdata = data.filter(function (value) {
                return value.ActiveFlag == 1;
            });

        $.each(tempdata, function (index, client) {
            var row = $(table).find(".man-row.template").clone();
            $(row).removeClass("template");

            $(row).find(".man-name").html(client.ClientName);
            $(row).find(".man-type").html(client.ClientType);

            var label = client.ClientType == "A" ? "<span class='label label-alliance' fullN='Alliance' shortN='A'>A</span>" : client.ClientType == "B" ? "<span class='label label-billing' fullN='Billing Client' shortN='B'>B</span>" : "<span class='label label-market' fullN='Market Client' shortN='M'>M</span>";

            $(row).find(".man-type").html(label);
            $(row).find(".man-type").attr("billtype", client.ClientType);

            var attention = "";

            attention = client.Attention;
            $(row).find(".man-attention").html(attention);

            var address = "";
            address = client.AddressLine1;
            if (address == "")
                address = client.AddressLine2;
            else if (client.AddressLine2 != "")
                address += "<br>" + client.AddressLine2;

            $(row).find(".man-address").html(address);
            $(row).find(".man-address").attr("address1", client.AddressLine1);
            $(row).find(".man-address").attr("address2", client.AddressLine2);

            var city = "";

            city = client.City;
            $(row).find(".man-city").html(city);

            var state = "";

            state = client.State;
            $(row).find(".man-state").html(state);

            var zip = "";

            zip = client.PostalCode;
            $(row).find(".man-zip").html(zip);

            //Split Email

            var emailList = client.Email.split(";");
            var emailFixed = "";
            $.each(emailList, function (tempIndex, emailItem) {
                emailFixed += emailItem + "<br>";
            });

            if (emailFixed == "<br>")
                emailFixed = "";

            $(row).find(".man-email").html(emailFixed);
            $(row).find(".man-email").attr("rawemail", client.Email);
            var secured = client.SecuredEmailFlag;
            if (secured == 1) {
                secured = "<span class='label label-success'>True</span>";
            } else {
                secured = "<span class='label label-default'>False</span>";
            }
            $(row).find(".man-secured").html(secured);
            $(row).find(".man-secured").attr("secured", client.SecuredEmailFlag);

            var termInt = client.Terms;
            if (termInt > 0) {
                termInt = "Net" + termInt;
            } else if (termInt == 0) {
                termInt = "DoR";
            } else {
                termInt = "";
            }

            $(row).find(".man-terms").attr("term", client.Terms);
            $(row).find(".man-terms").html(termInt);

            $(row).attr("clientID", client.ClientID);
            $(row).attr("clientType", client.ClientType);
            $(row).attr("phone", client.Phone);
            $(row).attr("isactive", client.ActiveFlag);
            $(table).append($(row));
        });

        BindProfileRowEvent();
        ReInitializeTable('#client-profile-data');
        BindActiveSwitchEvent();
        BindProfileUpdateSubmitEvent();
        BindProfileSearchEvent();

    }

    function ReInitializeTable(tablename) {
        $(tablename).dataTable().fnDestroy();
        $(tablename).DataTable({
            "bDestroy": true,
            "lengthMenu": [[20, 50, 100, -1], [20, 50, 100, "All"]]
        });
    }

    function BindProfileSearchEvent() {
        $('#client-management-pane #filter').val("");
        $('#client-management-pane #filter').trigger("keyup");
        $('#client-management-pane #filter').unbind();
        $('#client-management-pane #filter').keyup(function () {
            var filterKey = $(this);
            $(filterKey).addClass("loadinggif");

            var rex = new RegExp($(this).val().toLowerCase().replace(/  +/g, ' '), 'i');
            $('#client-management-pane .searchable tr').hide();
            $('#client-management-pane .searchable tr').filter(function () {
                return rex.test($(this).text().toLowerCase().replace(/  +/g, ' '));
            }).show();
            $(filterKey).removeClass("loadinggif");
        })
    }

    function BindProfileRowEvent() {
        $("#client-management-pane").find("tr:not(.template)").on("click", function (e) {
            e.preventDefault();

            if (!$(".tng-form-alert").hasClass("template"))
                $(".tng-form-alert").addClass("template");
            $("#profileModal").trigger("reset");
            $("#profileModal").find("input[type=text], textarea").val("");

            $("#profileModal").find("#inputClientID").val($(this).attr("clientid"));
            $("#profileModal").find("#inputClientType").val($(this).attr("clienttype"));
            $("#profileModal").find("#inputClientName").val($(this).find(".man-name").text());
            //if ($(this).find(".man-attention").text() != "Missing")
            $("#profileModal").find("#inputAttention").val($(this).find(".man-attention").text());
            if ($(this).find(".man-address").attr("address1") != "Missing")
                $("#profileModal").find("#inputAddress1").val($(this).find(".man-address").attr("address1"));
            $("#profileModal").find("#inputAddress2").val($(this).find(".man-address").attr("address2"));
            if ($(this).find(".man-city").text() != "Missing")
                $("#profileModal").find("#inputCity").val($(this).find(".man-city").text());
            if ($(this).find(".man-state").text() != "Missing")
                $("#profileModal").find("#inputState").val($(this).find(".man-state").text());
            if ($(this).find(".man-zip").text() != "Missing")
                $("#profileModal").find("#inputZipcode").val($(this).find(".man-zip").text());
            if ($(this).find(".man-email").text() != "Missing")
                $("#profileModal").find("#inputEmail").val($(this).find(".man-email").attr("rawemail"));
            $("#profileModal").find("#inputPhone").val($(this).attr("phone"));
            $("#profileModal").find("#inputTerm").val($(this).find(".man-terms").attr("term"));

            var secured = $(this).find(".man-secured").attr("secured");
            if (secured == 1)
                $("#profileModal").find("#inputSecured").prop("checked", true);
            $("#profileModal").find("#inputSecured").val(secured);

            var active = $(this).attr("isactive");
            if (active == 1)
                $("#profileModal").find("#inputActive").prop("checked", true);
            $("#profileModal").find("#inputActive").val(active);
        });

        $("#client-management-pane").find("tr:not(.template)").on("mouseenter", function (e) {
            var element = $(this).find(".man-type > .label");
            //$(element).hide();
            $(element).css("width", "100px");
            //$(element).html($(element).attr("fullN"));
            $(element).unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd");
            $(element).bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function () {
                $(element).html($(element).attr("fullN"));
                $(element).unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd");
            });
            //$(element).slideDown("fast");

        });

        $("#client-management-pane").find("tr:not(.template)").on("mouseleave", function (e) {
            var element = $(this).find(".man-type > .label");
            $(element).css("width", "30px");
            $(element).unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd");
            //$(element).html($(element).attr("shortN"));
            $(element).bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function () {
                $(element).html($(element).attr("shortN"));
                $(element).unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd");
            });
        });

        $(".profileForm").on("change", "input[type=checkbox]", function () {
            this.checked ? this.value = 1 : this.value = 0;
        });
    }

    function BindProfileUpdateSubmitEvent() {
        $(".management-submit").on("click", function (e) {
            e.preventDefault();
            var form = $(".profileForm");

            var isValid = true;
            var error = "";
            var errorCount = 0;
            $(form).find('input[type="text"], textarea').each(function () {
                if ($(this).attr("isrequired") == "true") {
                    if ($.trim($(this).val()) == '') {
                        isValid = false;
                        if (errorCount == 0)
                            error += $(this).attr("name");
                        else
                            error += ", " + $(this).attr("name");
                        errorCount++;
                    }
                }
            });

            if (isValid == true) {
                TriggerLoading(true);
                $('#profileModal').modal('toggle');
                var formData = $(form).serializeObject();

                $.ajax({
                    dataType: 'json',
                    method: "POST",
                    crossDomain: true,
                    xhrFields: {
                        withCredentials: true
                    },

                    data: formData,
                    url: "http://" + apiSite + "/api/Data",
                    success: function (data) {
                        //console.log(data);
                        ReloadProfileManagement();
                    },
                    error: function (data) {
                        if (data.statusText == "error") {
                            ShowError("Connection Error!");
                        }
                        TriggerLoading(false);
                    }
                });

            } else {
                $(".tng-form-alert").removeClass("template");
                $(".tng-form-alert").find(".alert-msg").html("Field: " + error + " is Missing");
            }
        });
    }

    function BindActiveSwitchEvent() {
        $("[name='showActive']").unbind("switchChange.bootstrapSwitch");
        $("[name='showActive']").on("switchChange.bootstrapSwitch", function (event, state) {
            TriggerLoading(true);
            showActiveOnly = state ? true : false;
            PopulateClientProfileTable(clientProfileData);
            TriggerLoading(false);
        });
    }

    function GetSearchCriteria() {

        $.ajax({
            dataType: 'json',
            method: "GET",
            crossDomain: true,
            xhrFields: {
                withCredentials: true
            },
            data: {
                type: "Relationship"
            },
            url: "http://" + apiSite + "/api/Data",
            success: function (data) {
                BindRelationFilterEvent(data);
            },
            error: function (data) {
                if (data.statusText == "error") {
                    ShowError("Connection Error!");
                }
                TriggerLoading(false);
            }
        });
    }

    var AllianceNameList;
    var BillTypeList = [{
        objID: "A",
        objName: "Alliance"
    }, {
        objID: "B",
        objName: "Billing Client"
    }, {
        objID: "M",
        objName: "Market Client"
    }];

    function BindRelationFilterEvent(data) {
        AllianceNameList = data.Alliance;
        $('#alliance-filter').selectize({
            options: AllianceNameList,
            labelField: "objName",
            valueField: "objID",
            searchField: "objName",
            maxItems: 1,
            persist: false,
            delimiter: ','
        })
        $('#billing-filter').selectize({
            options: data.BillingClient,
            labelField: "objName",
            valueField: "objID",
            searchField: "objName",
            maxItems: 1,
            persist: false,
            delimiter: ','
        });
        $('#market-filter').selectize({
            options: data.MarketClient,
            labelField: "objName",
            valueField: "objID",
            searchField: "objName",
            maxItems: 1,
            persist: false,
            delimiter: ','
        });
        $('#itemtype-filter').selectize({
            options: data.ServiceItemType,
            labelField: "objName",
            valueField: "objID",
            searchField: "objName",
            maxItems: 1,
            persist: false,
            delimiter: ','
        });
        $('#item-filter').selectize({
            options: data.ServiceItem,
            labelField: "objName",
            valueField: "objID",
            searchField: "objName",
            maxItems: 1,
            persist: false,
            delimiter: ','
        });
        var offerings = data.Offering.map(function (x) {
            return {
                accounting: x
            };
        });
        $('#offering-filter').selectize({
            options: offerings,
            labelField: "accounting",
            valueField: "accounting",
            searchField: "accounting",
            sortField: "accounting",
            maxItems: 1,
            persist: false,
            delimiter: ','
        });
        var accountings = data.AccountingType.map(function (x) {
            return {
                accounting: x
            };
        });

        $('#accounting-filter').selectize({
            options: accountings,
            maxItems: 1,
            persist: false,
            labelField: "accounting",
            valueField: "accounting",
            searchField: "accounting",
            delimiter: ',',
            onInitialize: function () {
                TriggerLoading(false);
            }
        });

        $(".relationship-filter-submit").on("click", function (e) {
            e.preventDefault();
            TriggerLoading(true);
            UpdateLoadingStatus("Status", "Retrieving Filtered Data ...");
            var formData = $("#relation-filter-form").serializeObject();
            backupFormdata = formData;
            ToggleFilterForm(false);
            ReloadRelationshipManagement();
        });

        $(".relationship-show-filter").on("click", function (e) {
            $(".relationship-show-filter").addClass("template");
            ToggleFilterForm(true);
        });
    }

    var backupFormdata;

    function ToggleFilterForm(status) {
        if (status) {
            $("#relation-filter-form").slideDown();
            $(".relationship-filter-submit").show();
            $(".relationship-show-filter").addClass("template");

        } else {
            $("#relation-filter-form").slideUp();
            $(".relationship-filter-submit").hide();
            $(".relationship-show-filter").removeClass("template");
        }
    }

    function ReloadRelationshipManagement() {
        $.ajax({
            dataType: 'json',
            method: "GET",
            crossDomain: true,
            xhrFields: {
                withCredentials: true
            },
            data: backupFormdata,
            url: "http://" + apiSite + "/api/Data",
            success: PopulateBillingRelationshipTable,
            error: function (data) {
                if (data.statusText == "error") {
                    ShowError("Connection Error!");
                }
                TriggerLoading(false);
            }
        });
    }

    var RelationshipDB = [];
    var dbcutoff = 10000;

    function FilterRelationshipTable(array) {
        var arrayCount = array.length;
        if (arrayCount > dbcutoff)
            array = array.slice(0, dbcutoff);
        $("#relationship-data").dataTable().fnDestroy();
        var table = $("#relationship-data > tbody")
        $.each($(table).find("tr:not(.template)"), function (dindex, drow) {
            $(drow).remove();
        });

        if (array.length > 0) {
            $.each(array, function (index, item) {
                var row = $(table).find(".man-row.template").clone();
                $(row).removeClass("template");
                $(row).attr("billingrelationshipid", item.BillingRelationshipID);
                $(row).find(".man-alliance").html(item.AllianceName);
                $(row).find(".man-alliance").attr("allianceID", item.AllianceID);

                $(row).find(".man-billing").html(item.BillingClientName);
                $(row).find(".man-billing").attr("billingID", item.BillingClientID);

                $(row).find(".man-market").html(item.MarketClientName);
                $(row).find(".man-market").attr("marketID", item.MarketClientID);

                $(row).find(".man-servicetype").html(item.ServiceItemType);
                $(row).find(".man-servicetype").attr("serviceTypeID", item.ServiceItemTypeID);

                $(row).find(".man-offering").html(item.OfferingDescription);
                $(row).find(".man-offering").attr("offeringID", item.OfferingID);

                $(row).find(".man-serviceitem").html(item.ServiceItemName);
                $(row).find(".man-serviceitem").attr("serviceItemID", item.ServiceItemID);

                $(row).find(".man-accounting").html(item.AccountingType);

                //var label = item.BillToIDType == "A" ? "<span class='label label-alliance'>Alliance</span>" : item.BillToIDType == "B" ? "<span class='label label-billing'>Billing Client</span>" : "<span class='label label-market'>Market Client</span>";

                var label = item.BillToIDType == "A" ? "<span class='label label-alliance' fullN='Alliance' shortN='A'>A</span>" : item.BillToIDType == "B" ? "<span class='label label-billing' fullN='Billing Client' shortN='B'>B</span>" : "<span class='label label-market' fullN='Market Client' shortN='M'>M</span>";

                $(row).find(".man-billtype").html(label);
                $(row).find(".man-billtype").attr("billtype", item.BillToIDType);

                $(table).append($(row));
            });
            $("[rel='relation-option']").tooltip();

            BindRelationshipRowEvent();

            ReInitializeTable('#relationship-data');

            BindRelationshipUpdateSubmitEvent();
        } else {
            var row = "<tr class='man-row' billingrelationshipid='' data-toggle='' ><td colspan=9>No Result Found</td></tr>";
            $(table).append(row);
        }

        TriggerLoading(false);
    }

    var timeout = undefined;

    /*function BindRelationshipSearchEvent() {
        //$('#relationship-management-pane #filter').val("");
        //$('#relationship-management-pane #filter').trigger("keyup");
        $('#relationship-management-pane #filter').unbind();
        $('#relationship-management-pane #filter').keyup(function () {
            var filterKey = $(this);
            $(filterKey).addClass("loadinggif");
            if (timeout != undefined) {
                clearTimeout(timeout);
            }
            var timeout = setTimeout(function () {
                timeout = undefined;

                var stext = $(filterKey).val();

                var checkList = [];
                if (stext.indexOf("||") >= 0) {
                    var tempArr = stext.split("||");
                    $.each(tempArr, function () {
                        if (this != "")
                            checkList.push(this);
                    });
                } else
                    checkList.push(stext);

                var grepDB = $.grep(RelationshipDB, function (v) {
                    return c_contains(v.AllianceName, checkList) || c_contains(v.BillingClientName, checkList) || c_contains(v.MarketClientName, checkList) ||
                        c_contains(v.ServiceItemType, checkList) || c_contains(v.OfferingDescription, checkList) || c_contains(v.ServiceItemName, checkList) ||
                        c_contains(v.AccountingType, checkList) || c_contains(v.ListPrice, checkList);
                });
                FilterRelationshipTable(grepDB);
                $(filterKey).removeClass("loadinggif");
            }, 1000);
        });
    }*/

    function c_contains(text1, textList) {
        var temp = text1 + "";
        if (temp != null) {
            var valid = false;
            $.each(textList, function () {
                var tempstring = this + "";
                valid = temp.toLowerCase().indexOf(tempstring.toLowerCase().trim()) >= 0;
                if (valid == true)
                    return valid;
            });
            return valid;
        } else
            return false;
    }

    function PopulateBillingRelationshipTable(data) {
        var checkTable = $("#relationship-management-pane table");
        if ($(checkTable).hasClass("template"))
            $(checkTable).removeClass("template");
        var table = $("#relationship-management-pane > table > tbody")
        $.each($(table).find("tr:not(.template)"), function (dindex, drow) {
            $(drow).remove();
        });

        RelationshipDB = data;

        var top100 = RelationshipDB.slice(0, dbcutoff);

        FilterRelationshipTable(top100);
    }

    var template;

    function BindRelationshipRowEvent() {
        $("#relationship-management-pane").find("tr:not(.template) .relation-edit").on("click", function (e) {
            e.preventDefault();
            $(".man-row-highlighter").removeClass("man-row-highlighter");
            if (!$(".tng-form-alert").hasClass("template"))
                $(".tng-form-alert").addClass("template");

            var mainRow = $(this).parent().parent();
            $(mainRow).addClass("man-row-highlighter");

            $("#relationshipModal").trigger("reset");
            $("#relationshipModal").find("#inputBillingRelationshipID").val($(mainRow).attr("billingrelationshipid"));

            $("#relationshipModal").find("#inputAllianceID").val($(mainRow).find(".man-alliance").attr("allianceID"));
            var selectize = $("#relationshipModal").find("#inputAllianceName").selectize({
                options: AllianceNameList,
                labelField: "objName",
                valueField: "objID",
                searchField: "objName",
                maxItems: 1,
                persist: false,
                delimiter: ',',
                onChange: function (value) {
                    $("#relationshipModal").find("#inputAllianceID").val(value);
                }
            })
            selectize[0].selectize.setValue($(mainRow).find(".man-alliance").attr("allianceID"), true);

            $("#relationshipModal").find("#inputBillingClientID").val($(mainRow).find(".man-billing").attr("billingID"));
            $("#relationshipModal").find("#inputBillingClientName").val($(mainRow).find(".man-billing").text());

            $("#relationshipModal").find("#inputMarketClientID").val($(mainRow).find(".man-market").attr("marketID"));
            $("#relationshipModal").find("#inputMarketClientName").val($(mainRow).find(".man-market").text());

            $("#relationshipModal").find("#inputServiceItemTypeID").val($(mainRow).find(".man-servicetype").attr("serviceTypeID"));
            $("#relationshipModal").find("#inputServiceItemType").val($(mainRow).find(".man-servicetype").text());

            $("#relationshipModal").find("#inputServiceItemID").val($(mainRow).find(".man-serviceitem").attr("serviceItemID"));
            $("#relationshipModal").find("#inputServiceItemName").val($(mainRow).find(".man-serviceitem").text());

            $("#relationshipModal").find("#inputOfferingID").val($(mainRow).find(".man-offering").attr("offeringID"));
            $("#relationshipModal").find("#inpuOffering").val($(mainRow).find(".man-offering").text());

            $("#relationshipModal").find("#inputAccountingType").val($(mainRow).find(".man-accounting").text());

            var typeSelectize = $("#relationshipModal").find("#inputBillToIDType").selectize({
                options: BillTypeList,
                maxItems: 1,
                persist: false,
                labelField: "objName",
                valueField: "objID",
                searchField: "objName",
                delimiter: ','
            });

            typeSelectize[0].selectize.setValue($(mainRow).find(".man-billtype").attr("billtype"), true);
        });

        $("#relationship-management-pane").find("tr:not(.template)").on("mouseenter", function (e) {
            var element = $(this).find(".man-billtype > .label");
            $(element).css("width", "100px");
            //$(element).html($(element).attr("fullN"));
            $(element).bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function () {
                $(element).html($(element).attr("fullN"));
                $(element).unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd");
            });

        });

        $("#relationship-management-pane").find("tr:not(.template)").on("mouseleave", function (e) {
            //$(".man-row-highlighter").removeClass("man-row-highlighter");
            var element = $(this).find(".man-billtype > .label");
            $(element).css("width", "30px");
            //$(element).html($(element).attr("shortN"));
            $(element).bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function () {
                $(element).html($(element).attr("shortN"));
                $(element).unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd");
            });

        });

        $("#relationship-management-pane").find("tr:not(.template) .relation-price").on("click", function (e) {
            e.preventDefault();
            $(".man-row-highlighter").removeClass("man-row-highlighter");
            var mainRow = $(this).parent().parent();
            $(mainRow).addClass("man-row-highlighter");
            var relationshipID = $(mainRow).attr("billingrelationshipid");
            var modal = $("#billingPriceModal");

            //Reset Status
            $(modal).find(".price-update-status").each(function (indexStatus, status) {
                if ($(status).hasClass("grayout") == false) {
                    $(status).addClass("grayout");
                }
            });

            TriggerLoading(true);
            $.ajax({
                dataType: 'json',
                method: "GET",
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                relationshipid: relationshipID,
                data: {
                    BillingRelationshipID: relationshipID
                },
                url: "http://" + apiSite + "/api/Data",
                success: function (data) {
                    PopulateBillingPriceModal(data, this.relationshipid);
                },
                error: function (data) {
                    if (data.statusText == "error") {
                        ShowError("Connection Error!");
                    }
                    TriggerLoading(false);
                }
            });

        });
    }

    function PopulateBillingPriceModal(data, relationshipid) {
        var modal = $("#billingPriceModal");
        $(modal).find(".billing-row-container").html("");
        $(modal).find("#inputBillingRelationshipID").val(relationshipid);
        $.each(data, function (index, pricerow) {
            var row = $(modal).find(".billing-price-row.template").clone();
            $(row).removeClass("template");
            $(row).find(".billing-list-price").val(pricerow.ListPrice);
            var chargedPrice = parseFloat(pricerow.ChargedPrice);
            if (chargedPrice != -9999) {
                $(row).find(".billing-charged-price").val(chargedPrice);
            }
            $(modal).find(".billing-row-container").append($(row));
        });
        BindSubmitChangePriceEvent(relationshipid);
        TriggerLoading(false);
    }

    function BindSubmitChangePriceEvent(relationshipid) {
        var modal = $("#billingPriceModal");
        $(modal).find(".submit-price").on("click", function (e) {
            TriggerLoading(true);
            UpdateLoadingStatus("Status", "Updating Price ...");
            e.preventDefault();
            var parent = $(this).parent().parent().parent().parent();
            var status = $(parent).find(".price-update-status");
            var query = "?BillingRelationshipID=" + relationshipid + "&ListPrice=@listprice&ChargedPrice=@chargedprice";
            var chargedprice = $(parent).find(".billing-charged-price").val();
            if (chargedprice == '')
                chargedprice = -9999;
            var listprice = $(parent).find(".billing-list-price").val();
            query = query.replace("@listprice", listprice).replace("@chargedprice", chargedprice);
            $.ajax({
                dataType: 'json',
                method: "POST",
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },

                status: status,
                url: "http://" + apiSite + "/api/Data" + query,
                success: function (data) {
                    //console.log(data);
                    if (data == "Success") {
                        if ($(this.status).hasClass("grayout"))
                            $(this.status).removeClass("grayout");
                    }
                    TriggerLoading(false);
                },
                error: function (data) {
                    if (data.statusText == "error") {
                        ShowError("Connection Error! " + data);
                    }
                    TriggerLoading(false);
                }
            });
            UpdateLoadingStatus("", "");
        });
    }

    function BindRelationshipUpdateSubmitEvent() {
        $(".relationship-submit").on("click", function (e) {
            e.preventDefault();
            var form = $(".relationshipForm");

            var isValid = true;
            var error = "";
            var errorCount = 0;
            $(form).find('input[type="text"], textarea').each(function () {
                if ($(this).attr("isrequired") == "true") {
                    if ($.trim($(this).val()) == '') {
                        isValid = false;
                        if (errorCount == 0)
                            error += $(this).attr("name");
                        else
                            error += ", " + $(this).attr("name");
                        errorCount++;
                    }
                }
            });

            if (isValid == true) {
                TriggerLoading(true);
                $('#relationshipModal').modal('toggle');
                var formData = $(form).serializeObject();

                $.ajax({
                    dataType: 'json',
                    method: "POST",
                    crossDomain: true,
                    xhrFields: {
                        withCredentials: true
                    },

                    data: formData,
                    url: "http://" + apiSite + "/api/Data?relationIndex=1",
                    success: function (data) {
                        ReloadRelationshipManagement();
                        $('#relationship-management-pane #filter').trigger("keyup");
                    },
                    error: function (data) {
                        if (data.statusText == "error") {
                            ShowError("Connection Error!");
                        }
                        TriggerLoading(false);
                    }
                });

            } else {
                $(".tng-form-alert").removeClass("template");
                $(".tng-form-alert").find(".alert-msg").html("Field: " + error + " is Missing");
            }
        });
    }

    //Helper Function  
    //Fix serializeArray and force it to turn Form into 1 Object
    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    function TriggerLoading(isShow) {
        if (isShow == true) {
            if (!$(".tng-alert.alert.alert-danger").hasClass("template")) {
                $(".tng-alert.alert.alert-danger").addClass("template");
                $(".tng-alert.alert.alert-danger").find(".alert-msg").html("");
            }
            $(".loading-bg").fadeIn("fast");
        } else {
            $(".loading-bg").fadeOut("fast");
        }
    }

    function ShowError(string) {
        if ($(".tng-alert.alert.alert-danger").hasClass("template")) {
            $(".tng-alert.alert.alert-danger").removeClass("template");
            $(".tng-alert.alert.alert-danger").find(".alert-msg").html("<b>Error: </b> " + string);
        }
    }

    function GetStatusText(batchID) {
        $.ajax({
            dataType: 'json',
            method: "GET",
            crossDomain: true,
            xhrFields: {
                withCredentials: true
            },
            batchID: batchID,
            data: {
                batchID: batchID,
                type: "nothing"
            },
            url: "http://" + apiSite + "/api/File",
            success: function (data) {
                UpdateLoadingStatus("Status", data);
            }
        });
    }

    function UpdateExpandIcon(current, isParent) {
        var isShow = $(current).attr("isShow");
        if (isShow == "false") {
            $(current).find("i").removeClass("fa-plus-square");
            $(current).find("i").addClass("fa-minus-square");
            $(current).attr("isShow", "true");
        } else {
            $(current).find("i").removeClass("fa-minus-square");
            $(current).find("i").addClass("fa-plus-square");
            $(current).attr("isShow", "false");
            var parent = $(current).parent().parent().parent();
            if (isParent == true) {
                var classToggle = $(current).attr("data-target");
                $.each($.find(classToggle), function (index, value) {
                    $.each($(value).find(".expand-row"), function (index2, value2) {
                        if ($(value2).attr("isShow") != "false") {
                            var selector = $(value2).attr("data-target");
                            $.each($(parent).find(selector), function (index3, value3) {
                                $(value3).collapse('hide');
                            })
                            $(value2).attr("isShow", "false");
                            $(value2).find("i").removeClass("fa-minus-square");
                            $(value2).find("i").addClass("fa-plus-square");
                        }
                    })
                });
            }

        }
    }

    function FormatAmount(nStr) {
        nStr = nStr.toFixed(2);
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return "$ " + (x1 + x2);
    }

    function FormatDateString(input) {
        return input.split("T")[0].replace(/(\d{4})-(\d{2})-(\d{2})/, "$2/$3/$1");
    }

    function GetFileName(fileName) {
        var fileNameIndex = fileName.lastIndexOf("/") + 1;
        var fixed = fileName.substr(fileNameIndex);
        var extension = fixed.replace(/^.*\./, '');
        if (extension.toLowerCase().indexOf("pdf") != -1)
            fixed = "<b>" + fixed + "</b>";
        return fixed;
    }

    function UpdateLoadingStatus(string1, string2) {
        $(".loading-bg h2").html(string1);
        $(".loading-bg .status-text").html(string2);
    }

    //Find Object Index in Array
    function FindObjectInArray(ObjName, ObjID, array, fieldName, fieldID) {
        for (var i = 0; i < array.length; i++) {
            if (array[i][fieldName] == ObjName && array[i][fieldID] == ObjID) {
                return i;
            }
        }

        return -1;
    }

    function ChangeSwitchState(name, val) {
        var toChange = $("[name='" + name + "']");
        if ($(toChange).bootstrapSwitch('state') != val) { //check if is not checked
            $(toChange).bootstrapSwitch('toggleState'); //change the checkbox state
        }
    }

    function SortByName(a, b) {
        var aName = a.MarketClientName.toLowerCase();
        var bName = b.MarketClientName.toLowerCase();
        //console.log(aName+" - "+bName);
        return ((aName > bName) ? -1 : ((aName < bName) ? 1 : 0));
    }

});