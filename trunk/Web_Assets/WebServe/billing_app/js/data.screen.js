function ReloadDataManagement() {
    $.ajax({
        dataType: 'json',
        method: "GET",
        data: {
            type: "Profile"
        },
        url: "http://" + apiSite + "/api/Data",
        success: function (data) {
            PopulateClientProfileTable(data);
            TriggerLoading(false);
        },
        error: function (data) {
            if (data.statusText == "error") {
                ShowError("Connection Error!");
            }
            TriggerLoading(false);
        }
    });
}

function PopulateClientProfileTable(data) {
    var table = $("#client-management-pane > table > tbody")
    $.each($(table).find("tr:not(.template)"), function (dindex, drow) {
        $(drow).remove();
    });


    $.each(data, function (index, client) {
        var row = $(table).find(".man-row.template").clone();
        $(row).removeClass("template");

        $(row).find(".man-name").html(client.ClientName);

        var attention = "";
        if (client.Attention == "")
            attention = "<span class='label label-warning'>Missing</span>";
        else
            attention = client.Attention;
        $(row).find(".man-attention").html(attention);

        var address = "";
        address = client.AddressLine1;
        if (address == "")
            address = client.AddressLine2;
        else if (client.AddressLine2 != "")
            address += "<br>" + client.AddressLine2;

        if (address == "")
            address = "<span class='label label-warning'>Missing</span>";

        $(row).find(".man-address").html(address);
        $(row).find(".man-address").attr("address1", client.AddressLine1);
        $(row).find(".man-address").attr("address2", client.AddressLine2);

        var city = "";
        if (client.City == "")
            city = "<span class='label label-warning'>Missing</span>";
        else
            city = client.City;
        $(row).find(".man-city").html(city);

        var state = "";
        if (client.State == "")
            state = "<span class='label label-warning'>Missing</span>";
        else
            state = client.State;
        $(row).find(".man-state").html(state);

        var zip = "";
        if (client.PostalCode == "")
            zip = "<span class='label label-warning'>Missing</span>";
        else
            zip = client.PostalCode;
        $(row).find(".man-zip").html(zip);

        //Split Email

        var emailList = client.Email.split(";");
        var emailFixed = "";
        $.each(emailList, function (tempIndex, emailItem) {
            emailFixed += emailItem + "<br>";
        });

        if (emailFixed == "<br>")
            emailFixed = "<span class='label label-warning'>Missing</span>";

        $(row).find(".man-email").html(emailFixed);
        $(row).find(".man-email").attr("rawemail", client.Email);
        var secured = client.SecuredEmailFlag;
        if (secured == 1) {
            secured = "<span class='label label-success'>True</span>";
        } else {
            secured = "<span class='label label-default'>False</span>";
        }
        $(row).find(".man-secured").html(secured);
        $(row).find(".man-secured").attr("secured", client.SecuredEmailFlag);

        $(row).find(".man-phone").html(client.Phone);
        var termInt = client.Terms;
        if ($.isNumeric(termInt) && termInt != 0) {
            termInt = "Net" + termInt;
        } else if (termInt == 0) {
            termInt = "COD";
        } else {
            termInt = "<span class='label label-warning'>Missing</span>";
        }

        $(row).find(".man-terms").attr("term", client.Terms);
        $(row).find(".man-terms").html(termInt);

        var isactive = client.ActiveFlag;
        if (isactive == 1) {
            isactive = "<span class='label label-success'>Active</span>";
        } else {
            isactive = "<span class='label label-default'>Not Active</span>";
        }
        $(row).find(".man-active").html(isactive);
        $(row).find(".man-active").attr("isactive", client.ActiveFlag);
        $(row).attr("clientID", client.ClientID);
        $(row).attr("clientType", client.ClientType);
        $(table).append($(row));
    });

    BindProfileRowEvent();
    BindProfileUpdateSubmitEvent();
    BindProfileSearchEvent();

}

function BindProfileSearchEvent() {
    $('#filter').val("");
    $('#filter').trigger("keyup");
    $('#filter').unbind();
    $('#filter').keyup(function () {

        var rex = new RegExp($(this).val(), 'i');
        $('.searchable tr').hide();
        $('.searchable tr').filter(function () {
            return rex.test($(this).text());
        }).show();

    })
}

function BindProfileRowEvent() {
    $("#client-management-pane").find("tr:not(.template)").on("click", function (e) {
        e.preventDefault();

        if (!$(".tng-form-alert").hasClass("template"))
            $(".tng-form-alert").addClass("template");
        $("#profileModal").trigger("reset");
        $("#profileModal").find("#inputClientID").val($(this).attr("clientid"));
        $("#profileModal").find("#inputClientType").val($(this).attr("clienttype"));
        $("#profileModal").find("#inputClientName").val($(this).find(".man-name").text());
        if ($(this).find(".man-attention").text() != "Missing")
            $("#profileModal").find("#inputAttention").val($(this).find(".man-attention").text());
        if ($(this).find(".man-address").attr("address1") != "Missing")
            $("#profileModal").find("#inputAddress1").val($(this).find(".man-address").attr("address1"));
        $("#profileModal").find("#inputAddress2").val($(this).find(".man-address").attr("address2"));
        if ($(this).find(".man-city").text() != "Missing")
            $("#profileModal").find("#inputCity").val($(this).find(".man-city").text());
        if ($(this).find(".man-state").text() != "Missing")
            $("#profileModal").find("#inputState").val($(this).find(".man-state").text());
        if ($(this).find(".man-zip").text() != "Missing")
            $("#profileModal").find("#inputZipcode").val($(this).find(".man-zip").text());
        if ($(this).find(".man-email").text() != "Missing")
            $("#profileModal").find("#inputEmail").val($(this).find(".man-email").attr("rawemail"));
        $("#profileModal").find("#inputPhone").val($(this).find(".man-phone").text());

        $("#profileModal").find("#inputTerm").val($(this).find(".man-terms").attr("term"));

        var secured = $(this).find(".man-secured").attr("secured");
        if (secured == 1)
            $("#profileModal").find("#inputSecured").prop("checked", true);
        $("#profileModal").find("#inputSecured").val(secured);

        var active = $(this).find(".man-active").attr("isactive");
        if (active == 1)
            $("#profileModal").find("#inputActive").prop("checked", true);
        $("#profileModal").find("#inputActive").val(active);
    });

    $(".profileForm").on("change", "input[type=checkbox]", function () {
        this.checked ? this.value = 1 : this.value = 0;
    });
}

function BindProfileUpdateSubmitEvent() {
    $(".management-submit").on("click", function (e) {
        e.preventDefault();
        var form = $(".profileForm");

        var isValid = true;
        var error = "";
        var errorCount = 0;
        $(form).find('input[type="text"], textarea').each(function () {
            if ($(this).attr("isrequired") == "true") {
                if ($.trim($(this).val()) == '') {
                    isValid = false;
                    if (errorCount == 0)
                        error += $(this).attr("name");
                    else
                        error += ", " + $(this).attr("name");
                    errorCount++;
                }
            }
        });

        if (isValid == true) {
            TriggerLoading(true);
            $('#profileModal').modal('toggle');
            var formData = $(form).serializeObject();

            $.ajax({
                dataType: 'json',
                method: "POST",
                data: formData,
                url: "http://" + apiSite + "/api/Data",
                success: function (data) {
                    //console.log(data);
                    ReloadDataManagement();
                },
                error: function (data) {
                    if (data.statusText == "error") {
                        ShowError("Connection Error!");
                    }
                    TriggerLoading(false);
                }
            });

        } else {
            $(".tng-form-alert").removeClass("template");
            $(".tng-form-alert").find(".alert-msg").html("Field: " + error + " is Missing");
        }
    });
}