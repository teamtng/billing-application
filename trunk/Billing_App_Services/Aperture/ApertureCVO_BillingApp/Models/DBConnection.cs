﻿using FastMember;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace ApertureCVO_BillingApp.Models
{
    /// <summary>
    /// Handle All DB Connection
    /// </summary>
    public class DBConnection
    {
        /// <summary>
        /// Main Connection
        /// </summary>
        public SqlConnection connection;
        /// <summary>
        /// User.Identity
        /// </summary>
        public string Username;
        /// <summary>
        /// Constructor for DB Connection
        /// </summary>
        /// <param name="Username">User.Identity from API Controller</param>
        public DBConnection(string Username)
        {
            this.Username = Username;
            connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BillingApp"].ToString());

        }

        /// <summary>
        /// For Thread Safe Function
        /// </summary>
        private static Object _lock = new Object();

        /// <summary>
        /// Check For Closed Connection and Open it - Also handle Renew Connection String if error occured
        /// </summary>
        public void ConnectionCheck()
        {
            bool lockWasTaken = false;
            try
            {
                Monitor.Enter(_lock, ref lockWasTaken);
                if (this.connection.State != ConnectionState.Open)
                {
                    try
                    {
                        if (this.connection.ConnectionString == "")
                            this.connection.ConnectionString = ConfigurationManager.ConnectionStrings["BillingApp"].ToString();
                        this.connection.Open();
                    }
                    catch
                    {
                        SqlConnection.ClearPool(this.connection);
                        this.connection = new SqlConnection(ConfigurationManager.ConnectionStrings["BillingApp"].ToString());
                        this.connection.Open();
                    }
                }
            }
            finally
            {
                if (lockWasTaken)
                {
                    Monitor.Exit(_lock);
                }
            }
        }

        #region PendingInvoice
        /// <summary>
        /// Create new batch from data range and add to Pending Invoice List
        /// </summary>
        /// <param name="dateFrom">Date in Valid Format [MM/dd/yyyy]</param>
        /// <param name="dateTo">Date in Valid Format [MM/dd/yyyy]</param>
        /// <returns></returns>
        public string GenerateBatch(string dateFrom, string dateTo)
        {
            if (CheckIfBatchExist(dateFrom, dateTo))
                return "Batch Existed For The Selected Date Range";

            var BatchID = CreateNewBatch(dateFrom, dateTo);
            var db = new DBConnection(Username);
            SqlConnection.ClearPool(db.connection);
            using (db.connection)
            {

                if (BatchID != -1)
                {
                    var query = string.Format("INSERT INTO TempInvoiceLineDetail (InvoiceBatchID, " +
                    "AllianceID,AllianceName, " +
                    "BillingClientID,BillingClientName,MarketClientID, MarketClientName, " +
                    "BillToIDType, " +
                    "BillToID, " +
                    "BillToName, " +
                    "ServiceItemTypeID, ServiceItemType,ServiceItemID,ServiceItemName, " +
                    "OfferingID,OfferingDescription,ProductTypeID, ProductType, " +
                    "RequestID,RequestDescription,ResolutionStatus,ProviderID, " +
                    "ProviderName,SSN,FacilityName," +
                    "ExternalID,Comment,LastActionDate, " +
                    "AppReceivedDate,AppCompletedDate,BillDate, " +
                    "Quantity,ListPrice,ChargedPrice, TransactionType, TransactionSource) " +
                    "SELECT " +
                    "{0} AS InvoiceBatchID, " +
                    "br.AllianceID, a.AllianceName, " +
                    "br.BillingClientID, bc.BillingClientName, br.MarketClientID, mc.MarketClientName, " +
                    "br.BillToIDType, " +
                    "CASE br.BillToIDType WHEN 'A' THEN br.AllianceID WHEN 'B' THEN br.BillingClientID WHEN 'M' THEN br.MarketClientID END AS BillToID, " +
                    "CASE br.BillToIDType WHEN 'A' THEN a.AllianceName WHEN 'B' THEN bc.BillingClientName WHEN 'M' THEN mc.MarketClientName END AS BillToName, " +
                    "br.ServiceItemTypeID, st.ServiceItemType, br.ServiceItemID, si.ServiceItemName, " +
                    "br.OfferingID, o.OfferingDescription, ri.ProductTypeID, ri.ProductType, " +
                    "stg.RequestID, ri.RequestDescription, ri.ResolutionStatus, ri.ProviderID, " +
                    "CASE ri.ProductTypeID WHEN 1 THEN ri.ProviderName ELSE NULL END AS ProviderName, " +
                    "ri.SSN, " +
                    "CASE ri.ProductTypeID WHEN 7 THEN ri.ProviderName ELSE NULL END AS FacilityName, " +
                    "stg.ExternalID, stg.Comment, " +
                    "ri.LastActionDate, " +
                    "ri.AppReceivedDate, " +
                    "ri.AppCompletedDate, " +
                    "stg.BillDate, " +
                    "stg.Quantity, " +
                    "stg.ListPrice, " +
                    "bp.ChargedPrice, " +
                    "stg.TransactionType, " +
                    "stg.TransactionSource " +
                    "FROM  " +
                    "StageInvoiceDetail stg " +
                    "LEFT JOIN RequestInfo ri ON ri.RequestID = stg.RequestID " +
                    "JOIN BillingRelationship br ON br.BillingRelationshipID = stg.BillingRelationshipID " +
                    "LEFT JOIN Alliance a ON a.AllianceID = br.AllianceID " +
                    "JOIN BillingClient bc ON bc.BillingClientID = br.BillingClientID " +
                    "JOIN MarketClient mc ON mc.MarketClientID = br.MarketClientID " +
                    "JOIN ServiceItem si ON si.ServiceItemID = br.ServiceItemID " +
                    "JOIN ServiceItemType st ON st.ServiceItemTypeID = br.ServiceItemTypeID " +
                    "JOIN Offering o ON o.OfferingID = br.OfferingID " +
                    "LEFT JOIN BillingPrice bp ON br.BillingRelationshipID = bp.BillingRelationshipID AND stg.ListPrice = bp.ListPrice " +
                    "WHERE " +
                    "stg.BillDate >= '{1}' AND stg.BillDate < '{2} 23:59:59'", BatchID, dateFrom, dateTo);

                    db.ConnectionCheck();
                    using (var command = new SqlCommand(query, db.connection))
                    {
                        try
                        {
                            command.ExecuteNonQuery();
                        }
                        catch (Exception err)
                        {
                            Debug.WriteLine(err.Message);
                            DeleteInvoice(BatchID, false);
                            return "False - " + err.Message;
                        }
                    }

                    return "Success";
                }
                else
                {
                    return "False - Batch Creation Failed";
                }
            }
        }
        /// <summary>
        /// Create New InvoiceBatch For Data Range, Return newly created Batch ID
        /// </summary>
        /// <param name="dateFrom">Date in Valid Format [MM/dd/yyyy]</param>
        /// <param name="dateTo">Date in Valid Format [MM/dd/yyyy]</param>
        /// <returns></returns>
        public int CreateNewBatch(string dateFrom, string dateTo)
        {
            var db = new DBConnection(Username);
            db.ConnectionCheck();
            using (db.connection)
            {//System.Environment.UserName

                
                string query = string.Format("INSERT INTO InvoiceBatch (BatchTotalAmount, BatchTotalAutomatedAmount, BatchTotalAdjustmentAmount" +
                            ", BatchFromDate, BatchToDate, BatchStatus, BatchCreatedDate, BatchCreatedBy) " +
                            "OUTPUT INSERTED.InvoiceBatchID VALUES({0},{1},{2},'{3}','{4} 23:59:59','{5}',convert(datetime,'{6}',101),'{7}')", 0, 0, 0,
                            dateFrom, dateTo, "Reviewing", DateTime.Now.ToString("MM/dd/yyyy"), Username);

                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    try
                    {
                        int id = (int)com.ExecuteScalar();
                        return id;
                    }
                    catch (Exception err)
                    {
                        var tempErr = err.StackTrace;
                        return -1;
                    }
                }
            }
        }
        /// <summary>
        /// Check if Batch for certain date rangeon already exist
        /// </summary>
        /// <param name="fromDate">Date in Valid Format [MM/dd/yyyy]</param>
        /// <param name="toDate">Date in Valid Format [MM/dd/yyyy]</param>
        /// <returns></returns>
        public bool CheckIfBatchExist(string fromDate, string toDate)
        {
            var db = new DBConnection(Username);
            db.ConnectionCheck();
            using (db.connection)
            {
                string query = string.Format("select BatchStatus from InvoiceBatch where BatchStatus='Reviewing' AND BatchFromDate = '{0}' and BatchToDate = '{1} 23:59:59'", fromDate, toDate);

                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    try
                    {
                        string status = (string)com.ExecuteScalar();
                        return status == null ? false : true;
                    }
                    catch (Exception err)
                    {
                        var tempErr = err.StackTrace;
                        return false;
                    }
                }
            }
        }
        /// <summary>
        /// Get Summary version for Invoice in certain date range [Obsolete]
        /// </summary>
        /// <param name="dateFrom">Date in Valid Format [MM/dd/yyyy]</param>
        /// <param name="dateTo">Date in Valid Format [MM/dd/yyyy]</param>
        /// <returns></returns>
        public List<FilteredListModel> GetSummaryList(string dateFrom, string dateTo)
        {
            var db = new DBConnection(Username);
            db.ConnectionCheck();
            var toReturn = new List<FilteredListModel>();

            using (db.connection)
            {
                var tempInvoiceList = new List<TempInvoiceDetailModel>();
                //var newTo = Convert.ToDateTime(dateTo).AddDays(1).ToString("MM/dd/yyyy");

                string query = "select AllianceID, AllianceName, BillingClientID, " +
                                "BillingClientName, MarketClientID, MarketClientName, " +
                                "ListPrice, Quantity, BillToIDType " +
                                "from StageInvoiceLineDetail " +
                                "where BillDate >= '" + dateFrom + "' AND BillDate <= '" + dateTo + " 23:59:59' ";

                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        var tempList = new List<TempInvoiceDetailModel>();
                        while (reader.Read())
                        {
                            var type = reader["BillToIDType"].ToString();
                            switch (type)
                            {
                                case "A":
                                    var tempA = tempList.SingleOrDefault(p => p.AllianceName == reader["AllianceName"].ToString());
                                    if (tempA == null)
                                    {
                                        var newA = new TempInvoiceDetailModel();
                                        newA.AllianceName = reader["AllianceName"].ToString();
                                        newA.ListPrice = Convert.ToDecimal(reader["ListPrice"]);
                                        newA.Quantity = Convert.ToInt32(reader["Quantity"]);
                                        tempList.Add(newA);

                                        var tempItem = new FilteredListModel();
                                        tempItem.ClientID = (int)reader["AllianceID"];
                                        tempItem.ClientName = reader["AllianceName"].ToString();
                                        tempItem.InvoiceTotal = newA.ListPrice * newA.Quantity;
                                        toReturn.Add(tempItem);
                                    }
                                    else
                                    {
                                        var ListPrice = Convert.ToDecimal(reader["ListPrice"]);
                                        var Quantity = Convert.ToInt32(reader["Quantity"]);
                                        //tempA.Amount += Convert.ToDecimal(reader["Amount"]);
                                        var tempItem = toReturn.Single(p => p.ClientName == reader["AllianceName"].ToString());
                                        tempItem.InvoiceTotal += (ListPrice * Quantity);
                                    }
                                    break;
                                case "B":
                                    var tempB = tempList.SingleOrDefault(p => p.BillingClientName == reader["BillingClientName"].ToString());
                                    if (tempB == null)
                                    {
                                        var newB = new TempInvoiceDetailModel();
                                        newB.BillingClientName = reader["BillingClientName"].ToString();
                                        newB.ListPrice = Convert.ToDecimal(reader["ListPrice"]);
                                        newB.Quantity = Convert.ToInt32(reader["Quantity"]);
                                        tempList.Add(newB);

                                        var tempItem = new FilteredListModel();
                                        tempItem.ClientID = (int)reader["BillingClientID"];
                                        tempItem.ClientName = reader["BillingClientName"].ToString();
                                        tempItem.InvoiceTotal = newB.ListPrice * newB.Quantity;
                                        toReturn.Add(tempItem);
                                    }
                                    else
                                    {
                                        var ListPrice = Convert.ToDecimal(reader["ListPrice"]);
                                        var Quantity = Convert.ToInt32(reader["Quantity"]);
                                        var tempItem = toReturn.Single(p => p.ClientName == reader["BillingClientName"].ToString());
                                        tempItem.InvoiceTotal += (ListPrice * Quantity);
                                    }
                                    break;
                                case "M":
                                    var tempM = tempList.SingleOrDefault(p => p.MarketClientName == reader["MarketClientName"].ToString());
                                    if (tempM == null)
                                    {
                                        var newM = new TempInvoiceDetailModel();
                                        newM.MarketClientName = reader["MarketClientName"].ToString();
                                        newM.ListPrice = Convert.ToDecimal(reader["ListPrice"]);
                                        newM.Quantity = Convert.ToInt32(reader["Quantity"]);
                                        tempList.Add(newM);

                                        var tempItem = new FilteredListModel();
                                        tempItem.ClientID = (int)reader["MarketClientID"];
                                        tempItem.ClientName = reader["MarketClientName"].ToString();
                                        tempItem.InvoiceTotal = newM.ListPrice * newM.Quantity;
                                        toReturn.Add(tempItem);
                                    }
                                    else
                                    {
                                        var ListPrice = Convert.ToDecimal(reader["ListPrice"]);
                                        var Quantity = Convert.ToInt32(reader["Quantity"]);
                                        var tempItem = toReturn.Single(p => p.ClientName == reader["MarketClientName"].ToString());
                                        tempItem.InvoiceTotal += (ListPrice * Quantity);
                                    }
                                    break;
                            }
                        }
                        //return JsonConvert.SerializeObject(toReturn.OrderBy(p => p.ProductID).ToList());
                    }
                }

                return toReturn.OrderByDescending(p => p.InvoiceTotal).ThenBy(p => p.ClientName).ToList();
            }
        }
        /// <summary>
        /// Get Batch Detail
        /// </summary>
        /// <param name="batchID">Batch ID</param>
        /// <returns></returns>
        public InvoiceBatchModel GetBatchDetail(int batchID)
        {
            var db = new DBConnection(Username);
            db.ConnectionCheck();
            var batch = new InvoiceBatchModel();
            using (db.connection)
            {
                string query = "select * from InvoiceBatch where InvoiceBatchID=" + batchID;

                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        reader.Read();

                        var tempBatch = new InvoiceBatchModel();
                        tempBatch.InvoiceBatchID = (int)reader["InvoiceBatchID"];
                        tempBatch.BatchTotalAutomatedAmount = Convert.ToDecimal(reader["BatchTotalAutomatedAmount"]);
                        tempBatch.BatchTotalAdjustmentAmount = Convert.ToDecimal(reader["BatchTotalAdjustmentAmount"]);
                        tempBatch.BatchStatus = reader["BatchStatus"].ToString();
                        tempBatch.BatchFromDate = DateTime.Parse(reader["BatchFromDate"].ToString());
                        tempBatch.BatchToDate = DateTime.Parse(reader["BatchToDate"].ToString());
                        batch = tempBatch;
                    }
                }

                batch = GenerateTempInvoiceList(batch);
                return batch;
            }
        }

        public InvoiceBatchModel GetBatchObject(int batchID)
        {
            var db = new DBConnection(Username);
            db.ConnectionCheck();
            var batch = new InvoiceBatchModel();
            using (db.connection)
            {
                string query = "select * from InvoiceBatch where InvoiceBatchID=" + batchID;

                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        reader.Read();

                        var tempBatch = new InvoiceBatchModel();
                        tempBatch.InvoiceBatchID = (int)reader["InvoiceBatchID"];
                        tempBatch.BatchTotalAutomatedAmount = Convert.ToDecimal(reader["BatchTotalAutomatedAmount"]);
                        tempBatch.BatchTotalAdjustmentAmount = Convert.ToDecimal(reader["BatchTotalAdjustmentAmount"]);
                        tempBatch.BatchStatus = reader["BatchStatus"].ToString();
                        tempBatch.BatchFromDate = DateTime.Parse(reader["BatchFromDate"].ToString());
                        tempBatch.BatchToDate = DateTime.Parse(reader["BatchToDate"].ToString());
                        batch = tempBatch;
                    }
                }

                return batch;
            }
        }
        /// <summary>
        /// Get Batch Detail that sorted by Name [Obsolete]
        /// </summary>
        /// <param name="batchID">Batch ID</param>
        /// <param name="sort">ASC or DEC</param>
        /// <returns></returns>
        public InvoiceBatchModel GetBatchDetail(int batchID, string sort)
        {
            var batch = GetBatchDetail(batchID);

            if (sort == "ASC")
                batch.TempInvoiceDetailList = batch.TempInvoiceDetailList.OrderBy(p => p.BillToName).ToList();
            else
                batch.TempInvoiceDetailList = batch.TempInvoiceDetailList.OrderByDescending(p => p.BillToName).ToList();
            return batch;
        }        
        /// <summary>
        /// Get all Batch have status as Reviewing
        /// </summary>
        /// <returns></returns>
        public List<InvoiceBatchModel> GetAllReviewBatch()
        {

            CleanUpLogging();

            var db = new DBConnection(Username);
            var returnList = new List<InvoiceBatchModel>();
            db.ConnectionCheck();
            using (db.connection)
            {
                SqlConnection.ClearPool(db.connection);
                db.ConnectionCheck();
                string query = "select * from InvoiceBatch where BatchStatus = 'Reviewing'";

                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            var tempBatch = new InvoiceBatchModel();
                            tempBatch.InvoiceBatchID = (int)reader["InvoiceBatchID"];
                            tempBatch.BatchTotalAutomatedAmount = Convert.ToDecimal(reader["BatchTotalAutomatedAmount"]);
                            tempBatch.BatchTotalAdjustmentAmount = Convert.ToDecimal(reader["BatchTotalAdjustmentAmount"]);
                            tempBatch.BatchStatus = reader["BatchStatus"].ToString();
                            tempBatch.BatchFromDate = DateTime.Parse(reader["BatchFromDate"].ToString());
                            tempBatch.BatchToDate = DateTime.Parse(reader["BatchToDate"].ToString());
                            tempBatch.CreatedDate = DateTime.Parse(reader["BatchCreatedDate"].ToString());
                            returnList.Add(tempBatch);
                        }
                    }
                }


                foreach (var batch in returnList)
                {
                    db.ConnectionCheck();
                    query = string.Format("select TransactionType, SUM(COALESCE(ChargedPrice, ListPrice)*Quantity) as Amount " +
                            "from TempInvoiceLineDetail where InvoiceBatchID={0} Group By TransactionType", batch.InvoiceBatchID);

                    using (SqlCommand com = new SqlCommand(query, db.connection))
                    {
                        using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            while (reader.Read())
                            {
                                if (reader["TransactionType"].ToString() == "Base")
                                {
                                    batch.BatchTotalAutomatedAmount = Convert.ToDecimal(reader["Amount"]);
                                }
                                else
                                {
                                    batch.BatchTotalAdjustmentAmount = Convert.ToDecimal(reader["Amount"]);
                                }
                            }
                        }
                    }

                    //batch.TempInvoiceDetailList = GenerateTempInvoiceList(batch).TempInvoiceDetailList;
                }
                return returnList;
            }
        }
        /// <summary>
        /// Generate Invoice Detail For Batch
        /// </summary>
        /// <param name="input">Empty Invoice Batch Model</param>
        /// <returns>Same Batch Model with TempInvoiceDetailList filled</returns>
        public InvoiceBatchModel GenerateTempInvoiceList(InvoiceBatchModel input)
        {
            var db = new DBConnection(Username);
            var batch = input;
            db.ConnectionCheck();
            using (db.connection)
            {

                var query = string.Format("select AllianceID,  " +
                "AllianceName, BillingClientID, BillingClientName ,MarketClientID,  " +
                "MarketClientName, TEMP.BillToIDType, TEMP.BillToID, TEMP.BillToName, COALESCE(EnableGrouping, 1) as EnableGrouping, ServiceItemTypeID,  " +
                "ServiceItemType, TransactionSource, TransactionType, SUM(COALESCE(ChargedPrice, ListPrice)*Quantity) as Amount,  " +
                "ba.AddressLine1, CAST(ba.Email AS varchar(max)) Email, ba.Terms " +
                "from TempInvoiceLineDetail TEMP " +
                "INNER JOIN DisplayOption dis on TEMP.BillToID = dis.BillToID AND TEMP.BillToIDType = dis.BillToIDType " +
                "INNER JOIN BillingAddress ba on TEMP.BillToID = ba.BillToID AND TEMP.BillToIDType = ba.BillToIDType " +
                "where InvoiceBatchID = {0}   " +
                "group by AllianceID, AllianceName, BillingClientID, BillingClientName ,MarketClientID,  " +
                "MarketClientName, TEMP.BillToIDType, TEMP.BillToID, TEMP.BillToName, EnableGrouping, ServiceItemTypeID,  " +
                "ServiceItemType, TransactionSource, TransactionType, ba.AddressLine1 " +
                ", CAST(ba.Email AS varchar(max)), ba.Terms order by TEMP.BillToName", input.InvoiceBatchID);

                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        var tempList = new List<TempInvoiceDetailModel>();
                        batch.TempInvoiceDetailList = new List<TempInvoiceDetailModel>();

                        while (reader.Read())
                        {
                            var tempInvoiceDetail = new TempInvoiceDetailModel();
                            //tempInvoiceDetail.TempInvoiceDetailLineID = (int)reader["TempInvoiceDetailLineID"];
                            tempInvoiceDetail.EnableGrouping = (int)reader["EnableGrouping"];
                            tempInvoiceDetail.InvoiceBatchID = batch.InvoiceBatchID;
                            tempInvoiceDetail.AllianceID = reader["AllianceID"] != DBNull.Value ? (int)reader["AllianceID"] : 0;
                            tempInvoiceDetail.AllianceName = reader["AllianceName"] != DBNull.Value ? reader["AllianceName"].ToString() : "";
                            tempInvoiceDetail.BillingClientID = reader["BillingClientID"] != DBNull.Value ? (int)reader["BillingClientID"] : 0;
                            tempInvoiceDetail.BillingClientName = reader["BillingClientName"] != DBNull.Value ? reader["BillingClientName"].ToString() : "";
                            tempInvoiceDetail.MarketClientID = reader["MarketClientID"] != DBNull.Value ? (int)reader["MarketClientID"] : 0;
                            tempInvoiceDetail.MarketClientName = reader["MarketClientName"] != DBNull.Value ? reader["MarketClientName"].ToString() : "";
                            tempInvoiceDetail.BillToIDType = reader["BillToIDType"].ToString();
                            //var billType = tempInvoiceDetail.BillToIDType == "A" ? "Alliance" : tempInvoiceDetail.BillToIDType == "B" ? "BillingClient" : "MarketClient";
                            tempInvoiceDetail.BillToName = reader["BillToName"].ToString();
                            //var temp = reader["BillToID"].ToString();
                            tempInvoiceDetail.BillToID = (int)reader["BillToID"];
                            tempInvoiceDetail.ServiceItemTypeID = reader["ServiceItemTypeID"] != DBNull.Value ? Convert.ToInt32(reader["ServiceItemTypeID"]) : 0;
                            tempInvoiceDetail.ServiceItemType = reader["ServiceItemType"].ToString();
                            tempInvoiceDetail.TransactionSource = reader["TransactionSource"].ToString();
                            tempInvoiceDetail.TransactionType = reader["TransactionType"].ToString();
                            tempInvoiceDetail.Quantity = 1;
                            tempInvoiceDetail.ListPrice = Convert.ToDecimal(reader["Amount"]);
                            var isMissing = 0;
                            /*if (DBNull.Value == reader["Attention"])
                                isMissing = 1;*/
                            if (DBNull.Value == reader["AddressLine1"])
                                isMissing = 1;
                            if (DBNull.Value == reader["Email"])
                                isMissing = 1;
                            if (DBNull.Value == reader["Terms"])
                                isMissing = 1;

                            tempInvoiceDetail.IsMissingData = isMissing;
                            tempList.Add(tempInvoiceDetail);
                        }

                        foreach(var client in tempList)
                        {
                            var tempDetail = new TempInvoiceDetailModel();
                            tempDetail.AllianceID = client.AllianceID;
                            tempDetail.AllianceName = client.AllianceName;
                            tempDetail.BillingClientID = client.BillingClientID;
                            tempDetail.BillingClientName = client.BillingClientName;
                            tempDetail.MarketClientID = client.MarketClientID;
                            tempDetail.MarketClientName = client.MarketClientName;
                            tempDetail.BillToIDType = client.BillToIDType;
                            tempDetail.BillToID = client.BillToID;
                            tempDetail.BillToName = client.BillToName;
                            tempDetail.InvoiceBatchID = client.InvoiceBatchID;
                            tempDetail.TransactionType = client.TransactionType;
                            tempDetail.ListPrice = client.ListPrice;
                            tempDetail.EnableGrouping = client.EnableGrouping;
                            tempDetail.IsMissingData = client.IsMissingData;
                            batch.TempInvoiceDetailList.Add(tempDetail);
                        }

                        //Get All Alliance
                        //var allianceItemList = tempList.Where(a => a.BillToIDType == "A").ToList();
                        ////var allianceNameList = allianceItemList.Select(p => new { p.AllianceName, p.AllianceID, p.BillToIDType, p.BillToID, p.BillToName, p.TransactionType }).Distinct().ToList();
                        //foreach (var alliance in allianceItemList)
                        //{
                        //    var tempDetail = new TempInvoiceDetailModel();
                        //    tempDetail.AllianceID = alliance.AllianceID;
                        //    tempDetail.AllianceName = alliance.AllianceName;
                        //    tempDetail.BillingClientName = alliance.BillingClientName;
                        //    tempDetail.MarketClientName = alliance.MarketClientName;
                        //    tempDetail.BillToIDType = alliance.BillToIDType;
                        //    tempDetail.BillToID = alliance.BillToID;
                        //    tempDetail.BillToName = alliance.BillToName;
                        //    tempDetail.InvoiceBatchID = batch.InvoiceBatchID;
                        //    tempDetail.TransactionType = alliance.TransactionType;
                        //    tempDetail.ListPrice = alliance.ListPrice;
                        //    tempDetail.EnableGrouping = alliance.EnableGrouping;
                        //    tempDetail.IsMissingData = alliance.IsMissingData;
                        //    batch.TempInvoiceDetailList.Add(tempDetail);
                        //}

                        ////Get All Break Billing Client
                        //var billingAItemList = tempList.Where(a => a.BillToIDType == "B" && a.AllianceName != "").ToList();
                        ////var billingANameList = billingAItemList.Select(p => new { p.AllianceID, p.AllianceName, p.BillingClientID, p.BillingClientName, p.BillToIDType, p.BillToID, p.BillToName, p.TransactionType }).Distinct().ToList();
                        //foreach (var billing in billingAItemList)
                        //{
                        //    var tempDetail = new TempInvoiceDetailModel();
                        //    tempDetail.AllianceID = billing.AllianceID;
                        //    tempDetail.AllianceName = billing.AllianceName;
                        //    tempDetail.BillingClientID = billing.BillingClientID;
                        //    tempDetail.BillingClientName = billing.BillingClientName;
                        //    tempDetail.MarketClientName = billing.MarketClientName;
                        //    tempDetail.BillToIDType = billing.BillToIDType;
                        //    tempDetail.BillToID = billing.BillToID;
                        //    tempDetail.BillToName = billing.BillToName;
                        //    tempDetail.InvoiceBatchID = batch.InvoiceBatchID;
                        //    tempDetail.TransactionType = billing.TransactionType;
                        //    tempDetail.ListPrice = billing.ListPrice;
                        //    tempDetail.EnableGrouping = billing.EnableGrouping;
                        //    tempDetail.IsMissingData = billing.IsMissingData;
                        //    batch.TempInvoiceDetailList.Add(tempDetail);
                        //}

                        ////Get All Billing Non Break Client
                        //var billingItemList = tempList.Where(a => a.BillToIDType == "B" && a.AllianceName == "").ToList();
                        ////var billingNameList = billingItemList.Select(p => new { p.BillingClientID, p.BillingClientName, p.BillToIDType, p.BillToID, p.BillToName, p.TransactionType }).Distinct().ToList();
                        //foreach (var billing in billingItemList)
                        //{
                        //    var tempDetail = new TempInvoiceDetailModel();
                        //    tempDetail.BillingClientID = billing.BillingClientID;
                        //    tempDetail.BillingClientName = billing.BillingClientName;
                        //    tempDetail.MarketClientName = billing.MarketClientName;
                        //    tempDetail.BillToIDType = billing.BillToIDType;
                        //    tempDetail.BillToID = billing.BillToID;
                        //    tempDetail.BillToName = billing.BillToName;
                        //    tempDetail.InvoiceBatchID = batch.InvoiceBatchID;
                        //    tempDetail.TransactionType = billing.TransactionType;
                        //    tempDetail.ListPrice = billing.ListPrice;
                        //    tempDetail.EnableGrouping = billing.EnableGrouping;
                        //    tempDetail.IsMissingData = billing.IsMissingData;
                        //    batch.TempInvoiceDetailList.Add(tempDetail);
                        //}

                        ////Get All Market withAlliance Client
                        //var marketAItemList = tempList.Where(a => a.BillToIDType == "M" && a.AllianceName != "").ToList();
                        ////var marketANameList = marketAItemList.Select(p => new { p.AllianceID, p.AllianceName, p.BillingClientID, p.BillingClientName, p.MarketClientID, p.MarketClientName, p.BillToIDType, p.BillToID, p.BillToName, p.TransactionType }).Distinct().ToList();
                        //foreach (var market in marketAItemList)
                        //{
                        //    var tempDetail = new TempInvoiceDetailModel();
                        //    tempDetail.AllianceID = market.AllianceID;
                        //    tempDetail.AllianceName = market.AllianceName;
                        //    tempDetail.BillingClientID = market.BillingClientID;
                        //    tempDetail.BillingClientName = market.BillingClientName;
                        //    tempDetail.MarketClientID = market.MarketClientID;
                        //    tempDetail.MarketClientName = market.MarketClientName;
                        //    tempDetail.BillToIDType = market.BillToIDType;
                        //    tempDetail.BillToID = market.BillToID;
                        //    tempDetail.BillToName = market.BillToName;
                        //    tempDetail.InvoiceBatchID = batch.InvoiceBatchID;
                        //    tempDetail.TransactionType = market.TransactionType;
                        //    tempDetail.ListPrice = market.ListPrice;
                        //    tempDetail.EnableGrouping = market.EnableGrouping;
                        //    tempDetail.IsMissingData = market.IsMissingData;
                        //    batch.TempInvoiceDetailList.Add(tempDetail);
                        //}

                        ////Get All Market noAlliance Client
                        //var marketItemList = tempList.Where(a => a.BillToIDType == "M" && a.AllianceName == "").ToList();
                        ////var marketNameList = marketItemList.Select(p => new { p.BillingClientID, p.BillingClientName, p.MarketClientID, p.MarketClientName, p.BillToIDType, p.BillToID, p.BillToName, p.TransactionType }).Distinct().ToList();
                        //foreach (var market in marketItemList)
                        //{
                        //    var tempDetail = new TempInvoiceDetailModel();
                        //    tempDetail.BillingClientID = market.BillingClientID;
                        //    tempDetail.BillingClientName = market.BillingClientName;
                        //    tempDetail.MarketClientID = market.MarketClientID;
                        //    tempDetail.MarketClientName = market.MarketClientName;
                        //    tempDetail.BillToIDType = market.BillToIDType;
                        //    tempDetail.BillToID = market.BillToID;
                        //    tempDetail.BillToName = market.BillToName;
                        //    tempDetail.InvoiceBatchID = batch.InvoiceBatchID;
                        //    tempDetail.TransactionType = market.TransactionType;
                        //    tempDetail.ListPrice = market.ListPrice;
                        //    tempDetail.EnableGrouping = market.EnableGrouping;
                        //    tempDetail.IsMissingData = market.IsMissingData;
                        //    batch.TempInvoiceDetailList.Add(tempDetail);
                        //}
                    }
                }
            }

            return batch;
        }
        /// <summary>
        /// Get Children for input Client
        /// </summary>
        /// <param name="batchID">Batch to check</param>
        /// <param name="clientID">Parent ID</param>
        /// <param name="allianceID">Alliance ID [if have]</param>
        /// <param name="type">Client Type</param>
        /// <param name="parenttype">Parent Type [if is child]</param>
        /// <returns>List of all Children</returns>
        public List<TempInvoiceDetailModel> GetClientDetail(int batchID, int clientID, int allianceID, string type, string parenttype)
        {
            var returnList = new List<TempInvoiceDetailModel>();
            var db = new DBConnection(Username);
            db.ConnectionCheck();
            using (db.connection)
            {
                var query = "";

                switch (type)
                {
                    case "A":
                        query = string.Format("select BillingClientID, BillingClientName, TransactionType, SUM(COALESCE(ChargedPrice, ListPrice)*Quantity) as Amount " +
                                "from dbo.TempInvoiceLineDetail " +
                                "where BillToIDType = '{0}' AND AllianceID = {1} AND InvoiceBatchID = {2} " +
                                "group by BillingClientID, BillingClientName, TransactionType", type, clientID, batchID);
                        using (SqlCommand com = new SqlCommand(query, db.connection))
                        {
                            using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                            {
                                while (reader.Read())
                                {
                                    var temp = new TempInvoiceDetailModel();
                                    temp.BillingClientID = (int)reader["BillingClientID"];
                                    temp.BillingClientName = reader["BillingClientName"].ToString();
                                    temp.TransactionType = reader["TransactionType"].ToString();
                                    temp.InvoiceBatchID = batchID;
                                    temp.Quantity = 1;
                                    temp.ListPrice = Convert.ToDecimal(reader["Amount"]);

                                    returnList.Add(temp);
                                }
                            }
                        }

                        break;
                    case "B":
                        if (parenttype == "B")
                        {
                            if (allianceID == 0)
                                query = string.Format("select MarketClientID, MarketClientName, TransactionType, SUM(COALESCE(ChargedPrice, ListPrice)*Quantity) as Amount " +
                                    "from dbo.TempInvoiceLineDetail " +
                                    "where BillToIDType = '{0}' AND BillingClientID = {1} AND InvoiceBatchID = {2} AND AllianceID IS NULL " +
                                    "group by MarketClientID, MarketClientName, TransactionType", type, clientID, batchID);
                            else
                                query = string.Format("select MarketClientID, MarketClientName, TransactionType, SUM(COALESCE(ChargedPrice, ListPrice)*Quantity) as Amount " +
                                    "from dbo.TempInvoiceLineDetail " +
                                    "where BillToIDType = '{0}' AND BillingClientID = {1} AND InvoiceBatchID = {2} AND AllianceID IS NOT NULL " +
                                    "group by MarketClientID, MarketClientName, TransactionType", type, clientID, batchID);

                        }
                        else
                        {
                            query = string.Format("select MarketClientID, MarketClientName, TransactionType, SUM(COALESCE(ChargedPrice, ListPrice)*Quantity) as Amount " +
                                    "from dbo.TempInvoiceLineDetail " +
                                    "where BillToIDType = '{0}' AND BillingClientID = {1} AND InvoiceBatchID = {2} AND AllianceID IS NOT NULL " +
                                    "group by MarketClientID, MarketClientName, TransactionType", parenttype, clientID, batchID);
                        }
                        using (SqlCommand com = new SqlCommand(query, db.connection))
                        {
                            using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                            {
                                while (reader.Read())
                                {
                                    var temp = new TempInvoiceDetailModel();
                                    temp.MarketClientID = (int)reader["MarketClientID"];
                                    temp.MarketClientName = reader["MarketClientName"].ToString();
                                    temp.TransactionType = reader["TransactionType"].ToString();
                                    temp.InvoiceBatchID = batchID;
                                    temp.Quantity = 1;
                                    temp.ListPrice = Convert.ToDecimal(reader["Amount"]);

                                    returnList.Add(temp);
                                }
                            }
                        }
                        break;
                }
                return returnList;
            }
        }
        /// <summary>
        /// Clean up file folder for deleted batch
        /// </summary>
        /// <param name="batchID">Deleted Batch ID</param>
        /// <returns>String Status</returns>
        public string CleanUpFolder(int batchID)
        {
            var ServerPath = HttpContext.Current.Server.MapPath("~/Serve/");
            var ClientFolder = "Client_Files";
            var BatchFolder = "Batch_" + batchID;
            string serverClientFolder = Path.Combine(ServerPath, ClientFolder, BatchFolder);
            EmptyFolder(new DirectoryInfo(serverClientFolder));
            try
            {
                Directory.Delete(serverClientFolder, true);
            }
            catch { }
            return "Success";
        }
        /// <summary>
        /// Delete Batch
        /// </summary>
        /// <param name="batchID">Batch to Delete</param>
        /// <param name="isUpload">Is this Delivery Pending Batch</param>
        /// <returns>String Status</returns>
        public string DeleteInvoice(int batchID, bool isUpload)
        {
            var ServerPath = HttpContext.Current.Server.MapPath("~/Serve/");
            var ClientFolder = "Client_Files";
            var BatchFolder = "Batch_" + batchID;
            string serverClientFolder = Path.Combine(ServerPath, ClientFolder, BatchFolder);
            if (isUpload)
            {
                CleanUpFolder(batchID);
            }
            ChangeBatchStatus(batchID, "Deleted");
            var db = new DBConnection(Username);
            db.ConnectionCheck();
            using (db.connection)
            {
                var query = string.Format("DELETE FROM TempInvoiceLineDetail WHERE InvoiceBatchID = {0};", batchID);

                using (var command = new SqlCommand())
                {
                    try
                    {
                        command.Connection = db.connection;
                        command.CommandText = query;
                        command.CommandType = CommandType.Text;
                        command.ExecuteNonQuery();
                        return "Success";
                    }
                    catch (Exception err)
                    {
                        Debug.WriteLine(err.Message);
                        return "False - " + err.Message;
                    }
                }
            }
        }
        /// <summary>
        /// Event triggered when any request failed
        /// </summary>
        /// <param name="batchID">Batch that trigger failed request</param>
        /// <returns>String Status</returns>
        public string CleanUpFailedRequest(int batchID)
        {
            var db = new DBConnection(Username);            
            db.ConnectionCheck();
            using (db.connection)
            {
                var query = string.Format("delete from InvoiceDetail where InvoiceNumber IN (select InvoiceNumber from InvoiceHeader where InvoiceBatchID = {0});" +
                "delete from InvoiceLine where InvoiceNumber IN (select InvoiceNumber from InvoiceHeader where InvoiceBatchID = {0});" +
                "delete from InvoiceHeader where InvoiceBatchID = {0};", batchID);

                using (var command = new SqlCommand())
                {
                    try
                    {
                        command.Connection = db.connection;
                        command.CommandTimeout = 0;
                        command.CommandText = query;
                        command.CommandType = CommandType.Text;
                        command.ExecuteNonQuery();
                        return "Success";
                    }
                    catch (Exception err)
                    {
                        Debug.WriteLine(err.Message);
                        SqlConnection.ClearPool(db.connection);
                        return "False - " + err.Message;
                    }
                }
            }
        }
        /// <summary>
        /// Get Detail Invoice for certain client
        /// </summary>
        /// <param name="batchID">Batch to check</param>
        /// <param name="clientID">Client to pull detail</param>
        /// <param name="type">Client type</param>
        /// <returns>String Status</returns>
        public InvoiceDetailViewModel GetInvoiceDetail(int batchID, int clientID, string type)
        {
            var db = new DBConnection(Username);
            db.ConnectionCheck();
            using (db.connection)
            {
                var isSet = false;
                var toReturn = new InvoiceDetailViewModel();
                toReturn.DetailList = new List<InvoiceDetailObj>();

                var idtypestring = "BillToID";

                idtypestring += "=" + clientID;

                string query = string.Format("select MarketClientID, MarketClientName ,BillToIDType, BillToID, BillToName, ServiceItemTypeID, ServiceItemType, TransactionType, TransactionSource" +
                ", SUM(COALESCE(ChargedPrice, ListPrice)*Quantity) as Amount, DO.EnableGrouping, DO.DisplayOption  " +
                "from TempInvoiceLineDetail, (SELECT DisplayOption, EnableGrouping FROM DisplayOption WHERE BillToIDType = '{1}' AND BillToID = {3}) DO  " +
                "where InvoiceBatchID={0} AND BillToIDType = '{1}' AND {2}  " +
                "group by MarketClientID, MarketClientName ,ServiceItemTypeID, ServiceItemType, TransactionType, TransactionSource, BillToIDType, BillToID, BillToName, DO.EnableGrouping, DO.DisplayOption order by MarketClientName", batchID, type, idtypestring, clientID);

                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            if(!isSet)
                            {
                                toReturn.EnableGrouping = (int)reader["EnableGrouping"];
                                toReturn.DisplayOption = reader["DisplayOption"].ToString();
                                toReturn.BillToID = clientID;
                                toReturn.BillToIDType = type;
                            }
                            var tempInvoice = new InvoiceDetailObj();
                            tempInvoice.InvoiceBatchID = batchID;
                            tempInvoice.MarketClientID = reader["MarketClientID"] != DBNull.Value ? (int)reader["MarketClientID"] : 0;
                            tempInvoice.MarketClientName = reader["MarketClientName"] != DBNull.Value ? reader["MarketClientName"].ToString() : "";
                            tempInvoice.BillToIDType = reader["BillToIDType"].ToString(); ;
                            tempInvoice.BillToID = (int)reader["BillToID"];
                            tempInvoice.BillToName = reader["BillToName"].ToString();
                            tempInvoice.ServiceItemType = reader["ServiceItemType"].ToString();
                            if (reader["ServiceItemTypeID"] != DBNull.Value)
                                tempInvoice.ServiceItemTypeID = (int)reader["ServiceItemTypeID"];
                            tempInvoice.TransactionType = reader["TransactionType"].ToString();
                            tempInvoice.TransactionSource = reader["TransactionSource"].ToString();
                            tempInvoice.Quantity = 1;
                            tempInvoice.ListPrice = Convert.ToDecimal(reader["Amount"]);
                            //tempInvoice.ChargedPrice = reader["ChargedPrice"] != DBNull.Value ? Convert.ToDecimal(reader["ChargedPrice"]) : 0;
                            toReturn.DetailList.Add(tempInvoice);
                        }
                    }
                }

                return toReturn;
            }
        }
        /// <summary>
        /// Handle changing Client Grouping and Display Option
        /// </summary>
        /// <param name="clientID">Client to change</param>
        /// <param name="type">Client type</param>
        /// <param name="toChange">DisplayOption or EnableGrouping</param>
        /// <param name="value">value index [0 or 1]</param>
        /// <returns>String Status</returns>
        public string ChangeGroupingOrDisplay(int clientID, string type, string toChange, int value)
        {
            var options = new string[] { "Service Item Type","Market Client" };

            var queryOption = toChange;
            switch(toChange)
            {
                case "DisplayOption":
                    queryOption += "='" + options[value] + "'";
                    break;
                case "EnableGrouping":
                    queryOption += "="+value + "";
                    break;
                default:
                    break;
            }

            var query = string.Format("UPDATE DisplayOption SET {2} WHERE BillToID = {0} AND BillToIDType='{1}'", clientID, type, queryOption);

            var db = new DBConnection(Username);
            db.ConnectionCheck();

            using (db.connection)
            {
                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    com.ExecuteNonQuery();
                    Logging("DisplayOption", query);
                }
            }

            return "Success";
        }
        /// <summary>
        /// Handle breaking child client from Billing or Alliance
        /// </summary>
        /// <param name="batchID">Batch to Break</param>
        /// <param name="clientID">Client to Break</param>
        /// <param name="type">Client Type</param>
        /// <param name="isCombine">Is this Combine or Break</param>
        /// <param name="AllianceID">AllianceID if have</param>
        /// <returns>String Status</returns>
        public string BreakOrCombineInvoice(int batchID, int clientID, string type, bool isCombine, int AllianceID)
        {
            var db = new DBConnection(Username);
            db.ConnectionCheck();
            using (db.connection)
            {
                var query = "";

                if (!isCombine)
                {
                    switch (type)
                    {
                        case "B":
                            query = string.Format("update dbo.TempInvoiceLineDetail " +
                                "set BillToIDType='{0}', BillToID = {1}, BillToName =  (select distinct BillingClientName FROM dbo.TempInvoiceLineDetail WHERE BillToIDType = 'A' AND BillingClientID = {1} AND InvoiceBatchID = {2}) " +
                                "where BillToIDType = 'A' AND BillingClientID = {1} AND InvoiceBatchID = {2} AND AllianceID IS NOT NULL ", type, clientID, batchID);

                            break;
                        case "M":
                            if (AllianceID == 0)
                                query = string.Format("update dbo.TempInvoiceLineDetail " +
                                    "set BillToIDType='{0}' , BillToID = {1}, BillToName =  (select distinct BillingClientName FROM dbo.TempInvoiceLineDetail WHERE BillToIDType = 'B' AND MarketClientID = {1} AND InvoiceBatchID = {2}) " +
                                    "where BillToIDType = 'B' AND MarketClientID = {1} AND InvoiceBatchID = {2} AND AllianceID IS NULL ", type, clientID, batchID);
                            else
                                query = string.Format("update dbo.TempInvoiceLineDetail " +
                                "set BillToIDType='{0}' , BillToID = {1}, BillToName =  (select distinct BillingClientName FROM dbo.TempInvoiceLineDetail WHERE BillToIDType = 'A' AND MarketClientID = {1} AND InvoiceBatchID = {2}) " +
                                "where BillToIDType = 'A' AND MarketClientID = {1} AND InvoiceBatchID = {2} AND AllianceID = {3} ", type, clientID, batchID, AllianceID);
                            break;
                    }
                }
                else
                {
                    switch (type)
                    {
                        case "B":
                            query = string.Format("update dbo.TempInvoiceLineDetail " +
                                "set BillToIDType='A' , BillToID = {3}, BillToName =  (select distinct AllianceName FROM dbo.TempInvoiceLineDetail WHERE AllianceID = {3} AND InvoiceBatchID = {2}) " +
                                "where BillToIDType = '{0}' AND BillingClientID = {1} AND InvoiceBatchID = {2} AND AllianceID IS NOT NULL ", type, clientID, batchID, AllianceID);

                            break;
                        case "M":
                            if (AllianceID == 0)
                                query = string.Format("update dbo.TempInvoiceLineDetail " +
                                    "set BillToIDType='B', BillToID = (select distinct BillingClientID FROM dbo.TempInvoiceLineDetail WHERE BillToIDType = '{0}' AND MarketClientID = {1} AND InvoiceBatchID = {2}) " +
                                    ", BillToName =  (select distinct BillingClientName FROM dbo.TempInvoiceLineDetail WHERE BillToIDType = '{0}' AND MarketClientID = {1} AND InvoiceBatchID = {2}) " +
                                    "where BillToIDType = '{0}' AND MarketClientID = {1} AND InvoiceBatchID = {2} AND AllianceID IS NULL ", type, clientID, batchID);
                            else
                                query = string.Format("update dbo.TempInvoiceLineDetail " +
                                "set BillToIDType='A' , BillToID = {3}, BillToName =  (select distinct AllianceName FROM dbo.TempInvoiceLineDetail WHERE AllianceID = {3} AND InvoiceBatchID = {2})" +
                                "where BillToIDType = '{0}' AND MarketClientID = {1} AND InvoiceBatchID = {2} AND AllianceID = {3} ", type, clientID, batchID, AllianceID);
                            break;
                    }
                }

                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    try
                    {
                        var affected = (int)com.ExecuteNonQuery();

                        if (affected == 0 && isCombine && type == "M")
                        {
                            query = string.Format("update dbo.TempInvoiceLineDetail " +
                                "set BillToIDType='A'  , BillToID = {3}, BillToName =  (select distinct AllianceName FROM dbo.TempInvoiceLineDetail WHERE AllianceID = {3} AND InvoiceBatchID = {2}) " +
                                "where BillToIDType = '{0}' AND MarketClientID = {1} AND InvoiceBatchID = {2} AND AllianceID IS NOT NULL ", type, clientID, batchID, AllianceID);
                            com.CommandText = query;
                            com.ExecuteNonQuery();
                        }
                        else if (affected == 0 && !isCombine && type == "M")
                        {
                            query = string.Format("update dbo.TempInvoiceLineDetail " +
                                "set BillToIDType='{0}'  , BillToID = {1}, BillToName =  (select distinct MarketClientName FROM dbo.TempInvoiceLineDetail WHERE MarketClientID = {1} AND InvoiceBatchID = {2}) " +
                                "where BillToIDType = 'A' AND MarketClientID = {1} AND InvoiceBatchID = {2} AND AllianceID IS NOT NULL ", type, clientID, batchID);
                            com.CommandText = query;
                            com.ExecuteNonQuery();
                        }

                    }
                    catch (Exception err)
                    {
                        return "Failed";
                    }
                }

                return "Success";
            }
        }
        /// <summary>
        /// Add new Manual / Adjustment
        /// </summary>
        /// <param name="batchID">Batch to add</param>
        /// <param name="clientID">Client to add</param>
        /// <param name="type">Client Type</param>
        /// <param name="quantity">Quantity Field [1]</param>
        /// <param name="description">Adjustment Description</param>
        /// <param name="rate">Rate / Amount</param>
        /// <returns>String Status</returns>
        public string AddNewAdjustment(int batchID, int clientID, string type, int quantity, string description, decimal rate)
        {
            var db = new DBConnection(Username);
            db.ConnectionCheck();
            using (db.connection)
            {
                var idtypestring = "BillToID";

                idtypestring += "=" + clientID;

                var query = string.Format("INSERT INTO TempInvoiceLineDetail (InvoiceBatchID, " +
                                "AllianceID,AllianceName, " +
                                "BillingClientID,BillingClientName,MarketClientID, MarketClientName, " +
                                "BillToIDType,BillToID, BillToName,ServiceItemTypeID, " +
                                "ServiceItemType,ServiceItemID,ServiceItemName, " +
                                "OfferingID,OfferingDescription,ProductTypeID, " +
                                "ProductType, " +
                                "TransactionSource, " +
                                "TransactionType,RequestID,RequestDescription, " +
                                "ProviderID,ProviderName,SSN,FacilityName, " +
                                "ExternalID,Comment,StartDate,LastActionDate, " +
                                "AppReceivedDate,AppCompletedDate,BillDate, " +
                                "Quantity,ListPrice,ChargedPrice) " +
                                "SELECT TOP 1 " + //Start the Select
                                "{0} AS InvoiceBatchID, " +
                                "null, null, " +
                                "null, null, null, null, " +
                                "BillToIDType, BillToID, BillToName, null, " +
                                "'{5}' as ServiceItemType, null, null, " +
                                "null,null,null, " +
                                "null, " +
                                "'ManualInput' as TransactionSource, " +
                                "'Adjustment' as TransactionType, null, null, " +
                                "null, null, null, null, " +
                                "null, null, null, GETDATE(), " +
                                "null, null, GETDATE(), " +
                                "{1} as Quantity, {2} as ListPrice, null " +
                                "FROM " +
                                "TempInvoiceLineDetail temp " +
                                "WHERE InvoiceBatchID= {0} AND BillToIDType = '{3}' AND {4} ", batchID, 1, rate, type, idtypestring, description);

                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    try
                    {
                        com.ExecuteNonQuery();
                        return "Success";
                    }
                    catch (Exception err)
                    {
                        return "Error - " + err.Message;
                    }
                }
            }
        }
        /// <summary>
        /// Delete Manual / Adjustment
        /// </summary>
        /// <param name="batchID">Batch to remove from</param>
        /// <param name="clientID">Client to remove from</param>
        /// <param name="clientType">Client Type</param>
        /// <param name="serviceDescription">Adjustment Description to remove</param>
        /// <returns>String Status</returns>
        public string DeleteAdjustment(int batchID, int clientID, string clientType, string serviceDescription)
        {
            var db = new DBConnection(Username);
            db.ConnectionCheck();
            using (db.connection)
            {
                var idtypestring = "BillToID";
                idtypestring += "=" + clientID;

                var query = string.Format("DELETE FROM TempInvoiceLineDetail WHERE InvoiceBatchID = {0} AND BillToIDType='{1}' AND {2} AND ServiceItemType='{3}'", batchID, clientType, idtypestring, serviceDescription);

                using (var command = new SqlCommand(query, db.connection))
                {
                    try
                    {
                        command.ExecuteNonQuery();
                        return "Success";
                    }
                    catch (Exception err)
                    {
                        Debug.WriteLine(err.Message);
                        return "False - " + err.Message;
                    }
                }
            }
        }
        /// <summary>
        /// Save CSV in Display By Market Format
        /// </summary>
        /// <param name="batchID">Batch to get Data</param>
        /// <param name="clientID">Client to get Data</param>
        /// <param name="type">Client Type</param>
        /// <param name="MarketName">Market to get Data</param>
        /// <param name="filePath">Where to save</param>
        /// <param name="isSecure">Is this secured file [remove SSN if not secure]</param>
        /// <returns>String Status</returns>
        public string SaveMarketDetail(int batchID, int clientID, string type, string MarketName, string filePath, int isSecure)
        {
            var db = new DBConnection(Username);
            db.ConnectionCheck();
            using (db.connection)
            {
                var InvoiceDetailList = new List<ExcelInvoiceModel>();
                var idtypestring = "";
                switch (type)
                {
                    case "A":
                        idtypestring = "AllianceID";
                        break;
                    case "B":
                        idtypestring = "BillingClientID";
                        break;
                    case "M":
                        idtypestring = "MarketClientID";
                        break;
                }
                idtypestring += "=" + clientID;
                string query = string.Format("select te.AllianceName, te.BillingClientName, te.MarketClientName, te.ServiceItemType, te.ServiceItemName, te.OfferingDescription, te.ProductTypeID " +
                                            ",te.ProductType, te.RequestID, te.RequestDescription, te.ProviderName, te.SSN, te.FacilityName, te.ExternalID, te.Comment, te.StartDate, te.LastActionDate, te.AppReceivedDate " +
                                            ",te.AppCompletedDate, te.BillDate, COALESCE(te.ChargedPrice, te.ListPrice) as Amount, ri.City, ri.State " +
                                            "from TempInvoiceLineDetail te, RequestInfo ri where te.RequestID = ri.RequestID AND InvoiceBatchID={0} AND BillToIDType = '{1}' AND {2} " +
                                            "AND MarketClientName = '{3}' order by MarketClientName", batchID, type, idtypestring, MarketName);

                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            var tempInvoice = new ExcelInvoiceModel();
                            tempInvoice.AllianceName = reader["AllianceName"].ToString();
                            tempInvoice.BillingClientName = reader["BillingClientName"].ToString();
                            tempInvoice.MarketClientName = reader["MarketClientName"].ToString();
                            tempInvoice.ServiceItemType = reader["ServiceItemType"].ToString();
                            tempInvoice.ServiceItemName = reader["ServiceItemName"].ToString();
                            tempInvoice.OfferingDescription = reader["OfferingDescription"].ToString();
                            if (reader["ProductTypeID"] != DBNull.Value)
                                tempInvoice.ProductTypeID = (int)reader["ProductTypeID"];
                            tempInvoice.ProductType = reader["ProductType"].ToString();
                            if (reader["RequestID"] != DBNull.Value)
                                tempInvoice.RequestID = (int)reader["RequestID"];
                            tempInvoice.RequestDescription = reader["RequestDescription"].ToString();
                            tempInvoice.ProviderName = reader["ProviderName"].ToString();
                            tempInvoice.SSN = reader["SSN"].ToString();
                            tempInvoice.FacilityName = reader["FacilityName"].ToString();
                            tempInvoice.ExternalID = reader["ExternalID"].ToString();
                            tempInvoice.Comment = reader["Comment"].ToString();
                            if (reader["StartDate"] != DBNull.Value)
                                tempInvoice.StartDate = Convert.ToDateTime(reader["StartDate"]);
                            if (reader["LastActionDate"] != DBNull.Value)
                                tempInvoice.LastActionDate = Convert.ToDateTime(reader["LastActionDate"]);
                            if (reader["AppReceivedDate"] != DBNull.Value)
                                tempInvoice.AppReceivedDate = Convert.ToDateTime(reader["AppReceivedDate"]);
                            if (reader["AppCompletedDate"] != DBNull.Value)
                                tempInvoice.AppCompletedDate = Convert.ToDateTime(reader["AppCompletedDate"]);
                            if (reader["BillDate"] != DBNull.Value)
                                tempInvoice.BillDate = Convert.ToDateTime(reader["BillDate"]);
                            tempInvoice.Amount = Convert.ToDecimal(reader["Amount"]);
                            tempInvoice.City = reader["City"].ToString();
                            tempInvoice.State = reader["State"].ToString();

                            InvoiceDetailList.Add(tempInvoice);
                        }
                    }
                }
                if (InvoiceDetailList.Count > 0)
                {
                    ExcelProcessor processor = new ExcelProcessor(MarketName, InvoiceDetailList);
                    var secured = isSecure == 1 ? true : false;
                    processor.AddServiceInfo(secured);
                    processor.AddPivotInfo();
                    processor.AddSummaryPivotInfo();
                    processor.SaveFile(filePath);
                }
                return "Success";
            }
        }
        /// <summary>
        /// Save CSV in Display By Service Type Format
        /// </summary>
        /// <param name="batchID">Batch to get Data</param>
        /// <param name="clientID">Client to get Data</param>
        /// <param name="type">Client Type</param>
        /// <param name="ServiceItemType">Service Type Name to get Data</param>
        /// <param name="filePath">Where to save</param>
        /// <param name="isSecure">Is this secured file [remove SSN if not secure]</param>
        /// <returns></returns>
        public string SaveServiceItemTypeDetail(int batchID, int clientID, string type, string ServiceItemType, string filePath, int isSecure)
        {
            var db = new DBConnection(Username);
            db.ConnectionCheck();
            using (db.connection)
            {
                var InvoiceDetailList = new List<ExcelInvoiceModel>();
                var idtypestring = "";
                switch (type)
                {
                    case "A":
                        idtypestring = "AllianceID";
                        break;
                    case "B":
                        idtypestring = "BillingClientID";
                        break;
                    case "M":
                        idtypestring = "MarketClientID";
                        break;
                }
                idtypestring += "=" + clientID;
                //string query = string.Format("select * " +
                //                            "from TempInvoiceLineDetail where InvoiceBatchID={0} AND BillToIDType = '{1}' AND {2} " +
                //                            "AND ServiceItemType = '{3}' order by ServiceItemType", batchID, type, idtypestring, ServiceItemType);

                string query = string.Format("select te.AllianceName, te.BillingClientName, te.MarketClientName, te.ServiceItemType, te.ServiceItemName, te.OfferingDescription, te.ProductTypeID " +
                                            ",te.ProductType, te.RequestID, te.RequestDescription, te.ProviderName, te.SSN, te.FacilityName, te.ExternalID, te.Comment, te.StartDate, te.LastActionDate, te.AppReceivedDate " +
                                            ",te.AppCompletedDate, te.BillDate, COALESCE(te.ChargedPrice, te.ListPrice) as Amount, ri.City, ri.State " +
                                            "from TempInvoiceLineDetail te, RequestInfo ri where te.RequestID = ri.RequestID AND InvoiceBatchID={0} AND BillToIDType = '{1}' AND {2} " +
                                            "AND ServiceItemType = '{3}' order by ServiceItemType", batchID, type, idtypestring, ServiceItemType);

                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            var tempInvoice = new ExcelInvoiceModel();
                            tempInvoice.AllianceName = reader["AllianceName"].ToString();
                            tempInvoice.BillingClientName = reader["BillingClientName"].ToString();
                            tempInvoice.MarketClientName = reader["MarketClientName"].ToString();
                            tempInvoice.ServiceItemType = reader["ServiceItemType"].ToString();
                            tempInvoice.ServiceItemName = reader["ServiceItemName"].ToString();
                            tempInvoice.OfferingDescription = reader["OfferingDescription"].ToString();
                            if (reader["ProductTypeID"] != DBNull.Value)
                                tempInvoice.ProductTypeID = (int)reader["ProductTypeID"];
                            tempInvoice.ProductType = reader["ProductType"].ToString();
                            if (reader["RequestID"] != DBNull.Value)
                                tempInvoice.RequestID = (int)reader["RequestID"];
                            tempInvoice.RequestDescription = reader["RequestDescription"].ToString();
                            tempInvoice.ProviderName = reader["ProviderName"].ToString();
                            tempInvoice.SSN = reader["SSN"].ToString();
                            tempInvoice.FacilityName = reader["FacilityName"].ToString();
                            tempInvoice.ExternalID = reader["ExternalID"].ToString();
                            tempInvoice.Comment = reader["Comment"].ToString();
                            if (reader["StartDate"] != DBNull.Value)
                                tempInvoice.StartDate = Convert.ToDateTime(reader["StartDate"]);
                            if (reader["LastActionDate"] != DBNull.Value)
                                tempInvoice.LastActionDate = Convert.ToDateTime(reader["LastActionDate"]);
                            if (reader["AppReceivedDate"] != DBNull.Value)
                                tempInvoice.AppReceivedDate = Convert.ToDateTime(reader["AppReceivedDate"]);
                            if (reader["AppCompletedDate"] != DBNull.Value)
                                tempInvoice.AppCompletedDate = Convert.ToDateTime(reader["AppCompletedDate"]);
                            if (reader["BillDate"] != DBNull.Value)
                                tempInvoice.BillDate = Convert.ToDateTime(reader["BillDate"]);
                            //if (DBNull.Value != reader["ChargedPrice"])
                            //    tempInvoice.Amount = Convert.ToDecimal(reader["ChargedPrice"]);
                            //else
                            tempInvoice.Amount = Convert.ToDecimal(reader["Amount"]);
                            tempInvoice.City = reader["City"].ToString();
                            tempInvoice.State = reader["State"].ToString();

                            InvoiceDetailList.Add(tempInvoice);
                        }
                    }
                }
                if (InvoiceDetailList.Count > 0)
                {
                    ExcelProcessor processor = new ExcelProcessor(ServiceItemType, InvoiceDetailList);

                    var secured = isSecure == 1 ? true : false;
                    processor.AddServiceInfo(secured);
                    processor.AddPivotInfo();
                    processor.AddSummaryPivotInfo();
                    processor.SaveFile(filePath);
                }
                return "Success";
            }
        }
        /// <summary>
        /// Handle preview file download in service type format
        /// </summary>
        /// <param name="batchID"></param>
        /// <param name="clientID"></param>
        /// <param name="type"></param>
        /// <param name="ServiceItemTypeID"></param>
        /// <param name="ServiceItemType"></param>
        /// <returns></returns>
        public Stream GetServiceItemTypeDetail_CSV(int batchID, int clientID, string type, int ServiceItemTypeID, string ServiceItemType)
        {
            var db = new DBConnection(Username);
            db.ConnectionCheck();
            using (db.connection)
            {
                var InvoiceDetailList = new List<ExcelInvoiceModel>();

                var idtypestring = "";
                switch (type)
                {
                    case "A":
                        idtypestring = "AllianceID";
                        break;
                    case "B":
                        idtypestring = "BillingClientID";
                        break;
                    case "M":
                        idtypestring = "MarketClientID";
                        break;
                }
                idtypestring += "=" + clientID;

                //string query = string.Format("select * " +
                //                            "from TempInvoiceLineDetail where InvoiceBatchID={0} AND BillToIDType = '{1}' AND {2} " +
                //                            "AND ServiceItemTypeID = {3} order by ServiceItemType", batchID, type, idtypestring, ServiceItemTypeID);
                string query = string.Format("select te.AllianceName, te.BillingClientName, te.MarketClientName, te.ServiceItemType, te.ServiceItemName, te.OfferingDescription, te.ProductTypeID " +
                                            ",te.ProductType, te.RequestID, te.RequestDescription, te.ProviderName, te.SSN, te.FacilityName, te.ExternalID, te.Comment, te.StartDate, te.LastActionDate, te.AppReceivedDate " +
                                            ",te.AppCompletedDate, te.BillDate, COALESCE(te.ChargedPrice, te.ListPrice) as Amount, ri.City, ri.State " +
                                            "from TempInvoiceLineDetail te, RequestInfo ri where te.RequestID = ri.RequestID AND InvoiceBatchID={0} AND BillToIDType = '{1}' AND {2} " +
                                            "AND ServiceItemTypeID = {3} order by ServiceItemType", batchID, type, idtypestring, ServiceItemTypeID);

                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {

                        while (reader.Read())
                        {
                            var tempInvoice = new ExcelInvoiceModel();
                            tempInvoice.AllianceName = reader["AllianceName"].ToString();
                            tempInvoice.BillingClientName = reader["BillingClientName"].ToString();
                            tempInvoice.MarketClientName = reader["MarketClientName"].ToString();
                            tempInvoice.ServiceItemType = reader["ServiceItemType"].ToString();
                            tempInvoice.ServiceItemName = reader["ServiceItemName"].ToString();
                            tempInvoice.OfferingDescription = reader["OfferingDescription"].ToString();
                            if (reader["ProductTypeID"] != DBNull.Value)
                                tempInvoice.ProductTypeID = (int)reader["ProductTypeID"];
                            tempInvoice.ProductType = reader["ProductType"].ToString();
                            if (reader["RequestID"] != DBNull.Value)
                                tempInvoice.RequestID = (int)reader["RequestID"];
                            tempInvoice.RequestDescription = reader["RequestDescription"].ToString();
                            tempInvoice.ProviderName = reader["ProviderName"].ToString();
                            tempInvoice.SSN = reader["SSN"].ToString();
                            tempInvoice.FacilityName = reader["FacilityName"].ToString();
                            tempInvoice.ExternalID = reader["ExternalID"].ToString();
                            tempInvoice.Comment = reader["Comment"].ToString();
                            if (reader["StartDate"] != DBNull.Value)
                                tempInvoice.StartDate = Convert.ToDateTime(reader["StartDate"]);
                            if (reader["LastActionDate"] != DBNull.Value)
                                tempInvoice.LastActionDate = Convert.ToDateTime(reader["LastActionDate"]);
                            if (reader["AppReceivedDate"] != DBNull.Value)
                                tempInvoice.AppReceivedDate = Convert.ToDateTime(reader["AppReceivedDate"]);
                            if (reader["AppCompletedDate"] != DBNull.Value)
                                tempInvoice.AppCompletedDate = Convert.ToDateTime(reader["AppCompletedDate"]);
                            if (reader["BillDate"] != DBNull.Value)
                                tempInvoice.BillDate = Convert.ToDateTime(reader["BillDate"]);
                            //if (DBNull.Value != reader["ChargedPrice"])
                            //    tempInvoice.Amount = Convert.ToDecimal(reader["ChargedPrice"]);
                            //else
                            tempInvoice.Amount = Convert.ToDecimal(reader["Amount"]);
                            tempInvoice.City = reader["City"].ToString();
                            tempInvoice.State = reader["State"].ToString();

                            InvoiceDetailList.Add(tempInvoice);
                        }
                    }
                }

                ExcelProcessor processor = new ExcelProcessor(ServiceItemType, InvoiceDetailList);
                processor.AddServiceInfo(true);
                processor.AddPivotInfo();
                processor.AddSummaryPivotInfo();
                return processor.getDataStream();
            }
        }
        /// <summary>
        /// Handle preview file download in display by market format
        /// </summary>
        /// <param name="batchID"></param>
        /// <param name="clientID"></param>
        /// <param name="type"></param>
        /// <param name="MarketClientID"></param>
        /// <param name="MarketClientName"></param>
        /// <returns></returns>
        public Stream GetMarketDetail_CSV(int batchID, int clientID, string type, int MarketClientID, string MarketClientName)
        {
            var db = new DBConnection(Username);
            db.ConnectionCheck();
            using (db.connection)
            {
                var InvoiceDetailList = new List<ExcelInvoiceModel>();

                var idtypestring = "BillToID";

                idtypestring += "=" + clientID;

                //string query = string.Format("select * " +
                //                            "from TempInvoiceLineDetail where InvoiceBatchID={0} AND BillToIDType = '{1}' AND {2} " +
                //                            "AND MarketClientID = {3} order by MarketClientName", batchID, type, idtypestring, MarketClientID);

                string query = string.Format("select te.AllianceName, te.BillingClientName, te.MarketClientName, te.ServiceItemType, te.ServiceItemName, te.OfferingDescription, te.ProductTypeID " +
                                            ",te.ProductType, te.RequestID, te.RequestDescription, te.ProviderName, te.SSN, te.FacilityName, te.ExternalID, te.Comment, te.StartDate, te.LastActionDate, te.AppReceivedDate " +
                                            ",te.AppCompletedDate, te.BillDate, COALESCE(te.ChargedPrice, te.ListPrice) as Amount, ri.City, ri.State " +
                                            "from TempInvoiceLineDetail te, RequestInfo ri where te.RequestID = ri.RequestID AND InvoiceBatchID={0} AND BillToIDType = '{1}' AND {2} " +
                                            "AND MarketClientID = {3} order by MarketClientName", batchID, type, idtypestring, MarketClientID);

                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {

                        while (reader.Read())
                        {
                            var tempInvoice = new ExcelInvoiceModel();
                            tempInvoice.AllianceName = reader["AllianceName"].ToString();
                            tempInvoice.BillingClientName = reader["BillingClientName"].ToString();
                            tempInvoice.MarketClientName = reader["MarketClientName"].ToString();
                            tempInvoice.ServiceItemType = reader["ServiceItemType"].ToString();
                            tempInvoice.ServiceItemName = reader["ServiceItemName"].ToString();
                            tempInvoice.OfferingDescription = reader["OfferingDescription"].ToString();
                            if (reader["ProductTypeID"] != DBNull.Value)
                                tempInvoice.ProductTypeID = (int)reader["ProductTypeID"];
                            tempInvoice.ProductType = reader["ProductType"].ToString();
                            if (reader["RequestID"] != DBNull.Value)
                                tempInvoice.RequestID = (int)reader["RequestID"];
                            tempInvoice.RequestDescription = reader["RequestDescription"].ToString();
                            tempInvoice.ProviderName = reader["ProviderName"].ToString();
                            tempInvoice.SSN = reader["SSN"].ToString();
                            tempInvoice.FacilityName = reader["FacilityName"].ToString();
                            tempInvoice.ExternalID = reader["ExternalID"].ToString();
                            tempInvoice.Comment = reader["Comment"].ToString();
                            if (reader["StartDate"] != DBNull.Value)
                                tempInvoice.StartDate = Convert.ToDateTime(reader["StartDate"]);
                            if (reader["LastActionDate"] != DBNull.Value)
                                tempInvoice.LastActionDate = Convert.ToDateTime(reader["LastActionDate"]);
                            if (reader["AppReceivedDate"] != DBNull.Value)
                                tempInvoice.AppReceivedDate = Convert.ToDateTime(reader["AppReceivedDate"]);
                            if (reader["AppCompletedDate"] != DBNull.Value)
                                tempInvoice.AppCompletedDate = Convert.ToDateTime(reader["AppCompletedDate"]);
                            if (reader["BillDate"] != DBNull.Value)
                                tempInvoice.BillDate = Convert.ToDateTime(reader["BillDate"]);
                            //if (DBNull.Value != reader["ChargedPrice"])
                            //    tempInvoice.Amount = Convert.ToDecimal(reader["ChargedPrice"]);
                            //else
                                tempInvoice.Amount = Convert.ToDecimal(reader["Amount"]);
                            tempInvoice.City = reader["City"].ToString();
                            tempInvoice.State = reader["State"].ToString();

                            InvoiceDetailList.Add(tempInvoice);
                        }
                    }
                }

                ExcelProcessor processor = new ExcelProcessor(MarketClientName, InvoiceDetailList);
                processor.AddServiceInfo(true);
                processor.AddPivotInfo();
                processor.AddSummaryPivotInfo();
                return processor.getDataStream();
            }
        }
        /*
        public string UpdateProductPrice(int batchID, int clientID, string type, int ServiceItemTypeID, decimal priceToChange, decimal priceChangeTo)
        {
            var db = new DBConnection(Username);
            db.ConnectionCheck();
            try
            {
                using (db.connection)
                {
                    var idtypestring = "";
                    switch (type)
                    {
                        case "A":
                            idtypestring = "AllianceID";
                            break;
                        case "B":
                            idtypestring = "BillingClientID";
                            break;
                        case "M":
                            idtypestring = "MarketClientID";
                            break;
                    }
                    idtypestring += "=" + clientID;

                    string query = string.Format("UPDATE TempInvoiceLineDetail " +
                                    "SET ChargedPrice = {5} " +
                                    "WHERE InvoiceBatchID={0}  " +
                                    "AND BillToIDType = '{1}' AND {2} " +
                                    "AND ServiceItemTypeID = {3} AND ListPrice = {4}", batchID, type, idtypestring, ServiceItemTypeID, priceToChange, priceChangeTo);

                    using (SqlCommand com = new SqlCommand(query, db.connection))
                    {
                        var affected = com.ExecuteNonQuery();
                        if (affected > 0)
                            return "Success";
                        else
                            return "No Row Found";
                    }

                }
            }
            catch (Exception err)
            {
                return "Error - " + err.Message;
            }
        }
        */
        #endregion

        #region UploadInvoice
        
        /// <summary>
        /// Row format for PDF file
        /// </summary>
        string INVOICE_ROW = "<tr class='template-data-row'><td colspan = '3'> @servicetype </td><td> @rate </td><td colspan='2'>@total</td></tr>";
        string WEBSITE = "www.aperturecvo.com";
        string EMAIL = "accounting@aperturecvo.com";

        /// <summary>
        /// Get All Delivery Pending Batch
        /// </summary>
        /// <returns></returns>
        public List<InvoiceBatchModel> GetAllUploadCheckBatch()
        {
            var db = new DBConnection(Username);
            var returnList = new List<InvoiceBatchModel>();
            db.ConnectionCheck();
            using (db.connection)
            {
                SqlConnection.ClearPool(db.connection);
                db.ConnectionCheck();
                string query = "select * from InvoiceBatch where BatchStatus IN ('UploadCheck','Delivered') AND BatchCreatedDate > DATEADD(year,-1,GETDATE())";

                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            var tempBatch = new InvoiceBatchModel();
                            tempBatch.InvoiceBatchID = (int)reader["InvoiceBatchID"];
                            tempBatch.BatchTotalAutomatedAmount = Convert.ToDecimal(reader["BatchTotalAutomatedAmount"]);
                            tempBatch.BatchTotalAdjustmentAmount = Convert.ToDecimal(reader["BatchTotalAdjustmentAmount"]);
                            tempBatch.BatchStatus = reader["BatchStatus"].ToString();
                            tempBatch.BatchFromDate = DateTime.Parse(reader["BatchFromDate"].ToString());
                            tempBatch.BatchToDate = DateTime.Parse(reader["BatchToDate"].ToString());
                            tempBatch.CreatedDate = DateTime.Parse(reader["BatchCreatedDate"].ToString());
                            returnList.Add(tempBatch);
                        }
                    }
                }

                foreach (var batch in returnList)
                {
                    db.ConnectionCheck();
                    query = string.Format("select TransactionType, SUM(COALESCE(ChargedPrice, ListPrice)*Quantity) as Amount " +
                            "from TempInvoiceLineDetail where InvoiceBatchID={0} Group By TransactionType", batch.InvoiceBatchID);

                    using (SqlCommand com = new SqlCommand(query, db.connection))
                    {
                        using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            while (reader.Read())
                            {
                                if (reader["TransactionType"].ToString() == "Base")
                                {
                                    batch.BatchTotalAutomatedAmount = Convert.ToDecimal(reader["Amount"]);
                                }
                                else
                                {
                                    batch.BatchTotalAdjustmentAmount = Convert.ToDecimal(reader["Amount"]);
                                }
                            }
                        }
                    }
                }
                return returnList;
            }
        }

        /// <summary>
        /// Get All Generated File for this Batch
        /// </summary>
        /// <param name="batchID"></param>
        /// <returns></returns>
        public List<UploadObj> GetClientUploadDetail(int batchID)
        {
            var toReturn = new List<UploadObj>();
            var ServerPhysicalPath = HttpContext.Current.Server.MapPath("~/Serve/");
            var ServerPath = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath + "Serve";
            var ClientFolder = "Client_Files";
            var BatchFolder = "Batch_" + batchID;
            string serverClientFolder = ServerPath + "/" + ClientFolder + "/" + BatchFolder;
            string serverPhysicalClientFolder = Path.Combine(ServerPhysicalPath, ClientFolder, BatchFolder);

            var db = new DBConnection(Username);
            db.ConnectionCheck();

            using (db.connection)
            {

                var query = string.Format("select head.BillToID, head.BillToName, head.BillToIDType, ba.SecuredEmailFlag " +
                "FROM InvoiceHeader head " +
                "LEFT JOIN BillingAddress ba ON ba.BillToID = head.BillToID AND ba.BillToIDType = head.BillToIDType " +
                "WHERE head.InvoiceBatchID = {0} " +
                "GROUP BY head.BillToID, head.BillToName, head.BillToIDType, ba.SecuredEmailFlag " +
                "ORDER BY head.BillToName ", batchID);

                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            var clientName = FixInvalidPath(reader["BillToName"].ToString());
                            var pathPhysical = Path.Combine(serverPhysicalClientFolder, clientName);
                            var pathCheck = serverClientFolder + "/" + clientName;
                            DirectoryInfo d = new DirectoryInfo(pathPhysical);
                            var tempUploadObj = new UploadObj();
                            tempUploadObj.batchID = batchID;
                            tempUploadObj.clientID = (int)reader["BillToID"];
                            tempUploadObj.clientName = reader["BillToName"].ToString();
                            tempUploadObj.clientType = reader["BillToIDType"].ToString();

                            tempUploadObj.isSecured = (int)reader["SecuredEmailFlag"];

                            tempUploadObj.filePath = new List<string>();

                            if (d.Exists)
                            {
                                var files = Directory.EnumerateFiles(pathPhysical, "*.*", SearchOption.TopDirectoryOnly)
                                .Where(s => s.ToLower().EndsWith(".pdf") || s.ToLower().EndsWith(".xlsx")).OrderBy(p=> Path.GetExtension(p));
                                foreach (var file in files)
                                {
                                    tempUploadObj.filePath.Add(pathCheck + "/" + Path.GetFileName(file));
                                }
                            }
                            toReturn.Add(tempUploadObj);
                        }
                    }
                }
            }

            return toReturn;
        }
        /// <summary>
        /// Get client profile for delivery info
        /// </summary>
        /// <param name="batchID"></param>
        /// <returns></returns>
        public List<ClientProfileModel> GetClientProfileForDeliver(int batchID)
        {
            var toReturn = new List<ClientProfileModel>();

            var db = new DBConnection(Username);
            db.ConnectionCheck();

            using (db.connection)
            {

                var query = string.Format("select head.BillToName, CAST(ba.Email AS varchar(max)) Email " +
                "FROM InvoiceHeader head " +
                "LEFT JOIN BillingAddress ba ON ba.BillToID = head.BillToID AND ba.BillToIDType = head.BillToIDType " +
                "WHERE head.InvoiceBatchID = {0} " +
                "GROUP BY head.BillToName, CAST(ba.Email AS varchar(max)) ", batchID);

                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            var temp = new ClientProfileModel();
                            temp.ClientName = FixInvalidPath(reader["BillToName"].ToString());
                            var clientEmail = DBNull.Value != reader["Email"] ? reader["Email"].ToString() : "";
                            temp.Email = clientEmail;
                            toReturn.Add(temp);
                        }
                    }
                }
            }

            return toReturn;
        }

        /// <summary>
        /// Initiate Delivery Process
        /// </summary>
        /// <param name="batchID"></param>
        /// <returns></returns>
        public string DeliverInvoiceToClient(int batchID)
        {
            var toReturn = new List<UploadObj>();
            var ServerPhysicalPath = HttpContext.Current.Server.MapPath("~/Serve/");
            var ClientFolder = "Client_Files";
            var BatchFolder = "Batch_" + batchID;
            var ClientList = GetClientProfileForDeliver(batchID);
            string serverPhysicalClientFolder = Path.Combine(ServerPhysicalPath, ClientFolder, BatchFolder);

            var statusFile = "Status.txt";
            var statusFilePath = Path.Combine(serverPhysicalClientFolder, statusFile);

            using (FileStream stream = new FileStream(statusFilePath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
            {
                using (TextWriter writer = new StreamWriter(stream))
                {
                    writer.Write("Initiating Delivery Process!");
                    writer.Flush();
                }
            }

            var fileIndex = 0;

            foreach(var client in ClientList)
            {
                var pathPhysical = Path.Combine(serverPhysicalClientFolder, client.ClientName);
                //var pathCheck = serverClientFolder + "/" + clientName;
                var deliverFileList = new List<string>();
                DirectoryInfo d = new DirectoryInfo(pathPhysical);
                var zipList = new List<string>();
                var zipName = Path.Combine(pathPhysical, "InvoiceDetail.zip");

                //Find all XLSX Files
                if (d.Exists)
                {
                    var files = Directory.EnumerateFiles(pathPhysical, "*.*", SearchOption.TopDirectoryOnly)
                    .Where(s => s.ToLower().EndsWith(".pdf") || s.ToLower().EndsWith(".xlsx"));
                    foreach (var file in files)
                    {
                        if (file.ToLower().EndsWith(".xlsx"))
                        {
                            zipList.Add(file);
                        }
                        else
                            deliverFileList.Add(file);
                    }
                }

                //Pack All XLSX to ZIP
                var tempZipInfo = new FileInfo(zipName);
                if (!tempZipInfo.Exists)
                {
                    ZipArchive zip = ZipFile.Open(zipName, ZipArchiveMode.Create);
                    foreach (string zipElement in zipList)
                    {
                        zip.CreateEntryFromFile(zipElement, Path.GetFileName(zipElement), CompressionLevel.Optimal);
                    }
                    zip.Dispose();
                }

                //deliverFileList.Add(zipName); //STOP SENDING XLSX FILE
                DeliverEmail(client.ClientName, client.Email, deliverFileList);
                fileIndex++;

                using (FileStream stream = new FileStream(statusFilePath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
                {
                    using (TextWriter writer = new StreamWriter(stream))
                    {
                        writer.Write("Processing Client " + fileIndex + " / " + ClientList.Count());
                        writer.Flush();
                    }
                }
            }

            ChangeBatchStatus(batchID, "Delivered");
            return "Success";
        }

        /// <summary>
        /// Send out email with attachment
        /// </summary>
        /// <param name="ClientName"></param>
        /// <param name="Email"></param>
        /// <param name="Attachments"></param>
        /// <returns></returns>
        public string DeliverEmail(string ClientName, string Email, List<string> Attachments)
        {
            try
            {
                //Backup Email - might want to change to something else
                var backupEmail = "qkhong@aperturecvo.com";

                var backupTitle = "Client: " + ClientName + " Is Missing Email Info";

                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("accounting@aperturecvo.com", "ApertureCVO");

                //var EmailList = new List<string>();

                if (Email.Contains(";"))
                {
                    var tempList = Email.Split(';');
                    foreach (var emailItem in tempList)
                    {
                        mail.To.Add(emailItem.Trim());
                    }
                }

                //If Email is empty, use backup Email
                else if(Email=="")
                {
                    mail.To.Add(backupEmail);
                }
                else
                {
                    mail.To.Add(Email.Trim());
                }

                if (Email != "")
                    mail.Subject = "Invoice Email For " + ClientName;
                else
                    mail.Subject = backupTitle;
                var content = "Look at the attachment files for more detail.";

                foreach (var attach in Attachments)
                {
                    Attachment temp = new Attachment(attach);
                    mail.Attachments.Add(temp);
                }

                mail.Body = content;

                SmtpClient SmtpServer = new SmtpClient();
                SmtpServer.Send(mail);
                return "Success";
            }
            catch (Exception err)
            {
                return "Error - " + err.Message;
            }
        }

        /// <summary>
        /// Generate Delivery File from BatchID
        /// </summary>
        /// <param name="batchID"></param>
        /// <returns></returns>
        public string SaveTempToArchive(int batchID)
        {
            var db = new DBConnection(Username);
            try
            {
                var start = DateTime.Now;
                CleanUpFailedRequest(batchID);
                CleanUpFolder(batchID);

                GenerateClientFolder(batchID);
                GenerateInvoiceHeader(batchID);
                GenerateInvoiceLine(batchID);
                GenerateInvoiceDetail(batchID);
                GenerateInvoiceFiles(batchID);
                Debug.WriteLine("---- PROCESS UPLOAD FILE TOOK " + (DateTime.Now - start).TotalMilliseconds + " Milliseconds");
                ChangeBatchStatus(batchID, "UploadCheck");                
                SqlConnection.ClearPool(db.connection);
                return "Success";
            }
            catch (Exception err)
            {
                CleanUpFailedRequest(batchID);
                SqlConnection.ClearPool(db.connection);
                return "Error - " + err.Message;
            }
        }

        public string InvoiceHeader = "";
        public string InvoiceFooter = "";
        public int fileCount = 0;
        public int totalFile = 0;
        /// <summary>
        /// Generate Invoice Files ready for deliver
        /// </summary>
        /// <param name="batchID"></param>
        /// <returns></returns>
        public string GenerateInvoiceFiles(int batchID)
        {
            var ServerPath = HttpContext.Current.Server.MapPath("~/Serve/");
            var ClientFolder = "Client_Files";
            var BatchFolder = "Batch_" + batchID;

            var InvoiceTemplate = System.IO.File.ReadAllText(Path.Combine(HttpContext.Current.Server.MapPath("~/App_Data/"), "invoice_detail.html"));

            var InvoicePart = InvoiceTemplate.Split(new string[] { "@TOSPLIT@" }, StringSplitOptions.None);
            InvoiceHeader = InvoicePart[0];
            InvoiceFooter = InvoicePart[1];

            string serverClientFolder = Path.Combine(ServerPath, ClientFolder, BatchFolder);
            var statusFile = "Status.txt";
            var statusFilePath = Path.Combine(serverClientFolder, statusFile);

            var ClientList = GetInvoiceHeader(batchID);
            var AllClientProfiles = GetAllClientInfo();

            fileCount = 0;
            totalFile = ClientList.Count();
            var tasks = Task.Factory.StartNew(() => Parallel.ForEach<InvoiceHeaderModel>(ClientList, item => ProcessHeader(serverClientFolder, batchID, item
                , AllClientProfiles.Single(p => p.ClientID == item.BillToID && p.ClientType == item.BillToIDType && p.ClientName == item.BillToName))));

            Task.WaitAll(tasks);

            return "Success";
        }
        public Object _fileLock = new Object();
        /// <summary>
        /// Process HeaderInvoiceModel into PDF file and detail into CSV
        /// </summary>
        /// <param name="serverClientFolder"></param>
        /// <param name="batchID"></param>
        /// <param name="item"></param>
        /// <param name="profile"></param>
        public void ProcessHeader(string serverClientFolder, int batchID, InvoiceHeaderModel item, ClientProfileModel profile)
        {
            string fileName = "SummaryInvoice.pdf";
            
            var toAppend = "";
            decimal grandtotal = 0;
            var start = DateTime.Now;
            if (profile.DisplayOption != "Market Client")
            {
                var serviceList = GetServiceItemTypes(batchID, item.BillToID, item.BillToIDType);
                foreach (var row in serviceList)
                {
                    grandtotal += row.ListPrice;
                    toAppend += INVOICE_ROW.Replace("@servicetype", row.ServiceItemType).Replace("@rate", FormatNumber(row.ListPrice)).Replace("@total", FormatNumber(row.ListPrice));
                    string serviceFileName = FixInvalidPath(row.ServiceItemType) + ".xlsx";
                    var xlsPath = Path.Combine(serverClientFolder, FixInvalidPath(item.BillToName), serviceFileName);
                    SaveServiceItemTypeDetail(batchID, row.BillToID, row.BillToIDType, row.ServiceItemType, xlsPath, profile.SecuredEmailFlag);
                }
            }
            else
            {
                var marketList = GetShowByMarket(batchID, item.BillToID, item.BillToIDType, false);
                var manualList = GetShowByMarket(batchID, item.BillToID, item.BillToIDType, true);
                foreach (var row in marketList)
                {
                    grandtotal += row.ListPrice;
                    toAppend += INVOICE_ROW.Replace("@servicetype", row.MarketClientName).Replace("@rate", FormatNumber(row.ListPrice)).Replace("@total", FormatNumber(row.ListPrice));
                    string serviceFileName = FixInvalidPath(row.MarketClientName) + ".xlsx";
                    var xlsPath = Path.Combine(serverClientFolder, FixInvalidPath(item.BillToName), serviceFileName);
                    SaveMarketDetail(batchID, item.BillToID, item.BillToIDType, row.MarketClientName, xlsPath, profile.SecuredEmailFlag);
                }
                foreach (var row in manualList)
                {
                    grandtotal += row.ListPrice;
                    toAppend += INVOICE_ROW.Replace("@servicetype", row.MarketClientName).Replace("@rate", FormatNumber(row.ListPrice)).Replace("@total", FormatNumber(row.ListPrice));
                }
            }
            DateTime today = DateTime.Today;
            DateTime endOfMonth = new DateTime(today.Year,
                                               today.Month,
                                               DateTime.DaysInMonth(today.Year,
                                                                    today.Month));
            var batchObject = GetBatchObject(batchID);
            var TempHeader = InvoiceHeader.Replace("@today", batchObject.BatchToDate.ToString("MM/dd/yyyy"));
            TempHeader = TempHeader.Replace("@invoiceno", item.InvoiceNumber + "");
            TempHeader = TempHeader.Replace("@email", EMAIL).Replace("@website", WEBSITE);
            TempHeader = TempHeader.Replace("@clientname", profile.ClientName).Replace("@attention", profile.Attention);

            var address = profile.AddressLine1;
            if (profile.AddressLine2 != "")
                address += "<br>" + profile.AddressLine2;
            TempHeader = TempHeader.Replace("@address", address).Replace("@city", profile.City).Replace("@state", profile.State).Replace("@zip", profile.PostalCode);
            /*DateTime createDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month,
                                    DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));*/
            var termInt = profile.Terms;
            var term = "";
            if (termInt > 0)
                term = "Net " + termInt;
            else term = "Due on Receipt";
            TempHeader = TempHeader.Replace("@accountno", "").Replace("@pono", "").Replace("@term", term);

            //var duedate = DateTime.Now;
            var duedate = batchObject.BatchToDate.AddDays(termInt);
            if (term != "Due on Receipt")
                TempHeader = TempHeader.Replace("@duedate", duedate.ToString("MM/dd/yyyy"));

            var TempFooter = InvoiceFooter.Replace("@grandtotal", FormatNumber(grandtotal));

            var PDF_FORMAT = TempHeader + toAppend + TempFooter;
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            htmlToPdf.CustomWkHtmlArgs = " --window-status READY ";
            var pdfBytes = htmlToPdf.GeneratePdf(PDF_FORMAT);
            File.WriteAllBytes(Path.Combine(serverClientFolder, FixInvalidPath(item.BillToName), fileName), pdfBytes);

            if(profile.ClientName.StartsWith("Alliance Healthcare Administrative Solutions") && profile.ClientType == "A")
            {
                var component = new List<string>();
                var ADD_PDF = GetAHASAdditionalInvoice(batchObject);
                var additionalFileName = "SummaryInvoice – HPHC CT.pdf";
                ADD_PDF = ADD_PDF.Replace("@invoiceno", item.InvoiceNumber + "");
                var ADD_PDF_BYTES = htmlToPdf.GeneratePdf(ADD_PDF);
                File.WriteAllBytes(Path.Combine(serverClientFolder, FixInvalidPath(item.BillToName), additionalFileName), ADD_PDF_BYTES);
            }

            lock (_fileLock)
            {
                fileCount++;
                Debug.WriteLine("\t\t -- FILE : " + fileCount + " / " + totalFile + " - took " + (DateTime.Now - start).TotalMilliseconds + " Milliseconds");
                var statusFile = "Status.txt";
                var statusFilePath = Path.Combine(serverClientFolder, statusFile);
                //try {
                using (FileStream stream = new FileStream(statusFilePath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
                {
                    using (TextWriter writer = new StreamWriter(stream))
                    {
                        writer.Write("Processing File " + fileCount + " / " + totalFile);
                        writer.Flush();
                    }
                }
                //} catch { }
            }
        }


        public string GetAHASAdditionalInvoice(InvoiceBatchModel batchID)
        {
            var toReturn = new List<InvoiceDetailObj>();
            var query = string.Format("select  " +
            "temp.MarketClientName as BillToName, " +
            "ba.Attention, " +
            "ba.AddressLine1, " +
            "ba.AddressLine2, " +
            "ba.City, " +
            "ba.State, " +
            "ba.PostalCode, " +
            "ba.Terms, " +
            "temp.ServiceItemType, " +
            "SUM(temp.ListPrice) AS Amount " +
            "from  " +
            "TempInvoiceLineDetail temp " +
            "join Alliance a on temp.BillToID = a.AllianceID " +
            "join MarketClient mc on temp.MarketClientID = mc.MarketClientID " +
            "join BillingAddress ba on temp.MarketClientID = ba.BillToID and ba.BillToIDType = 'M' " +
            "where  " +
            "a.AllianceSourceID = '9127342' " +
            "and mc.MarketClientSourceID = '31102558' " +
            "and InvoiceBatchID = {0} " +
            "group by " +
            "temp.MarketClientName, ba.Attention, ba.AddressLine1, ba.AddressLine2, " +
            "ba.City, ba.State, ba.PostalCode, ba.Terms, temp.ServiceItemType ", batchID.InvoiceBatchID);

            var getAddress = false;
            var additionalProfile = new ClientProfileModel();
            var db = new DBConnection(Username);
            db.ConnectionCheck();
            using (db.connection)
            {
                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            if(!getAddress)
                            {
                                getAddress = true;
                                additionalProfile.ClientName = reader["BillToName"].ToString();
                                additionalProfile.Attention = reader["Attention"].ToString();
                                additionalProfile.AddressLine1 = reader["AddressLine1"].ToString();
                                additionalProfile.AddressLine2 = reader["AddressLine2"].ToString();
                                additionalProfile.City = reader["City"].ToString();
                                additionalProfile.State = reader["State"].ToString();
                                additionalProfile.PostalCode = reader["PostalCode"].ToString();
                                if (DBNull.Value != reader["Terms"])
                                    additionalProfile.Terms = (int)reader["Terms"];
                                else
                                    additionalProfile.Terms = -1;
                            }

                            var tempInvoice = new InvoiceDetailObj();
                            tempInvoice.BillToName = reader["BillToName"].ToString();
                            tempInvoice.ServiceItemType = reader["ServiceItemType"].ToString();
                            tempInvoice.Quantity = 1;
                            tempInvoice.ListPrice = Convert.ToDecimal(reader["Amount"]);
                            tempInvoice.InvoiceBatchID = batchID.InvoiceBatchID;

                            toReturn.Add(tempInvoice);
                        }
                    }
                }
            }

            decimal grandtotal = 0;

            var TempHeader = InvoiceHeader.Replace("@today", batchID.BatchToDate.ToString("MM/dd/yyyy"));
            TempHeader = TempHeader.Replace("@email", EMAIL).Replace("@website", WEBSITE);
            TempHeader = TempHeader.Replace("@clientname", additionalProfile.ClientName).Replace("@attention", additionalProfile.Attention);

            var address = additionalProfile.AddressLine1;
            if (additionalProfile.AddressLine2 != "")
                address += "<br>" + additionalProfile.AddressLine2;
            TempHeader = TempHeader.Replace("@address", address).Replace("@city", additionalProfile.City).Replace("@state", additionalProfile.State).Replace("@zip", additionalProfile.PostalCode);
            /*DateTime createDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month,
                                    DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));*/
            var termInt = additionalProfile.Terms;
            var term = "";
            if (termInt > 0)
                term = "Net " + termInt;
            else term = "Due on Receipt";
            TempHeader = TempHeader.Replace("@accountno", "").Replace("@pono", "").Replace("@term", term);

            //var today = DateTime.Now;
            var today = batchID.BatchToDate.AddDays(termInt);
            if (term != "Due on Receipt")
                TempHeader = TempHeader.Replace("@duedate", today.ToString("MM/dd/yyyy"));

            var additionalAppend = "";

            foreach (var line in toReturn)
            {
                additionalAppend += INVOICE_ROW.Replace("@servicetype", line.ServiceItemType).Replace("@rate", FormatNumber(line.ListPrice)).Replace("@total", FormatNumber(line.ListPrice));
                grandtotal += line.ListPrice;
            }

            var TempFooter = InvoiceFooter.Replace("@grandtotal", FormatNumber(grandtotal));
            var ADD_PDF = TempHeader + additionalAppend + TempFooter;

            return ADD_PDF;
        }


        /// <summary>
        /// Generate all invoice header associate with batch
        /// </summary>
        /// <param name="batchID"></param>
        /// <returns></returns>
        public string GenerateInvoiceHeader(int batchID)
        {
            var db = new DBConnection(Username);
            db.ConnectionCheck();
            using (db.connection)
            {
                string query = string.Format("INSERT INTO InvoiceHeader (InvoiceBatchID, BillToID, BillToName, BillToIDType, InvoiceDate, InvoiceTotalAmount, InvoiceStatus) " +
                "SELECT {0} as InvoiceBatchID, BillToID, BillToName, BillToIDType, GETDATE(), SUM(COALESCE(ChargedPrice, ListPrice) * Quantity), 'Reviewing' " +
                "FROM TempInvoiceLineDetail " +
                "WHERE InvoiceBatchID = {0} AND BillToID IS NOT NULL " +
                "GROUP BY BillToID, BillToName, BillToIDType " +
                "ORDER BY BillToIDType", batchID);
                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    com.ExecuteNonQuery();
                }
                return "Success";
            }
        }
        /// <summary>
        /// Get all invoice header associate with batch
        /// </summary>
        /// <param name="batchID"></param>
        /// <returns></returns>
        public List<InvoiceHeaderModel> GetInvoiceHeader(int batchID)
        {
            var db = new DBConnection(Username);
            var toReturn = new List<InvoiceHeaderModel>();
            db.ConnectionCheck();
            using (db.connection)
            {

                var query = string.Format("SELECT * from InvoiceHeader WHERE InvoiceBatchID = {0} order by BillToName", batchID);

                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            var tempInvoice = new InvoiceHeaderModel();
                            tempInvoice.BillToName = reader["BillToName"].ToString();
                            tempInvoice.BillToID = (int)reader["BillToID"]; ;
                            tempInvoice.BillToIDType = reader["BillToIDType"].ToString();
                            tempInvoice.InvoiceNumber = (int)reader["InvoiceNumber"];
                            tempInvoice.InvoiceBatchID = batchID;
                            toReturn.Add(tempInvoice);
                        }
                    }
                }
            }

            return toReturn;
        }
        /// <summary>
        /// Generate all invoice detail line associate with batch
        /// </summary>
        /// <param name="batchID"></param>
        /// <returns></returns>
        public string GenerateInvoiceLine(int batchID)
        {
            var headerList = GetInvoiceHeader(batchID);
            var invoiceTempList = new List<InvoiceDetailModel>();
            Dictionary<string, int> indexList = new Dictionary<string, int>();
            var query = "";
            var queryList = "";
            var db = new DBConnection(Username);
            db.ConnectionCheck();
            using (db.connection)
            {

                foreach (var header in headerList)
                {
                    query = string.Format("SELECT ServiceItemType, SUM(COALESCE(ChargedPrice, ListPrice) * Quantity) as Amount " +
                    "FROM TempInvoiceLineDetail WHERE InvoiceBatchID = {0} AND BillToIDType = '{1}' AND BillToID = {2} AND BillToName = '{3}' " +
                    "GROUP BY BillToIDType, BillToID, BillToName, ServiceItemType", batchID, header.BillToIDType, header.BillToID, header.BillToName);

                    using (SqlCommand com = new SqlCommand(query, db.connection))
                    {
                        db.ConnectionCheck();
                        using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            while (reader.Read())
                            {
                                if (indexList.ContainsKey(header.BillToName))
                                {
                                    indexList[header.BillToName]++;
                                }
                                else
                                {
                                    indexList.Add(header.BillToName, 1);
                                }

                                var tempQuery = string.Format("INSERT INTO InvoiceLine (InvoiceNumber, InvoiceLineNumber, InvoiceLineDescription, InvoiceLineAmount) " +
                                "VALUES ({0}, {1}, '{2}', {3});", header.InvoiceNumber, indexList[header.BillToName], reader["ServiceItemType"].ToString(), Convert.ToDecimal(reader["Amount"]));

                                queryList += tempQuery;
                            }
                        }
                    }
                }

                db.ConnectionCheck();
                using (SqlCommand com = new SqlCommand())
                {
                    com.CommandText = queryList;
                    com.CommandType = CommandType.Text;
                    com.Connection = db.connection;
                    com.ExecuteNonQuery();
                }

                return "Success";
            }
        }
        /// <summary>
        /// Get all invoice detail line associate with batch
        /// </summary>
        /// <param name="batchID"></param>
        /// <returns></returns>
        public List<InvoiceLineModel> GetInvoiceLine(int batchID)
        {
            var toReturn = new List<InvoiceLineModel>();
            //var query = "select * from InvoiceLine where InvoiceNumber IN (SELECT InvoiceNumber FROM InvoiceHeader WHERE InvoiceBatchID = " + batchID + ")";
            var query = string.Format("SELECT  " +
            "IL.InvoiceNumber, IL.InvoiceLineNumber, IL.InvoiceLineDescription,  " +
            "IL.InvoiceLineComments, IL.InvoiceLineAmount,  " +
            "IH.BillToID, IH.BillToIDType, IH.BillToName " +
            "FROM  " +
            "InvoiceLine IL, InvoiceHeader IH " +
            "WHERE  " +
            "IL.InvoiceNumber = IH.InvoiceNumber " +
            "AND " +
            "IH.InvoiceBatchID = {0} " +
            "ORDER BY BillToName, IL.InvoiceLineNumber", batchID);
            var db = new DBConnection(Username);
            db.ConnectionCheck();
            using (db.connection)
            {
                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            var temp = new InvoiceLineModel();
                            temp.InvoiceNumber = (int)reader["InvoiceNumber"];
                            temp.InvoiceLineNumber = (int)reader["InvoiceLineNumber"];
                            temp.InvoiceLineDescription = reader["InvoiceLineDescription"].ToString();
                            temp.BillToID = (int)reader["BillToID"];
                            temp.BillToName = reader["BillToName"].ToString();
                            temp.BillToIDType = reader["BillToIDType"].ToString();
                            toReturn.Add(temp);
                        }
                    }
                }
            }

            return toReturn;
        }
        /// <summary>
        /// Generate invoice detail for archive purpose
        /// </summary>
        /// <param name="batchID"></param>
        /// <returns></returns>
        public string GenerateInvoiceDetail(int batchID)
        {
            var lineList = GetInvoiceLine(batchID);
            var queryText = "";

            foreach (var line in lineList)
            {
                var query = string.Format("INSERT INTO InvoiceDetail (InvoiceNumber, InvoiceLineNumber, AllianceID, AllianceName, " +
                        "BillingClientID, BillingClientName, MarketClientID, MarketClientName, ServiceItemTypeID, ServiceItemType, " +
                        "ServiceItemID, ServiceItemName, OfferingID, OfferingDescription, " +
                        "ProductTypeID, ProductType, TransactionSource, TransactionType, " +
                        "RequestID, RequestDescription, ResolutionStatus, ProviderID, ProviderName, " +
                        "SSN, FacilityName, ExternalID, Comment, " +
                        "StartDate, LastActionDate, AppReceivedDate, AppCompletedDate, BillDate, " +
                        "Quantity, Amount) " +
                        "SELECT {0} as InvoiceNumber, {1} as InvoiceLineNumber, AllianceID, AllianceName, " +
                        "BillingClientID, BillingClientName, MarketClientID, MarketClientName, ServiceItemTypeID, ServiceItemType, " +
                        "ServiceItemID, ServiceItemName, OfferingID, OfferingDescription,  " +
                        "ProductTypeID, ProductType, TransactionSource, TransactionType, " +
                        "RequestID, RequestDescription, ResolutionStatus, ProviderID, ProviderName,  " +
                        "SSN, FacilityName, ExternalID, Comment, " +
                        "StartDate, LastActionDate, AppReceivedDate, AppCompletedDate, BillDate,  " +
                        "Quantity, COALESCE(ChargedPrice, ListPrice) as Amount " +
                        "FROM TempInvoiceLineDetail WHERE InvoiceBatchID = {2} " +
                        "AND BillToID = {3} AND BillToIDType = '{4}' AND ServiceItemType='{5}';", line.InvoiceNumber, line.InvoiceLineNumber, batchID, line.BillToID, line.BillToIDType, line.InvoiceLineDescription);
                queryText += query;
            }
            var db = new DBConnection(Username);
            db.ConnectionCheck();
            using (db.connection)
            {
                using (SqlCommand com = new SqlCommand())
                {
                    com.CommandText = queryText;
                    com.CommandType = CommandType.Text;
                    com.Connection = db.connection;
                    com.ExecuteNonQuery();
                }

                return "Success";
            }
        }
        /// <summary>
        /// Change batch status
        /// </summary>
        /// <param name="batchID">batch to change</param>
        /// <param name="status">status to change</param>
        /// <returns></returns>
        public string ChangeBatchStatus(int batchID, string status)
        {
            var db = new DBConnection(Username);
            db.ConnectionCheck();
            using (db.connection)
            {

                string query = "UPDATE InvoiceBatch SET BatchStatus='" + status + "' WHERE InvoiceBatchID = " + batchID;
                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    com.ExecuteNonQuery();
                    return "Success";
                }
            }
        }
        /// <summary>
        /// Get list of market for Display By Market option
        /// </summary>
        /// <param name="batchID"></param>
        /// <param name="clientID"></param>
        /// <param name="type"></param>
        /// <param name="isEmptymarket"></param>
        /// <returns></returns>
        public List<InvoiceDetailObj> GetShowByMarket(int batchID, int clientID, string type, bool isEmptymarket)
        {
            var db = new DBConnection(Username);
            db.ConnectionCheck();
            using (db.connection)
            {
                var toReturn = new List<InvoiceDetailObj>();

                var idtypestring = "BillToID";
                /*switch (type)
                {
                    case "A":
                        idtypestring = "AllianceID";
                        break;
                    case "B":
                        idtypestring = "BillingClientID";
                        break;
                    case "M":
                        idtypestring = "MarketClientID";
                        break;
                }*/
                idtypestring += "=" + clientID;
                string query = "";
                if (!isEmptymarket)
                    query = string.Format("select MarketClientID, MarketClientName" +
                    ", SUM(COALESCE(ChargedPrice, ListPrice)*Quantity) as Amount " +
                    "from TempInvoiceLineDetail " +
                    "where InvoiceBatchID={0} AND MarketClientID IS NOT NULL AND BillToIDType = '{1}' AND {2}  " +
                    "group by MarketClientID, MarketClientName order by MarketClientName", batchID, type, idtypestring, clientID);
                else
                {
                    query = string.Format("select ServiceItemType" +
                    ", SUM(COALESCE(ChargedPrice, ListPrice)*Quantity) as Amount " +
                    "from TempInvoiceLineDetail " +
                    "where InvoiceBatchID={0} AND MarketClientID IS NULL AND BillToIDType = '{1}' AND {2}  " +
                    "group by ServiceItemType  order by ServiceItemType", batchID, type, idtypestring, clientID);
                }

                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            var tempInvoice = new InvoiceDetailObj();
                            tempInvoice.InvoiceBatchID = batchID;
                            /*tempInvoice.AllianceID = reader["AllianceID"] != DBNull.Value ? (int)reader["AllianceID"] : 0;
                            tempInvoice.AllianceName = reader["AllianceName"] != DBNull.Value ? reader["AllianceName"].ToString() : "";
                            tempInvoice.BillingClientID = reader["BillingClientID"] != DBNull.Value ? (int)reader["BillingClientID"] : 0;
                            tempInvoice.BillingClientName = reader["BillingClientName"] != DBNull.Value ? reader["BillingClientName"].ToString() : "";*/
                            if (!isEmptymarket)
                            {
                                tempInvoice.MarketClientID = (int)reader["MarketClientID"];
                                tempInvoice.MarketClientName = reader["MarketClientName"].ToString();
                            } else
                            {
                                tempInvoice.MarketClientName = reader["ServiceItemType"].ToString();
                            }
                            /*tempInvoice.BillToIDType = reader["BillToIDType"].ToString(); ;
                            tempInvoice.BillToID = (int)reader["BillToID"];
                            tempInvoice.BillToName = reader["BillToName"].ToString();
                            tempInvoice.ServiceItemType = reader["ServiceItemType"].ToString();
                            if (reader["ServiceItemTypeID"] != DBNull.Value)
                                tempInvoice.ServiceItemTypeID = (int)reader["ServiceItemTypeID"];
                            tempInvoice.TransactionType = reader["TransactionType"].ToString();
                            tempInvoice.TransactionSource = reader["TransactionSource"].ToString();*/
                            tempInvoice.Quantity = 1;
                            tempInvoice.ListPrice = Convert.ToDecimal(reader["Amount"]);
                            //tempInvoice.ChargedPrice = reader["ChargedPrice"] != DBNull.Value ? Convert.ToDecimal(reader["ChargedPrice"]) : 0;
                            toReturn.Add(tempInvoice);
                        }
                    }
                }
                return toReturn;
            }
        }
        #endregion

        #region DataManagement
        /// <summary>
        /// Get all client profiles
        /// </summary>
        /// <returns></returns>
        public List<ClientProfileModel> GetAllClientInfo()
        {
            var toReturn = new List<ClientProfileModel>();
            var db = new DBConnection(Username);
            db.ConnectionCheck();

            using (db.connection)
            {                
                var query = @"SELECT " +
                "ba.BillToIDType, " +
                "ba.BillToID, " +
                "CASE ba.BillToIDType WHEN 'A' THEN a.AllianceName WHEN 'B' THEN bc.BillingClientName WHEN 'M' THEN mc.MarketClientName END AS BillToName, " +
                "ba.Attention, " +
                "ba.AddressLine1, " +
                "ba.AddressLine2, " +
                "ba.City, " +
                "ba.State, " +
                "ba.PostalCode, " +
                "ba.Email, " +
                "ba.Phone, " +
                "ba.Terms, " +
                "ba.SecuredEmailFlag, " +
                "ba.ActiveFlag, " +
                "do.DisplayOption " +
                "FROM  " +
                "BillingAddress ba " +
                "LEFT JOIN Alliance a ON ba.BillToID = a.AllianceID AND ba.BillToIDType='A' " +
                "LEFT JOIN BillingClient bc ON ba.BillToID = bc.BillingClientID AND ba.BillToIDType='B' " +
                "LEFT JOIN MarketClient mc ON ba.BillToID = mc.MarketClientID AND ba.BillToIDType='M' " +
                "LEFT JOIN DisplayOption do ON ba.BillToID = do.BillToID AND ba.BillToIDType=do.BillToIDType";

                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            var tempProfile = new ClientProfileModel();
                            tempProfile.ClientID = (int)reader["BillToID"];
                            tempProfile.ClientName = reader["BillToName"].ToString();
                            tempProfile.ClientType = reader["BillToIDType"].ToString();
                            tempProfile.Attention = reader["Attention"].ToString();
                            tempProfile.AddressLine1 = reader["AddressLine1"].ToString();
                            tempProfile.AddressLine2 = reader["AddressLine2"].ToString();
                            tempProfile.City = reader["City"].ToString();
                            tempProfile.State = reader["State"].ToString();
                            tempProfile.PostalCode = reader["PostalCode"].ToString();
                            tempProfile.Email = reader["Email"].ToString();
                            tempProfile.SecuredEmailFlag = (int)reader["SecuredEmailFlag"];
                            tempProfile.Phone = reader["Phone"].ToString();
                            if (DBNull.Value != reader["Terms"])
                                tempProfile.Terms = (int)reader["Terms"];
                            else
                                tempProfile.Terms = -1;
                            tempProfile.ActiveFlag = (int)reader["ActiveFlag"];
                            tempProfile.DisplayOption = reader["DisplayOption"].ToString();

                            toReturn.Add(tempProfile);
                        }
                    }
                }
            }

            return toReturn;
        }
        /// <summary>
        /// Update client profile
        /// </summary>
        /// <param name="profile"></param>
        /// <returns></returns>
        public string UpdateClientInfo(ClientProfileModel profile)
        {
            var query = string.Format("UPDATE BillingAddress " +
                        "SET  " +
                        "Attention = '{2}', " +
                        "AddressLine1 = '{3}', " +
                        "AddressLine2 = '{4}', " +
                        "City = '{5}', " +
                        "\"State\" = '{6}', " +
                        "PostalCode = '{7}', " +
                        "Email = '{8}', " +
                        "SecuredEmailFlag = {9}, " +
                        "Phone = '{10}', " +
                        "Terms = {11}, " +
                        "ActiveFlag = {12}, " +
                        "UpdatedDate = GETDATE(), " +
                        "UpdatedBy = 'BillingApp' " +
                        "WHERE " +
                        "BillToID = {0} AND BillToIDType = '{1}'", profile.ClientID, profile.ClientType, profile.Attention, profile.AddressLine1, profile.AddressLine2,
                        profile.City, profile.State, profile.PostalCode, profile.Email, profile.SecuredEmailFlag, profile.Phone, profile.Terms, profile.ActiveFlag);

            try
            {
                var db = new DBConnection(Username);
                db.ConnectionCheck();
                using (db.connection)
                {
                    var trimquery = query.Replace("''", "NULL");
                    using (SqlCommand com = new SqlCommand(trimquery, db.connection))
                    {
                        com.ExecuteNonQuery();
                        Logging("BillingAddress", trimquery);                        
                        return "Success";
                    }
                }
            }
            catch (Exception err)
            {
                return "Error - " + err.Message;
            }
        }
        /// <summary>
        /// Get Search Criteria for Billing Relationship Management Page 
        /// </summary>
        /// <returns></returns>
        public RelationshipSearchViewModel GetRelationshipSearchViewModel()
        {
            var toReturn = new RelationshipSearchViewModel();

            var db = new DBConnection(Username);
            db.ConnectionCheck();

            using (db.connection)
            {
                // Get Alliance
                var query = @"select distinct AllianceID, AllianceName from Alliance where AllianceID != 0";

                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        var tempList = new List<SearchObj>();
                        while (reader.Read())
                        {
                            var tempObj = new SearchObj();
                            tempObj.objID = (int)reader["AllianceID"];
                            tempObj.objName = reader["AllianceName"].ToString();
                            tempList.Add(tempObj);
                        }
                        toReturn.Alliance = tempList;
                    }
                }

                // Get BillingClient
                query = @"select distinct BillingClientID, BillingClientName from BillingClient where BillingClientID != 0";
                db.ConnectionCheck();
                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        var tempList = new List<SearchObj>();
                        while (reader.Read())
                        {
                            var tempObj = new SearchObj();
                            tempObj.objID = (int)reader["BillingClientID"];
                            tempObj.objName = reader["BillingClientName"].ToString();
                            tempList.Add(tempObj);
                        }
                        toReturn.BillingClient = tempList;
                    }
                }

                // Get MarketClient
                query = @"select distinct MarketClientID, MarketClientName from MarketClient where MarketClientID != 0 AND MarketClientName NOT LIKE '%DO NOT USE%'";
                db.ConnectionCheck();
                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        var tempList = new List<SearchObj>();
                        while (reader.Read())
                        {
                            var tempObj = new SearchObj();
                            tempObj.objID = (int)reader["MarketClientID"];
                            tempObj.objName = reader["MarketClientName"].ToString();
                            tempList.Add(tempObj);
                        }
                        toReturn.MarketClient = tempList;
                    }
                }

                // Get ServiceItemType
                query = @"select distinct ServiceItemTypeID, ServiceItemType from ServiceItemType";
                db.ConnectionCheck();
                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        var tempList = new List<SearchObj>();
                        while (reader.Read())
                        {
                            var tempObj = new SearchObj();
                            tempObj.objID = (int)reader["ServiceItemTypeID"];
                            tempObj.objName = reader["ServiceItemType"].ToString();
                            tempList.Add(tempObj);
                        }
                        toReturn.ServiceItemType = tempList;
                    }
                }

                // Get ServiceItem
                query = @"select distinct ServiceItemID, ServiceItemName from ServiceItem where ServiceItemID != 0";
                db.ConnectionCheck();
                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        var tempList = new List<SearchObj>();
                        while (reader.Read())
                        {
                            var tempObj = new SearchObj();
                            tempObj.objID = (int)reader["ServiceItemID"];
                            tempObj.objName = reader["ServiceItemName"].ToString();
                            tempList.Add(tempObj);
                        }
                        toReturn.ServiceItem = tempList;
                    }
                }

                // Get Offering
                query = @"select distinct OfferingDescription from Offering where OfferingID!=0 AND OfferingDescription IS NOT NULL";
                db.ConnectionCheck();
                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        var tempList = new List<string>();
                        while (reader.Read())
                        {
                            tempList.Add(reader["OfferingDescription"].ToString());
                        }
                        toReturn.Offering = tempList;
                    }
                }

                // Get AccountingType
                query = @"select distinct AccountingType from BillingRelationship";
                db.ConnectionCheck();
                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        var tempList = new List<string>();
                        while (reader.Read())
                        {
                            tempList.Add(reader["AccountingType"].ToString());
                        }
                        toReturn.AccountingType = tempList;
                    }
                }
            }

            return toReturn;
        }
        /// <summary>
        /// Get Search Result from Billing Relationship Search Criteria
        /// </summary>
        /// <param name="AllianceID"></param>
        /// <param name="BillingClientID"></param>
        /// <param name="MarketClientID"></param>
        /// <param name="ServiceItemTypeID"></param>
        /// <param name="ServiceItemID"></param>
        /// <param name="Offering"></param>
        /// <param name="AccountingType"></param>
        /// <returns></returns>
        public List<BillingRelationshipModel> GetFilteredResult(string AllianceID, string BillingClientID, string MarketClientID, string ServiceItemTypeID, string ServiceItemID, string Offering, string AccountingType)
        {
            var db = new DBConnection(Username);
            var conditionArray = new List<string>();
            if (AllianceID != null && AllianceID != "")
            {
                conditionArray.Add("BR.AllianceID = " + AllianceID);
            }
            if (BillingClientID != null && BillingClientID != "")
            {
                conditionArray.Add("BR.BillingClientID = " + BillingClientID);
            }
            if (MarketClientID != null && MarketClientID != "")
            {
                conditionArray.Add("BR.MarketClientID = " + MarketClientID);
            }
            if (ServiceItemTypeID != null && ServiceItemTypeID != "")
            {
                conditionArray.Add("ServiceItemTypeID = " + ServiceItemTypeID);
            }
            if (ServiceItemID != null && ServiceItemID != "")
            {
                conditionArray.Add("ServiceItemID = " + ServiceItemID);
            }
            if (Offering != null && Offering != "")
            {
                conditionArray.Add("OfferingID IN (SELECT OfferingID FROM Offering WHERE OfferingDescription = '" + Offering + "')");
            }
            if (AccountingType != null && AccountingType != "")
            {
                conditionArray.Add("AccountingType = '" + AccountingType + "'");
            }

            var count = 0;
            var formCondition = "";
            foreach (var condition in conditionArray)
            {
                if (count == 0)
                    formCondition += "WHERE " + condition + " ";
                else
                    formCondition += "AND " + condition + " ";
                count++;
            }

            var query = "SELECT BillingRelationshipID, " +
                        "BR.AllianceID, " +
                        "CASE WHEN BR.AllianceID IS NOT NULL THEN (SELECT AllianceName FROM Alliance A WHERE A.AllianceID = BR.AllianceID) ELSE '' END AS AllianceName, " +
                        "BR.BillingClientID, " +
                        "CASE WHEN BR.BillingClientID IS NOT NULL THEN (SELECT BillingClientName FROM BillingClient B WHERE B.BillingClientID = BR.BillingClientID) ELSE '' END AS BillingClientName, " +
                        "BR.MarketClientID, " +
                        "CASE WHEN BR.MarketClientID IS NOT NULL THEN (SELECT MarketClientName FROM MarketClient M WHERE M.MarketClientID = BR.MarketClientID) ELSE '' END AS MarketClientName, " +
                        "ServiceItemTypeID, " +
                        "(SELECT ServiceItemType FROM ServiceItemType WHERE ServiceItemTypeID = BR.ServiceItemTypeID) as ServiceItemType, " +
                        "OfferingID, " +
                        "(SELECT OfferingDescription FROM Offering WHERE OfferingID = BR.OfferingID) as OfferingDescription, " +
                        "ServiceItemID, " +
                        "(SELECT ServiceItemName FROM ServiceItem WHERE ServiceItemID = BR.ServiceItemID) as ServiceItemName, " +
                        "AccountingType, " +
                        "BillToIDType " +
                        "FROM BillingRelationship BR " + formCondition +
                        "ORDER BY BillToIDType";
            var toReturn = new List<BillingRelationshipModel>();

            db.ConnectionCheck();
            using (db.connection)
            {
                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            var tempProfile = new BillingRelationshipModel();
                            tempProfile.BillingRelationshipID = (int)reader["BillingRelationshipID"];
                            tempProfile.AllianceID = reader["AllianceID"] != DBNull.Value ? (int)reader["AllianceID"] : 0;
                            tempProfile.AllianceName = reader["AllianceName"] != DBNull.Value ? reader["AllianceName"].ToString() : "";
                            tempProfile.BillingClientID = (int)reader["BillingClientID"];
                            tempProfile.BillingClientName = reader["BillingClientName"].ToString();
                            tempProfile.MarketClientID = (int)reader["MarketClientID"];
                            tempProfile.MarketClientName = reader["MarketClientName"].ToString();
                            tempProfile.BillToIDType = reader["BillToIDType"].ToString();
                            var billType = tempProfile.BillToIDType == "A" ? "Alliance" : tempProfile.BillToIDType == "B" ? "BillingClient" : "MarketClient";
                            tempProfile.BillToName = reader[billType + "Name"].ToString();
                            tempProfile.BillToID = (int)reader[billType + "ID"];
                            tempProfile.ServiceItemTypeID = reader["ServiceItemTypeID"] != DBNull.Value ? Convert.ToInt32(reader["ServiceItemTypeID"]) : 0;
                            tempProfile.ServiceItemType = reader["ServiceItemType"].ToString();
                            tempProfile.ServiceItemID = reader["ServiceItemID"] != DBNull.Value ? Convert.ToInt32(reader["ServiceItemID"]) : 0;
                            tempProfile.ServiceItemName = reader["ServiceItemName"].ToString();
                            tempProfile.OfferingID = (int)reader["OfferingID"];
                            tempProfile.OfferingDescription = reader["OfferingDescription"].ToString();
                            tempProfile.AccountingType = reader["AccountingType"].ToString();
                            toReturn.Add(tempProfile);
                        }
                    }
                }
            }

            return toReturn;
        }
        /// <summary>
        /// Get Charged Price List 
        /// </summary>
        /// <param name="BillingRelationshipID"></param>
        /// <returns></returns>
        public List<BillingPriceModel> GetBillingPrice(int BillingRelationshipID)
        {
            var toReturn = new List<BillingPriceModel>();
            var db = new DBConnection(Username);

            var query = string.Format("select * from BillingPrice where BillingRelationshipID = {0}", BillingRelationshipID);
            db.ConnectionCheck();
            using (db.connection)
            {
                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            var temp = new BillingPriceModel();
                            temp.BillingRelationshipID = BillingRelationshipID;
                            temp.ListPrice = Convert.ToDecimal(reader["ListPrice"]);
                            if (DBNull.Value != reader["ChargedPrice"])
                                temp.ChargedPrice = Convert.ToDecimal(reader["ChargedPrice"]);
                            else
                                temp.ChargedPrice = -9999;
                            toReturn.Add(temp);
                        }
                    }
                }
            }


            return toReturn;
        }
        /// <summary>
        /// Update Billing Relationship Profile
        /// </summary>
        /// <param name="relation"></param>
        /// <returns></returns>
        public string UpdateBillingRelationship(BillingRelationshipModel relation)
        {
            var alliance = relation.AllianceName != null ? relation.AllianceID + "" : "NULL";            
            
            var billingtype = relation.AllianceName == null && relation.BillToIDType == "A" ? "B": relation.BillToIDType + "";
            var query = string.Format("UPDATE BillingRelationship " +
                        "SET AllianceID = {1}, " +
                        "BillToIDType = '{2}' " +
                        "WHERE " +
                        "BillingRelationshipID = '{0}'", relation.BillingRelationshipID, alliance, billingtype);
            try
            {
                var db = new DBConnection(Username);
                db.ConnectionCheck();
                using (db.connection)
                {
                    using (SqlCommand com = new SqlCommand(query, db.connection))
                    {
                        com.ExecuteNonQuery();
                        Logging("BillingRelationship", query);
                        return "Success";
                    }
                }
            }
            catch (Exception err)
            {
                return "Error - " + err.Message;
            }
        }
        /// <summary>
        /// Update Charged Price
        /// </summary>
        /// <param name="BillingRelationshipID"></param>
        /// <param name="ListPrice"></param>
        /// <param name="ChargedPrice"></param>
        /// <returns></returns>
        public string UpdateBillingPrice(int BillingRelationshipID, decimal ListPrice, decimal ChargedPrice)
        {
            var db = new DBConnection(Username);
            db.ConnectionCheck();

            var chargedprice = ChargedPrice + "";
            if (chargedprice == "-9999")
                chargedprice = "NULL";

            var query = string.Format("UPDATE BillingPrice SET ChargedPrice = {2}, UpdatedDate = GETDATE() WHERE BillingRelationshipID = {0} AND ListPrice = {1}", BillingRelationshipID, ListPrice, chargedprice);

            using (db.connection)
            {
                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    com.ExecuteNonQuery();
                    Logging("BillingPrice", query);
                    return "Success";
                }
            }
        }
        #endregion

        #region HelperMethods
        /// <summary>
        /// Handle Logging for Billing App
        /// </summary>
        /// <param name="table"></param>
        /// <param name="description"></param>
        public void Logging(string table, string description)
        {
            var db = new DBConnection(Username);
            db.ConnectionCheck();

            var query = string.Format("INSERT INTO ChangeLog VALUES ('{0}', @query, GETDATE(), '{1}')",table, Username);

            using (db.connection)
            {
                using (SqlCommand com = new SqlCommand())
                {
                    com.Connection = db.connection;
                    com.CommandText = query;
                    com.Parameters.AddWithValue("@query", description);
                    com.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// Call this to Clean Up any Logging that is more than 1 year old
        /// </summary>
        public void CleanUpLogging()
        {
            var db = new DBConnection(Username);
            db.ConnectionCheck();

            var query = "DELETE FROM ChangeLog WHERE CreatedDate < DATEADD(year, -1, GETDATE())";

            using (db.connection)
            {
                using (SqlCommand com = new SqlCommand(query,db.connection))
                {
                    com.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// Format number into $ amount
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public string FormatNumber(decimal number)
        {
            var toFormat = string.Format("{0:C}", Convert.ToDecimal(number.ToString("0.00")));
            return toFormat;
        }
        /// <summary>
        /// Generate client structure folder
        /// </summary>
        /// <param name="batchID"></param>
        /// <returns></returns>
        public string GenerateClientFolder(int batchID)
        {
            try
            {
                var path = "~/Serve/";
                var ClientFolder = "Client_Files";
                var BackupFolder = "Backup";
                string serverClientFolder = Path.Combine(HttpContext.Current.Server.MapPath(path), ClientFolder);
                if (!Directory.Exists(serverClientFolder))
                {
                    Directory.CreateDirectory(serverClientFolder);
                }

                //Generate Batch Specific Folder
                serverClientFolder = Path.Combine(serverClientFolder, "Batch_" + batchID);
                if (!Directory.Exists(serverClientFolder))
                {
                    Directory.CreateDirectory(serverClientFolder);
                }

                var statusFile = "Status.txt";
                var statusFilePath = Path.Combine(serverClientFolder, statusFile);
                using (FileStream stream = new FileStream(statusFilePath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
                {
                    using (TextWriter writer = new StreamWriter(stream))
                    {
                        writer.Write("Archiving the data ...");
                        writer.Flush();
                    }
                }

                var ClientList = GetAllParentRow(batchID);
                foreach (var item in ClientList)
                {
                    string folder = Path.Combine(serverClientFolder, FixInvalidPath(item.BillToName));
                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                        var backupPath = Path.Combine(folder, BackupFolder);
                        Directory.CreateDirectory(backupPath); //Create backup folder
                    }
                }

                return "Success";
            }
            catch (Exception err)
            {
                Debug.WriteLine("Error - " + err.Message);
                return "Error - " + err.Message;
            }
        }
        public List<TempInvoiceDetailModel> GetServiceItemTypes(int batchID, int BillToID, string BillToIDType)
        {
            var toReturn = new List<TempInvoiceDetailModel>();

            var db = new DBConnection(Username);

            db.ConnectionCheck();
            using (db.connection)
            {
                var query = string.Format("SELECT BillToName, ServiceItemType, SUM(COALESCE(ChargedPrice, ListPrice)*Quantity) Amount " +
                "FROM TempInvoiceLineDetail " +
                "WHERE InvoiceBatchID = {0} " +
                "AND BillToIDType = '{1}' AND BillToID = {2} " +
                "GROUP BY BillToName, ServiceItemType order by BillToName, ServiceItemType", batchID, BillToIDType, BillToID);

                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            var tempInvoice = new TempInvoiceDetailModel();
                            tempInvoice.BillToName = reader["BillToName"].ToString();
                            tempInvoice.BillToID = BillToID;
                            tempInvoice.BillToIDType = BillToIDType;
                            tempInvoice.ServiceItemType = reader["ServiceItemType"].ToString();
                            tempInvoice.ListPrice = Convert.ToDecimal(reader["Amount"]);

                            toReturn.Add(tempInvoice);
                        }
                    }
                }
            }

            return toReturn;
        }
        public List<TempInvoiceDetailModel> GetAllParentRow(int batchID)
        {
            var toReturn = new List<TempInvoiceDetailModel>();
            var db = new DBConnection(Username);
            db.ConnectionCheck();
            using (db.connection)
            {

                var query = string.Format("SELECT BillToID, BillToName, BillToIDType " +
                "FROM TempInvoiceLineDetail " +
                "WHERE InvoiceBatchID = {0} AND BillToID IS NOT NULL " +
                "GROUP BY BillToID, BillToName, BillToIDType " +
                "ORDER BY BillToIDType", batchID);
                using (SqlCommand com = new SqlCommand(query, db.connection))
                {
                    using (SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            var tempInvoice = new TempInvoiceDetailModel();
                            tempInvoice.BillToName = reader["BillToName"].ToString();
                            tempInvoice.BillToID = (int)reader["BillToID"]; ;
                            tempInvoice.BillToIDType = reader["BillToIDType"].ToString();

                            toReturn.Add(tempInvoice);
                        }
                    }
                }
            }
            return toReturn;
        }
        public List<UploadObj> GetAllFile(int batchID)
        {
            var toReturn = new List<UploadObj>();
            var ServerPhysicalPath = HttpContext.Current.Server.MapPath("~/Serve/");
            var ServerPath = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath + "Serve";
            var ClientFolder = "Client_Files";
            var BatchFolder = "Batch_" + batchID;
            var ClientList = GetAllParentRow(batchID);
            string serverClientFolder = ServerPath + "/" + ClientFolder + "/" + BatchFolder;
            string serverPhysicalClientFolder = Path.Combine(ServerPhysicalPath, ClientFolder, BatchFolder);

            foreach (var client in ClientList)
            {
                var pathPhysical = Path.Combine(serverPhysicalClientFolder, FixInvalidPath(client.BillToName));
                var pathCheck = serverClientFolder + "/" + FixInvalidPath(client.BillToName);
                DirectoryInfo d = new DirectoryInfo(pathPhysical);
                var tempUploadObj = new UploadObj();
                tempUploadObj.batchID = batchID;
                tempUploadObj.clientID = client.BillToID;
                tempUploadObj.clientName = client.BillToName;
                tempUploadObj.clientType = client.BillToIDType;
                tempUploadObj.filePath = new List<string>();
                if (d.Exists)
                {
                    var files = Directory.EnumerateFiles(pathPhysical, "*.*", SearchOption.AllDirectories)
                    .Where(s => s.ToLower().EndsWith(".pdf") || s.ToLower().EndsWith(".xlsx"));
                    foreach (var file in files)
                    {
                        tempUploadObj.filePath.Add(pathCheck + "/" + Path.GetFileName(file));
                        Debug.WriteLine(pathCheck + "/" + Path.GetFileName(file));
                        Debug.WriteLine(" -- "+file);
                    }
                }
                toReturn.Add(tempUploadObj);
            }

            return toReturn;
        }
        /// <summary>
        /// Fix invalid synbol in string to make it valid for folder structure
        /// </summary>
        /// <param name="inputPath"></param>
        /// <returns></returns>
        public static string FixInvalidPath(string inputPath)
        {
            string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
            Regex r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
            var tempReturn = r.Replace(inputPath, "");
            var clientName = tempReturn;
            if (clientName.EndsWith("."))
                clientName = clientName.Remove(clientName.Length - 1);
            return clientName;
        }
        public void EmptyFolder(DirectoryInfo directoryInfo)
        {
            if (directoryInfo.Exists)
            {
                foreach (FileInfo file in directoryInfo.GetFiles())
                {
                    file.Delete();
                }

                foreach (DirectoryInfo subfolder in directoryInfo.GetDirectories())
                {
                    EmptyFolder(subfolder);
                }
            }
        }
        /// <summary>
        /// Get status file detail - to update loading screen on billing app
        /// </summary>
        /// <param name="batchID"></param>
        /// <returns></returns>
        public string GetStatus(int batchID)
        {
            var path = "~/Serve/";
            var ClientFolder = "Client_Files";
            string serverClientFolder = Path.Combine(HttpContext.Current.Server.MapPath(path), ClientFolder);
            serverClientFolder = Path.Combine(serverClientFolder, "Batch_" + batchID);

            var statusFile = "Status.txt";
            var statusFilePath = Path.Combine(serverClientFolder, statusFile);
            while (true)
            {
                try
                {
                    using (FileStream stream = new FileStream(statusFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        using (var textReader = new StreamReader(stream))
                        {
                            var content = "";
                            while (!textReader.EndOfStream)
                            {
                                content = textReader.ReadLine();
                            }
                            return content;
                        }
                    }
                }
                catch (Exception err)
                {
                    Debug.WriteLine(err.Message);
                }
            }
            //return "Testing";
        }
        #endregion
    }
}