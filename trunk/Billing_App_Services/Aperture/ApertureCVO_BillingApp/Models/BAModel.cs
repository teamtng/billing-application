﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApertureCVO_BillingApp.Models
{
    
    public class InvoiceLineModel
    {
        public int InvoiceLineNumber { get; set; }
        public int InvoiceNumber { get; set; }
        public string InvoiceLineComment { get; set; }
        public int InvoiceLineQuantity { get; set; }
        public decimal InvoiceLineAmount { get; set; }
        public string InvoiceLineDescription { get; set; }
        public string BillToIDType { get; set; }
        public int BillToID { get; set; }
        public string BillToName { get; set; }

    }
    public class InvoiceHeaderModel
    {
        public int InvoiceNumber { get; set; }
        public int? InvoiceBatchID { get; set; }
        public string BillToIDType { get; set; }
        public int BillToID { get; set; }
        public string BillToName { get; set; }
        public DateTime InvoiceDate { get; set; }
        public double InvoiceTotalAmount { get; set; }
        public double InvoiceAutomatedAmount { get; set; }
        public double InvoiceAdjustmentAmount { get; set; }
        public double InvoiecCreaditAmount { get; set; }
        public string InvoiceStatus { get; set; }
    }
    public class InvoiceDetailModel
    {
        public int InvoiceDetailID { get; set; }
        public int InvoiceNumber { get; set; }
        public int InvoiceLineNumber { get; set; }
        public int AllianceID { get; set; }
        public string AllianceName { get; set; }
        public int BillingClientID { get; set; }
        public string BillingClientName { get; set; }
        public int MarketClientID { get; set; }
        public string MarketClientName { get; set; }
        public int ServiceItemTypeID { get; set; }
        public string ServiceItemType { get; set; }
        public int ServiceItemID { get; set; }
        public string ServiceItemName { get; set; }
        public int OfferingID { get; set; }
        public string OfferingDescription { get; set; }
        public int ProductTypeID { get; set; }
        public string ProductType { get; set; }
        public string TransactionSource { get; set; }
        public string TransactionType { get; set; }
        public int RequestID { get; set; }
        public string RequestDescription { get; set; }
        public string ResolutionStatus { get; set; }
        public int ProviderID { get; set; }
        public string ProviderName { get; set; }
        public string SSN { get; set; }
        public string FacilityName { get; set; }
        public string ExternalID { get; set; }
        public string Comment { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime LastActionDate { get; set; }
        public DateTime? AppReceivedDate { get; set; }
        public DateTime? AppCompletedDate { get; set; }
        public DateTime BillDate { get; set; }
        public decimal Quantity { get; set; }
        public decimal Amount { get; set; }
    }
    public class InvoiceBatchModel
    {
        public int InvoiceBatchID { get; set; }
        public decimal BatchTotalAmount { get { return BatchTotalAdjustmentAmount + BatchTotalAutomatedAmount; } }
        public decimal BatchTotalAutomatedAmount { get; set; }
        public decimal BatchTotalAdjustmentAmount { get; set; }
        public DateTime BatchFromDate { get; set; }
        public DateTime BatchToDate { get; set; }
        public DateTime ApprovedDate { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string BatchStatus { get; set; }
        public List<TempInvoiceDetailModel> TempInvoiceDetailList { get; set; }
    }
    public class TempInvoiceDetailModel
    {
        public int TempInvoiceDetailLineID { get; set; }
        public int EnableGrouping { get; set; }
        public int InvoiceBatchID { get; set; }
        public string TransactionUniqueID { get; set; }
        public int AllianceID { get; set; }
        public string AllianceName { get; set; }
        public int BillingClientID { get; set; }
        public string BillingClientName { get; set; }
        public int MarketClientID { get; set; }
        public string MarketClientName { get; set; }
        public string BillToIDType { get; set; }
        public int BillToID { get; set; }
        public string BillToName { get; set; }
        public int ServiceItemTypeID { get; set; }
        public string ServiceItemType { get; set; }
        public int ServiceItemID { get; set; }
        public string ServiceItemName { get; set; }
        public int OfferingID { get; set; }
        public string OfferingDescription { get; set; }
        public int ProductTypeID { get; set; }
        public string ProductType { get; set; }
        public string TransactionSource { get; set; }
        public string TransactionType { get; set; }        
        public int RequestID { get; set; }
        public string RequestDescription { get; set; }
        public string RequestStatus { get; set; }
        public int ProviderID { get; set; }
        public string ProviderName { get; set; }
        public string SSN { get; set; }
        public string FacilityName { get; set; }
        public string ExternalID { get; set; }
        public string Comment { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime LastActionDate { get; set; }
        public DateTime? AppReceivedDate { get; set; }
        public DateTime? AppCompletedDate { get; set; }
        public DateTime BillDate { get; set; }
        public decimal Quantity { get; set; }
        public decimal ListPrice { get; set; }
        public decimal ChargedPrice { get; set; }
        public int IsMissingData { get; set; }
    }
    public class ExcelInvoiceModel
    {
        public string AllianceName { get; set; }
        public string BillingClientName { get; set; }
        public string MarketClientName { get; set; }       
        public string ServiceItemType { get; set; }
        public string ServiceItemName { get; set; }
        public string OfferingDescription { get; set; }
        public int ProductTypeID { get; set; }
        public string ProductType { get; set; }
        public int RequestID { get; set; }
        public string RequestDescription { get; set; }
        public string ProviderName { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string SSN { get; set; }
        public string FacilityName { get; set; }
        public string ExternalID { get; set; }
        public string Comment { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime LastActionDate { get; set; }
        public DateTime? AppReceivedDate { get; set; }
        public DateTime? AppCompletedDate { get; set; }
        public DateTime BillDate { get; set; }
        public decimal Amount { get; set; }
    }
    public class ServiceItemTypeModel
    {
        public int ServiceItemTypeID { get; set; }
        public string ServiceItemTypeDescription { get; set; }
        public int ActiveFlag { get; set; }
        public string GLAccount { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }
    public class CustomServiceItemModel
    {
        public int ProductID { get; set; }
        public string ProductDescription { get; set; }
        public int? ProductTypeID { get; set; }
        public string BillToType { get; set; }
        public int BillToID { get; set; }
        public int EnableSQL { get; set; }
        public string QuantitySQL { get; set; }
        public double ListPrice { get; set; }
        public int ActiveFlag { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }
    public class ClientProfileModel
    {
        public int ClientID { get; set; }
        public string ClientName { get; set; }
        public string ClientType { get; set; }
        public string Attention { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Email { get; set; }
        public int SecuredEmailFlag { get; set; }
        public string Phone { get; set; }
        public int Terms { get; set; }
        public int ActiveFlag { get; set; }
        public string DisplayOption { get; set; }
    }
    public class BillingRelationshipModel
    {
        public int BillingRelationshipID { get; set; }
        public int AllianceID { get; set; }
        public string AllianceName { get; set; }
        public int BillingClientID { get; set; }
        public string BillingClientName { get; set; }
        public int MarketClientID { get; set; }
        public string MarketClientName { get; set; }
        public string BillToIDType { get; set; }
        public int BillToID { get; set; }
        public string BillToName { get; set; }
        public int ServiceItemTypeID { get; set; }
        public string ServiceItemType { get; set; }
        public int ServiceItemID { get; set; }
        public string ServiceItemName { get; set; }
        public int OfferingID { get; set; }
        public string OfferingDescription { get; set; }
        public string AccountingType { get; set; }
        public decimal ListPrice { get; set; }
    }
    public class BillingPriceModel
    {
        public int BillingRelationshipID { get; set; }
        public decimal ListPrice { get; set; }
        public decimal ChargedPrice { get; set; }
    }

    //Helper Object
    public class DateObj
    {
        public string dateFrom{ get; set;}
        public string dateTo { get; set; }
    }
    public class InvoiceObj
    {
        public int batchID { get; set; }
        public int clientID { get; set; }
        public int AllianceID { get; set; }
        public string type { get; set; }
        public bool isCombine { get; set; }
    }
    public class AdjustmentObj
    {
        public int batchID { get; set; }
        public int clientID { get; set; }
        public string clientType { get; set; }
        public int serviceQuantity { get; set; }
        public string serviceDescription { get; set; }
        public decimal serviceRate { get; set; }
    }
    public class UploadObj
    {
        public int batchID { get; set; }
        public int clientID { get; set; }
        public string clientType { get; set; }
        public string clientName { get; set; }
        public int isMissingData { get; set; }
        public int isSecured { get; set; }
        public List<string> filePath { get; set; }
    }
    public class SearchObj
    {
        public int objID { get; set; }
        public string objName { get; set; }
    }
    public class RelationshipSearchViewModel
    {
        public List<SearchObj> Alliance { get; set; }
        public List<SearchObj> BillingClient { get; set; }
        public List<SearchObj> MarketClient { get; set; }
        public List<SearchObj> ServiceItemType { get; set; }
        public List<SearchObj> ServiceItem { get; set; }
        public List<string> Offering { get; set; }
        public List<string> AccountingType { get; set; }
    }
    public class InvoiceDetailViewModel
    {
        public int EnableGrouping { get; set; }
        public string DisplayOption { get; set; }
        public int BillToID { get; set; }
        public string BillToIDType { get; set; }
        public List<InvoiceDetailObj> DetailList { get; set; }
    }
    public class InvoiceDetailObj
    {        
        public int InvoiceBatchID { get; set; }
        public int MarketClientID { get; set; }
        public string MarketClientName { get; set; }
        public string BillToIDType { get; set; }
        public int BillToID { get; set; }
        public string BillToName { get; set; }
        public int ServiceItemTypeID { get; set; }
        public string ServiceItemType { get; set; }
        public int ServiceItemID { get; set; }
        public string ServiceItemName { get; set; }
        public string TransactionSource { get; set; }
        public string TransactionType { get; set; }
        public decimal Quantity { get; set; }
        public decimal ListPrice { get; set; }
    }
    public class FilteredListModel
    {
        public int ClientID { get; set; }
        public string ClientName { get; set; }
        public decimal InvoiceTotal { get; set; }
    }
}