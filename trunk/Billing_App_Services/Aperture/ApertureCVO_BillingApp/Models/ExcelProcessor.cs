﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.IO;
//using ClosedXML.Excel;
using System.Data;
using OfficeOpenXml;
using OfficeOpenXml.Table.PivotTable;
using DocumentFormat.OpenXml.Spreadsheet;

namespace ApertureCVO_BillingApp.Models
{
    /// <summary>
    /// Handle Excel File
    /// </summary>
    public class ExcelProcessor
    {
        public DataTable table;
        public ExcelPackage ExPackage;
        public ExcelWorksheet ServiceSheet;
        public ExcelWorksheet PivotSheet;
        public ExcelRangeBase ServiceRange;
        public ExcelProcessor(string Name, List<ExcelInvoiceModel> RowList)
        {
            IEnumerable<ExcelInvoiceModel> data = RowList;
            table = CreateDataTable(data);
            ExPackage = new ExcelPackage();
        }

        /// <summary>
        /// Create Cred Data Sheet
        /// </summary>
        /// <param name="isSecure"></param>
        public void AddServiceInfo(bool isSecure)
        {
            ServiceSheet = ExPackage.Workbook.Worksheets.Add("Cred Data");
            var index = 17;
            if (!isSecure)
            {
                table.Columns.Remove("SSN");
                index--;
            }
            ServiceRange = ServiceSheet.Cells["A1"].LoadFromDataTable(table, true);
            ServiceRange.Style.Font.SetFromFont(new System.Drawing.Font("Arial", 10));
            ServiceSheet.Cells[ServiceSheet.Dimension.Address].AutoFitColumns();
            
            ServiceSheet.Column(index).Width = 100;
            index++;
            ServiceSheet.Column(index).Style.Numberformat.Format = "m/d/yyyy";
            index++;
            ServiceSheet.Column(index).Style.Numberformat.Format = "m/d/yyyy";
            index++;
            ServiceSheet.Column(index).Style.Numberformat.Format = "m/d/yyyy";
            index++;
            ServiceSheet.Column(index).Style.Numberformat.Format = "m/d/yyyy";
            index++;
            ServiceSheet.Column(index).Style.Numberformat.Format = "m/d/yyyy";
            index++;
            ServiceSheet.Column(index).Style.Numberformat.Format = "#,##0.00";

        }

        /// <summary>
        /// Get data stream of worksheet
        /// </summary>
        /// <returns></returns>
        public Stream getDataStream()
        {
            var stream = new MemoryStream(ExPackage.GetAsByteArray());
            return stream;
        }

        /// <summary>
        /// Save stream to path [include file name and extension as well]
        /// </summary>
        /// <param name="path"></param>
        public void SaveFile(string path)
        {
            Stream stream = File.Create(path);
            ExPackage.SaveAs(stream);
            stream.Close();
        }

        /// <summary>
        /// Create Pivot Summary Sheet
        /// </summary>
        public void AddPivotInfo()
        {
            PivotSheet = ExPackage.Workbook.Worksheets.Add("Pivot Summary 1");
            var pivotTable = PivotSheet.PivotTables.Add(PivotSheet.Cells["A3"], ServiceRange, "pivTable");

            string stylename = "fontSize";
            var style = ExPackage.Workbook.Styles.CreateNamedStyle(stylename);
            style.Style.Font.SetFromFont(new System.Drawing.Font("Arial", 10));
            //pivotTable.StyleName = stylename;           
            
            pivotTable.ShowHeaders = true;
            pivotTable.FirstHeaderRow = 1;
            pivotTable.FirstDataCol = 1;
            pivotTable.FirstDataRow = 2;
            pivotTable.Compact = false;
            pivotTable.CompactData = false;
            pivotTable.Indent = 0;
            pivotTable.RowGrandTotals = true;
            pivotTable.ColumGrandTotals = true;
            pivotTable.UseAutoFormatting = true;
            pivotTable.ShowMemberPropertyTips = false;
            pivotTable.DataOnRows = false;

            //Row Labels
            pivotTable.RowFields.Add(pivotTable.Fields["ServiceItemName"]);
            pivotTable.RowFields.Add(pivotTable.Fields["OfferingDescription"]);
            
            //pivotTable.DataOnRows = false;
            
            pivotTable.PageFields.Add(pivotTable.Fields["RequestID"]);
            pivotTable.PageFields.Add(pivotTable.Fields["ProviderName"]);
            pivotTable.PageFields.Add(pivotTable.Fields["MarketClientName"]);
            var amountField = pivotTable.Fields["Amount"];

            var dataAmountField = pivotTable.DataFields.Add(amountField);
            dataAmountField.Format = "$ #,##0.00;$ [Red](#,##0.00)";
            dataAmountField.Function = DataFieldFunctions.Sum;
            dataAmountField.Name = "Amount";

            var dataCountField = pivotTable.DataFields.Add(pivotTable.Fields["ServiceItemType"]);
            dataCountField.Function = DataFieldFunctions.Count;
            dataCountField.Name = "Count";            

            (from pf in pivotTable.Fields
             select pf).ToList().ForEach(f =>
             {
                 f.Compact = false;
                 f.Outline = false;                
             });
            
            PivotSheet.Cells["A1:AA100"].Style.Font.SetFromFont(new System.Drawing.Font("Arial", 10));
            PivotSheet.Cells["A1:AA100"].AutoFitColumns();
            PivotSheet.Column(1).Width = 60;

        }

        public void AddSummaryPivotInfo()
        {
            PivotSheet = ExPackage.Workbook.Worksheets.Add("Pivot Summary 2");
            var pivotTable = PivotSheet.PivotTables.Add(PivotSheet.Cells["A3"], ServiceRange, "pivTable");
            pivotTable.ShowHeaders = true;
            pivotTable.FirstHeaderRow = 1;
            pivotTable.FirstDataCol = 1;
            pivotTable.FirstDataRow = 2;

            pivotTable.Compact = false;
            pivotTable.CompactData = false;
            pivotTable.Indent = 0;
            pivotTable.RowGrandTotals = true;
            pivotTable.ColumGrandTotals = true;
            pivotTable.UseAutoFormatting = true;
            pivotTable.ShowMemberPropertyTips = false;
            pivotTable.DataOnRows = false;

            //Row Labels
            pivotTable.RowFields.Add(pivotTable.Fields["BillingClientName"]);
            
            //pivotTable.DataOnRows = false;
            var amountField = pivotTable.Fields["Amount"];   
            //var colAmountField = pivotTable.ColumnFields.Add(amountField);


            pivotTable.PageFields.Add(pivotTable.Fields["RequestID"]);
            pivotTable.PageFields.Add(pivotTable.Fields["ProviderName"]);
            pivotTable.PageFields.Add(pivotTable.Fields["ServiceItemName"]);
            pivotTable.PageFields.Add(pivotTable.Fields["OfferingDescription"]);

            var dataAmountField = pivotTable.DataFields.Add(amountField);
            dataAmountField.Format = "$ #,##0.00;$ [Red](#,##0.00)";
            dataAmountField.Function = DataFieldFunctions.Sum;
            dataAmountField.Name = "Amount";

            var dataCountField = pivotTable.DataFields.Add(pivotTable.Fields["ServiceItemType"]);
            dataCountField.Function = DataFieldFunctions.Count;
            dataCountField.Name = "Count";

            (from pf in pivotTable.Fields
             select pf).ToList().ForEach(f =>
             {
                 f.Compact = false;
                 f.Outline = false;
             });

            PivotSheet.Cells["A1:AA100"].Style.Font.SetFromFont(new System.Drawing.Font("Arial", 10));
            PivotSheet.Cells["A1:AA100"].AutoFitColumns();

        }

        //Create Data Table from List Of Object
        public static System.Data.DataTable CreateDataTable<T>(IEnumerable<T> list)
        {
            Type type = typeof(T);
            var properties = type.GetProperties();

            System.Data.DataTable dataTable = new System.Data.DataTable();
            foreach (PropertyInfo info in properties)
            {
                dataTable.Columns.Add(new System.Data.DataColumn(info.Name, Nullable.GetUnderlyingType(info.PropertyType) ?? info.PropertyType));
            }

            foreach (T entity in list)
            {
                object[] values = new object[properties.Length];
                for (int i = 0; i < properties.Length; i++)
                {
                    values[i] = properties[i].GetValue(entity);
                }

                dataTable.Rows.Add(values);
            }

            return dataTable;
        }
        
    }
}