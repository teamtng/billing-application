﻿using ApertureCVO_BillingApp.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Http;

namespace ApertureCVO_BillingApp.Controllers
{
    /// <summary>
    /// Handle User Validataion for AD Authentication
    /// </summary>
    public class UserController : ApiController
    {

        public IHttpActionResult Get()
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            List<string> userAndGroups = GetUserAndGroups(User);
            if (!UserGroupAccess.CheckForAccess(userAndGroups) || !User.Identity.IsAuthenticated)
                result.Add("userAccess", "READ_ONLY");
            else
                result.Add("userAccess", "WRITE");

            return Ok(result);
        }

        internal static List<string> GetUserAndGroups(IPrincipal user)
        {
            List<string> userAndGroups = new List<string>();
            if (user != null && user.Identity != null)
            {
                userAndGroups.Add(user.Identity.Name);

                WindowsIdentity identity = user.Identity is WindowsIdentity ? user.Identity as WindowsIdentity : null;
                IdentityReferenceCollection groups = null;

                if (identity != null)
                {
                    groups = identity.Groups;
                    if (groups != null)
                    {
                        foreach (var groupId in groups)
                        {
                            var group = groupId.Translate(typeof(NTAccount));
                            userAndGroups.Add(group.ToString());
                        }
                    }
                }
            }

            return userAndGroups;
        }
    }
}