﻿using ApertureCVO_BillingApp.Models;
using ApertureCVO_BillingApp.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ApertureCVO_BillingApp.Controllers
{
    /// <summary>
    /// Handle Deliver Screen
    /// </summary>
    public class FileController : ApiController
    {
        //Get all Deliver Pending Batch
        public HttpResponseMessage Get()
        {
            var db = new DBConnection(User.Identity.Name);
            return new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.Accepted,
                Content = new StringContent(JsonConvert.SerializeObject(db.GetAllUploadCheckBatch()), System.Text.Encoding.UTF8, "application/json")
            };
        }

        //Get Batch Detail
        public HttpResponseMessage Get(int batchID)
        {            
            var db = new DBConnection(User.Identity.Name);
            return new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.Accepted,
                Content = new StringContent(JsonConvert.SerializeObject(db.GetClientUploadDetail(batchID)), System.Text.Encoding.UTF8, "application/json")
            };
        }

        //Get Batch Status - access status.txt file
        public HttpResponseMessage Get(int batchID, string type)
        {
            var db = new DBConnection(User.Identity.Name);
            return new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.Accepted,
                Content = new StringContent(JsonConvert.SerializeObject(db.GetStatus(batchID)), System.Text.Encoding.UTF8, "application/json")
            };
        }

        //Create Deliver Batch
        public HttpResponseMessage Put(int batchID)
        {
            List<string> userAndGroups = UserController.GetUserAndGroups(User);
            if (!UserGroupAccess.CheckForAccess(userAndGroups) || !User.Identity.IsAuthenticated)
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Unauthorized,
                    Content = new StringContent("Error - User does not have permission to call this request", System.Text.Encoding.UTF8, "application/json")
                };

            var db = new DBConnection(User.Identity.Name);
            return new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.Accepted,
                Content = new StringContent(JsonConvert.SerializeObject(db.SaveTempToArchive(batchID)), System.Text.Encoding.UTF8, "application/json")
            };
        }

        //Commit Deliver
        [HttpPost]
        public HttpResponseMessage CommitDeliver(int batchID)
        {
            List<string> userAndGroups = UserController.GetUserAndGroups(User);
            if (!UserGroupAccess.CheckForAccess(userAndGroups) || !User.Identity.IsAuthenticated)
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Unauthorized,
                    Content = new StringContent("Error - User does not have permission to call this request", System.Text.Encoding.UTF8, "application/json")
                };

            var db = new DBConnection(User.Identity.Name);
            try
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Accepted,
                    Content = new StringContent(JsonConvert.SerializeObject(db.DeliverInvoiceToClient(batchID)), System.Text.Encoding.UTF8, "application/json")
                };
            }
            catch (Exception err)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Content = new StringContent(JsonConvert.SerializeObject("Error - " + err.Message), System.Text.Encoding.UTF8, "application/json")
                };
            }
        }

        //Refresh client file [get back deleted file from backup folder]
        [HttpPost]
        public HttpResponseMessage RefreshClientFiles(int batchID, string clientName)
        {
            List<string> userAndGroups = UserController.GetUserAndGroups(User);
            if (!UserGroupAccess.CheckForAccess(userAndGroups) || !User.Identity.IsAuthenticated)
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Unauthorized,
                    Content = new StringContent("Error - User does not have permission to call this request", System.Text.Encoding.UTF8, "application/json")
                };

            try {
                var mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Serve/Client_Files/Batch_" + batchID);
                var clientPath = Path.Combine(mapPath, DBConnection.FixInvalidPath(clientName));
                var backupPath = Path.Combine(clientPath, "Backup");

                List<String> backupFiles = Directory
                       .GetFiles(backupPath, "*.*", SearchOption.AllDirectories).ToList();

                foreach (string file in backupFiles)
                {
                    FileInfo mFile = new FileInfo(file);
                    var toMovePath = Path.Combine(clientPath, mFile.Name);
                    // to remove name collusion
                    mFile.MoveTo(toMovePath);
                }


                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Accepted,
                    Content = new StringContent(JsonConvert.SerializeObject("Success"), System.Text.Encoding.UTF8, "application/json")
                };
            } catch (Exception err)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Content = new StringContent(JsonConvert.SerializeObject("Error - "+ err.Message), System.Text.Encoding.UTF8, "application/json")
                };
            }
        }

        //Handle Upload Custom File
        [HttpPost]
        public async Task<HttpResponseMessage> Upload()
        {
            List<string> userAndGroups = UserController.GetUserAndGroups(User);
            if (!UserGroupAccess.CheckForAccess(userAndGroups) || !User.Identity.IsAuthenticated)
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Unauthorized,
                    Content = new StringContent("Error - User does not have permission to call this request", System.Text.Encoding.UTF8, "application/json")
                };

            var tempPath = HttpContext.Current.Server.MapPath("~/App_Data/" + Guid.NewGuid());
            Directory.CreateDirectory(tempPath);

            var provider = new MultipartFormDataStreamProvider(tempPath);
            var readResult = await Request.Content.ReadAsMultipartAsync(provider);

            // No attachment check
            if (readResult.FileData.Count == 0)
            {
                Directory.Delete(tempPath, true);
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Content = new StringContent(JsonConvert.SerializeObject("Error - Bad Request"), System.Text.Encoding.UTF8, "application/json")
                };
            }

            try {
                var batchID = provider.FormData.GetValues("batchID").FirstOrDefault();
                var clientName = DBConnection.FixInvalidPath(provider.FormData.GetValues("clientName").FirstOrDefault());
                var mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Serve/Client_Files/Batch_" + batchID);
                var count = provider.FileData.Count;
                if (provider.FileData.Count > 0)
                {
                    foreach (var file in provider.FileData)
                    {
                        var fileName = file.Headers.ContentDisposition.FileName;
                        fileName = fileName.Trim('\"');
                        var saveAs = Path.Combine(mapPath, clientName, fileName);
                        File.Move(@file.LocalFileName, @saveAs);
                    }
                }
                Directory.Delete(tempPath, true);
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Accepted,
                    Content = new StringContent(JsonConvert.SerializeObject("Success"), System.Text.Encoding.UTF8, "application/json")
                };
            } catch (Exception err)
            {
                Directory.Delete(tempPath,true);
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Content = new StringContent(JsonConvert.SerializeObject("Error - "+err.Message), System.Text.Encoding.UTF8, "application/json")
                };
            }
        }

        //Delete Deliver Batch
        public HttpResponseMessage Delete(InvoiceObj input)
        {
            List<string> userAndGroups = UserController.GetUserAndGroups(User);
            if (!UserGroupAccess.CheckForAccess(userAndGroups) || !User.Identity.IsAuthenticated)
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Unauthorized,
                    Content = new StringContent("Error - User does not have permission to call this request", System.Text.Encoding.UTF8, "application/json")
                };

            var db = new DBConnection(User.Identity.Name);
            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(db.DeleteInvoice(input.batchID, true)), System.Text.Encoding.UTF8, "application/json")
            };
        }

        //Move File to Backup Folder - Delete from Screen
        public HttpResponseMessage Delete(string filePath)
        {
            List<string> userAndGroups = UserController.GetUserAndGroups(User);
            if (!UserGroupAccess.CheckForAccess(userAndGroups) || !User.Identity.IsAuthenticated)
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Unauthorized,
                    Content = new StringContent("Error - User does not have permission to call this request", System.Text.Encoding.UTF8, "application/json")
                };

            try
            {
                var mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/");
                var toDelete = mapPath+filePath;
                var fileInfo = new FileInfo(toDelete);
                var backupFolder = Path.Combine(Path.GetDirectoryName(toDelete), "Backup", fileInfo.Name);
                fileInfo.MoveTo(backupFolder);
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Accepted,
                    Content = new StringContent(JsonConvert.SerializeObject("Success"), System.Text.Encoding.UTF8, "application/json")
                };
            }
            catch (Exception err)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Content = new StringContent(JsonConvert.SerializeObject("Error - " + err.Message), System.Text.Encoding.UTF8, "application/json")
                };
            }
        }
    }
}