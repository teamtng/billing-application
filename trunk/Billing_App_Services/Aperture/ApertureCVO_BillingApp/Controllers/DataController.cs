﻿using ApertureCVO_BillingApp.Models;
using ApertureCVO_BillingApp.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ApertureCVO_BillingApp.Controllers
{
    /// <summary>
    /// Handle Management Screen
    /// </summary>
    public class DataController : ApiController
    {
        //Get Billing Client List
        public HttpResponseMessage Get(string type)
        {
            var db = new DBConnection(User.Identity.Name);
            if (type == "Profile")
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Accepted,
                    Content = new StringContent(JsonConvert.SerializeObject(db.GetAllClientInfo()), System.Text.Encoding.UTF8, "application/json")
                };
            } else if (type == "Relationship")
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Accepted,
                    Content = new StringContent(JsonConvert.SerializeObject(db.GetRelationshipSearchViewModel()), System.Text.Encoding.UTF8, "application/json")
                };
            } else if(type=="Test") {
                List<string> userAndGroups = UserController.GetUserAndGroups(User);
                if (!UserGroupAccess.CheckForAccess(userAndGroups) || !User.Identity.IsAuthenticated)
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.Unauthorized,
                        Content = new StringContent("Error - User does not have permission to call this request", System.Text.Encoding.UTF8, "application/json")
                    };
                else
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.Accepted,
                        Content = new StringContent("Accepted - "+ User.Identity.Name, System.Text.Encoding.UTF8, "application/json")
                    };
            } else
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Content = new StringContent(JsonConvert.SerializeObject("Bad Request"), System.Text.Encoding.UTF8, "application/json")
                };
            }
        }

        //Get Billing Relationship List
        public HttpResponseMessage Get(string AllianceID, string BillingClientID, string MarketClientID, string ServiceItemTypeID, string ServiceItemID, string Offering, string AccountingType)
        {
            var db = new DBConnection(User.Identity.Name);
            try {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Accepted,
                    //Content = new StringContent(JsonConvert.SerializeObject("TEST"), System.Text.Encoding.UTF8, "application/json")
                    Content = new StringContent(JsonConvert.SerializeObject(db.GetFilteredResult(AllianceID, BillingClientID, MarketClientID, ServiceItemTypeID, ServiceItemID, Offering, AccountingType)), System.Text.Encoding.UTF8, "application/json")
                };
            } catch(Exception err)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Content = new StringContent(JsonConvert.SerializeObject("Bad Request - "+err.Message), System.Text.Encoding.UTF8, "application/json")
                };
            }
        }

        //Get ChargedPrice for Billing Relationship
        [HttpGet]
        public HttpResponseMessage GetBillingPrices(int BillingRelationshipID)
        {
            var db = new DBConnection(User.Identity.Name);
            try
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Accepted,
                    //Content = new StringContent(JsonConvert.SerializeObject("TEST"), System.Text.Encoding.UTF8, "application/json")
                    Content = new StringContent(JsonConvert.SerializeObject(db.GetBillingPrice(BillingRelationshipID)), System.Text.Encoding.UTF8, "application/json")
                };
            }
            catch (Exception err)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Content = new StringContent(JsonConvert.SerializeObject("Bad Request - " + err.Message), System.Text.Encoding.UTF8, "application/json")
                };
            }
        }

        //Update Client Profile
        [HttpPost]
        public HttpResponseMessage UpdateProfile(ClientProfileModel profile)
        {
            List<string> userAndGroups = UserController.GetUserAndGroups(User);
            if (!UserGroupAccess.CheckForAccess(userAndGroups) || !User.Identity.IsAuthenticated)
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Unauthorized,
                    Content = new StringContent("Error - User does not have permission to call this request", System.Text.Encoding.UTF8, "application/json")
                };

            var db = new DBConnection(User.Identity.Name);

            try {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Accepted,
                    Content = new StringContent(JsonConvert.SerializeObject(db.UpdateClientInfo(profile)), System.Text.Encoding.UTF8, "application/json")
                };
            } catch (Exception err)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Content = new StringContent("Error - "+err.Message, System.Text.Encoding.UTF8, "application/json")
                };
            }
        }

        //Update Billing Relationship
        [HttpPost]
        public HttpResponseMessage UpdateRelationship(int relationIndex, BillingRelationshipModel relation)
        {
            List<string> userAndGroups = UserController.GetUserAndGroups(User);
            if (!UserGroupAccess.CheckForAccess(userAndGroups) || !User.Identity.IsAuthenticated)
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Unauthorized,
                    Content = new StringContent("Error - User does not have permission to call this request", System.Text.Encoding.UTF8, "application/json")
                };

            var db = new DBConnection(User.Identity.Name);

            try
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Accepted,
                    Content = new StringContent(JsonConvert.SerializeObject(db.UpdateBillingRelationship(relation)), System.Text.Encoding.UTF8, "application/json")
                };
            }
            catch (Exception err)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Content = new StringContent("Error - " + err.Message, System.Text.Encoding.UTF8, "application/json")
                };
            }
        }

        //Update ChargedPrice
        [HttpPost]
        public HttpResponseMessage UpdateBillingPrice(int BillingRelationshipID, decimal ListPrice, decimal ChargedPrice=-9999)
        {

            List<string> userAndGroups = UserController.GetUserAndGroups(User);
            if (!UserGroupAccess.CheckForAccess(userAndGroups) || !User.Identity.IsAuthenticated)
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Unauthorized,
                    Content = new StringContent("Error - User does not have permission to call this request", System.Text.Encoding.UTF8, "application/json")
                };

            var db = new DBConnection(User.Identity.Name);

            try
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Accepted,
                    Content = new StringContent(JsonConvert.SerializeObject(db.UpdateBillingPrice(BillingRelationshipID, ListPrice, ChargedPrice)), System.Text.Encoding.UTF8, "application/json")
                };
            }
            catch (Exception err)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Content = new StringContent("Error - " + err.Message, System.Text.Encoding.UTF8, "application/json")
                };
            }
        }
    }
}
