﻿using ApertureCVO_BillingApp.Models;
using ApertureCVO_BillingApp.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ApertureCVO_BillingApp.Controllers
{
    /// <summary>
    /// Handle Pending Page
    /// </summary>
    public class ReportController : ApiController
    {
        //Get All Reviewing Batch
        public HttpResponseMessage Get()
        {
            var db = new DBConnection(User.Identity.Name);
            return new HttpResponseMessage()
            {
                //Content = new StringContent(db.GetProductDetail(conn,37910777,"02/01/2016","03/01/2016"), System.Text.Encoding.UTF8, "application/json")
                Content = new StringContent(JsonConvert.SerializeObject(db.GetAllReviewBatch()), System.Text.Encoding.UTF8, "application/json")
            };
        }

        //Get BatchDetail - Populate Batch Screen
        public HttpResponseMessage Get(int batchID)
        {
            var db = new DBConnection(User.Identity.Name);
            return new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.Accepted,
                Content = new StringContent(JsonConvert.SerializeObject(db.GetBatchDetail(batchID)), System.Text.Encoding.UTF8, "application/json")
            };
        }

        //Get Invoice Detail
        public HttpResponseMessage Get(int batchID, int clientID, string rowType)
        {
            var db = new DBConnection(User.Identity.Name);
            return new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.Accepted,
                Content = new StringContent(JsonConvert.SerializeObject(db.GetInvoiceDetail(batchID, clientID, rowType)), System.Text.Encoding.UTF8, "application/json")
            };
        }

        //Get Client Detail [Hierachy]
        public HttpResponseMessage Get(int batchID, int clientID, int allianceID, string type, string parenttype)
        {
            var db = new DBConnection(User.Identity.Name);
            return new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.Accepted,
                Content = new StringContent(JsonConvert.SerializeObject(db.GetClientDetail(batchID, clientID, allianceID, type, parenttype)), System.Text.Encoding.UTF8, "application/json")
            };
        }

        //Get Summary Report List From Date Range
        public HttpResponseMessage Get(string dateFrom, string dateTo)
        {
            var db = new DBConnection(User.Identity.Name);
            return new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.Accepted,
                Content = new StringContent(JsonConvert.SerializeObject(db.GetSummaryList(dateFrom, dateTo)), System.Text.Encoding.UTF8, "application/json")
            };
        }

        //Download Sample Excel File
        public HttpResponseMessage Get(int batchID, int clientID, string clientType, int serviceTypeID, string serviceType)
        {
            var db = new DBConnection(User.Identity.Name);
            try
            {
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = db.GetServiceItemTypeDetail_CSV(batchID, clientID, clientType, serviceTypeID, serviceType);
                result.Content = new StreamContent(stream);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                string type = clientType == "M" ? "MarketClient" : clientType == "B" ? "BillingClient" : "Alliance";
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {

                    FileName = serviceType + " Detail_" + type + "#" + clientID + ".xlsx"
                };
                return result;

            }
            catch (Exception err)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Content = new StringContent(JsonConvert.SerializeObject("Error - " + err.Message), System.Text.Encoding.UTF8, "application/json")
                };
            }
        }

        //Download Sample By Market Excel File
        [HttpGet]
        public HttpResponseMessage GetMarketCSV(int batchID, int clientID, string clientType, int MarketClientID, string MarketClientName)
        {
            var db = new DBConnection(User.Identity.Name);
            try
            {
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = db.GetMarketDetail_CSV(batchID, clientID, clientType, MarketClientID, MarketClientName);
                result.Content = new StreamContent(stream);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                string type = clientType == "M" ? "MarketClient" : clientType == "B" ? "BillingClient" : "Alliance";
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {

                    FileName = MarketClientName + " Detail_" + type + "#" + clientID + ".xlsx"
                };
                return result;

            }
            catch (Exception err)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Content = new StringContent(JsonConvert.SerializeObject("Error - " + err.Message), System.Text.Encoding.UTF8, "application/json")
                };
            }
        }


        //Update Break | Combine status
        public HttpResponseMessage Post(InvoiceObj input)
        {
            List<string> userAndGroups = UserController.GetUserAndGroups(User);
            if (!UserGroupAccess.CheckForAccess(userAndGroups) || !User.Identity.IsAuthenticated)
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Unauthorized,
                    Content = new StringContent("Error - User does not have permission to call this request", System.Text.Encoding.UTF8, "application/json")
                };

            var db = new DBConnection(User.Identity.Name);
            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(db.BreakOrCombineInvoice(input.batchID, input.clientID, input.type, input.isCombine, input.AllianceID)), System.Text.Encoding.UTF8, "application/json")
            };
        }

        //Handle Client Groupping or Display By Flag
        [HttpPost]
        public HttpResponseMessage ChangeGroupingOrDisplay(int clientID, string clientType, string fieldChange, int fieldValue)
        {
            List<string> userAndGroups = UserController.GetUserAndGroups(User);
            if (!UserGroupAccess.CheckForAccess(userAndGroups) || !User.Identity.IsAuthenticated)
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Unauthorized,
                    Content = new StringContent("Error - User does not have permission to call this request", System.Text.Encoding.UTF8, "application/json")
                };

            var db = new DBConnection(User.Identity.Name);
            try {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Accepted,
                    Content = new StringContent(JsonConvert.SerializeObject(db.ChangeGroupingOrDisplay(clientID, clientType, fieldChange, fieldValue)), System.Text.Encoding.UTF8, "application/json")
                };
            } catch (Exception err)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Content = new StringContent(JsonConvert.SerializeObject("Error - "+err.Message), System.Text.Encoding.UTF8, "application/json")
                };
            }
        }

        //Create New Batch
        public HttpResponseMessage Put(DateObj input)
        {
            List<string> userAndGroups = UserController.GetUserAndGroups(User);
            if (!UserGroupAccess.CheckForAccess(userAndGroups) || !User.Identity.IsAuthenticated)
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Unauthorized,
                    Content = new StringContent("Error - User does not have permission to call this request", System.Text.Encoding.UTF8, "application/json")
                };

            var db = new DBConnection(User.Identity.Name);
            return new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.Accepted,
                Content = new StringContent(JsonConvert.SerializeObject(db.GenerateBatch(input.dateFrom, input.dateTo)), System.Text.Encoding.UTF8, "application/json")
            };
        }

        //Delete Batch
        public HttpResponseMessage Delete(InvoiceObj input)
        {
            List<string> userAndGroups = UserController.GetUserAndGroups(User);
            if (!UserGroupAccess.CheckForAccess(userAndGroups) || !User.Identity.IsAuthenticated)
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Unauthorized,
                    Content = new StringContent("Error - User does not have permission to call this request", System.Text.Encoding.UTF8, "application/json")
                };

            var db = new DBConnection(User.Identity.Name);
            return new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.Accepted,
                Content = new StringContent(JsonConvert.SerializeObject(db.DeleteInvoice(input.batchID, false)), System.Text.Encoding.UTF8, "application/json")
            };
        }
    }
}
