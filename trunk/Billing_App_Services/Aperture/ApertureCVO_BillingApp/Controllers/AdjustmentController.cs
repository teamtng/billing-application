﻿using ApertureCVO_BillingApp.Models;
using ApertureCVO_BillingApp.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ApertureCVO_BillingApp.Controllers
{
    /// <summary>
    /// Handle Adjustment Add / Remove
    /// </summary>
    public class AdjustmentController : ApiController
    {
        public HttpResponseMessage Put(AdjustmentObj input)
        {
            List<string> userAndGroups = UserController.GetUserAndGroups(User);
            if (!UserGroupAccess.CheckForAccess(userAndGroups) || !User.Identity.IsAuthenticated)
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Unauthorized,
                    Content = new StringContent("Error - User does not have permission to call this request", System.Text.Encoding.UTF8, "application/json")
                };

            var db = new DBConnection(User.Identity.Name);
            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(db.AddNewAdjustment(input.batchID, input.clientID, input.clientType, input.serviceQuantity, input.serviceDescription, input.serviceRate)), System.Text.Encoding.UTF8, "application/json")
            };
        }

        public HttpResponseMessage Delete(AdjustmentObj input)
        {
            List<string> userAndGroups = UserController.GetUserAndGroups(User);
            if (!UserGroupAccess.CheckForAccess(userAndGroups) || !User.Identity.IsAuthenticated)
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Unauthorized,
                    Content = new StringContent("Error - User does not have permission to call this request", System.Text.Encoding.UTF8, "application/json")
                };

            var db = new DBConnection(User.Identity.Name);
            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(db.DeleteAdjustment(input.batchID, input.clientID, input.clientType, input.serviceDescription)), System.Text.Encoding.UTF8, "application/json")
            };

        }
    }
}