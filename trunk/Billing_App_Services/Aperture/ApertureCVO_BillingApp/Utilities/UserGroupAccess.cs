﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace ApertureCVO_BillingApp.Utilities
{
    public class UserGroupAccess
    {
        //private static ReadOnlyDictionary<string, bool> hasAddUpdateAccess = new ReadOnlyDictionary<string, bool>(new Dictionary<string, bool>()
        //{
        //    { "corp\\sa_billing",               true },
        //    { "corp\\contract developers",      true },
        //    { "corp\\finance",                  true },
        //    { "corp\\tech – qa",                true },
        //    { "corp\\qkhong",                   true },
        //    { "corp\\nken",                     true },
        //    { "corp\\ckuo",                     true },
        //    { "corp\\cburton",                  true },
        //    { "corp\\kemery",                   true },
        //    { "corp\\kpotts",                   true },
        //    { "corp\\kmiles",                   true }
        //});

        //internal static bool CheckForAccess(List<string> userAndGroupList)
        //{
        //    foreach (string userOrGroup in userAndGroupList)
        //    {
        //        if (hasAddUpdateAccess.ContainsKey(userOrGroup.ToLower()) && hasAddUpdateAccess[userOrGroup.ToLower()])
        //            return true;
        //    }

        //    return false;
        //}

        internal static bool CheckForAccess(List<string> userAndGroupList)
        {
            string userGroupAccess = WebConfigurationManager.AppSettings["UserGroupAccess"];
            if (userGroupAccess != null && userGroupAccess.Length > 0)
            {
                userGroupAccess = userGroupAccess.ToLower();
                foreach (string userOrGroup in userAndGroupList)
                {
                    if (userGroupAccess.Contains(userOrGroup.ToLower()))
                        return true;
                }
            }

            //foreach (string userOrGroup in userAndGroupList)
            //{
            //if (hasAddUpdateAccess.ContainsKey(userOrGroup.ToLower()) && hasAddUpdateAccess[userOrGroup.ToLower()])
            //    return true;
            //}

            return false;
        }
    }
}