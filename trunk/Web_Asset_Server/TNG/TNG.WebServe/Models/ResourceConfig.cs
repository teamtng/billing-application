﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TNG.WebServe.Models
{
    public class ResourceConfig
    {
        public Dictionary<string, string> html { get; set; }
        public Dictionary<string, string> js { get; set; }
        public Dictionary<string, string> css { get; set; }
        public Dictionary<string, string> other { get; set; }
    }
}