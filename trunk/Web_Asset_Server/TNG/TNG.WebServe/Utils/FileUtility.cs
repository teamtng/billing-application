﻿using System;
using System.IO;

namespace TNG.WebServe.Utils
{
    public class FileUtility
    {
        public static string GetFileContent(string path)
        {
            string content = "";

            if (File.Exists(path))
                content = File.ReadAllText(path);
            else
                throw new FileNotFoundException();
            
            return content;
        }

        public static FileStream GetFileContentStream(string path)
        {
            FileStream content = null;

            if (File.Exists(path))
                content = File.OpenRead(path);
            else
                throw new FileNotFoundException();

            return content;
        }
    }
}