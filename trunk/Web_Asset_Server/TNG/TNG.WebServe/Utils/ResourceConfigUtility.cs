﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TNG.WebServe.Models;

namespace TNG.WebServe.Utils
{
    public class ResourceConfigUtility
    {
        public static ResourceConfig LoadConfiguration(string configFilePath)
        {
            string configContent = FileUtility.GetFileContent(configFilePath);

            ResourceConfig config = JsonConvert.DeserializeObject<ResourceConfig>(configContent);
            return config;
        }

        public static bool ContainsKey(string key, ResourceConfig config)
        {
            if (config.html.ContainsKey(key)
                || config.css.ContainsKey(key)
                || config.js.ContainsKey(key))
            {
                return true;
            }

            return false;
        }
    }
}