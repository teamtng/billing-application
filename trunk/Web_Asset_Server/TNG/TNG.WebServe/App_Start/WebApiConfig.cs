﻿using System.Web.Http;
using System.Web.Http.Tracing;

namespace TNG.WebServe
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            SystemDiagnosticsTraceWriter traceWriter = config.EnableSystemDiagnosticsTracing();
            traceWriter.IsVerbose = true;
            traceWriter.MinimumLevel = TraceLevel.Debug;

            // Web API routes
            config.MapHttpAttributeRoutes();

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "app/{controller}/{action}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);
            
            config.Routes.MapHttpRoute(
                name: "WebApp",
                routeTemplate: "app/{*anything}",
                defaults: new { controller = "Asset", action = "resource", type = "html" }
            );

            config.Routes.MapHttpRoute(
                name: "AppCSS",
                routeTemplate: "css/{*src}",
                defaults: new { controller = "Asset", action = "resource", type = "css", src = @"(.*?)\.(.*?)" }
            );

            config.Routes.MapHttpRoute(
                name: "AppJS",
                routeTemplate: "js/{*src}",
                defaults: new { controller = "Asset", action = "resource", type = "js", src = @"(.*?)\.(.*?)" }
            );

            config.Routes.MapHttpRoute(
                name: "AppContent",
                routeTemplate: "asset/{*src}",
                defaults: new { controller = "Asset", action = "resource", type = "asset", src = @"(.*?)\.(.*?)" }
            );
        }
    }
}
