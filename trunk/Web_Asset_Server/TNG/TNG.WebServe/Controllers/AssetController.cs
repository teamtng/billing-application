﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using System.Web.Http;
using System.Web.Http.Tracing;
using TNG.WebServe.Models;
using TNG.WebServe.Utils;

namespace TNG.WebServe.Controllers
{
    public class AssetController : ApiController
    {
        /// <summary>
        /// Reads into application memory the configuration for a designated application name. Uses web application caching to help prevent
        /// continuous file IO if reading configuration from a file.
        /// </summary>
        /// <param name="appName">The application name as web application defined as part of the URL.</param>
        /// <returns><seealso cref="ResourceConfig"/>The configuration of the requested application.</returns>
        private ResourceConfig GetResourceConfig(string appName)
        {
            // Cache resources so file IO is not continuously happening.
            Cache contextCache = HttpRuntime.Cache;

            ResourceConfig resourceConfig = null;

            if (contextCache[appName + "ResourcePaths"] == null
                || !(contextCache[appName + "ResourcePaths"] is ResourceConfig))
            {
                string configPath = "C:\\WebServe\\" + appName + "ResourcePathConfig.json";
                string configContent = FileUtility.GetFileContent(configPath);
                
                Configuration.Services.GetTraceWriter().Debug(Request, "AssetController", "Getting Resource Configuration From File" + DateTime.Now.ToString());
                resourceConfig = ResourceConfigUtility.LoadConfiguration(configPath);

                // Cache the resource paths.
                Task.Run(() =>
                {
                    contextCache.Insert(appName + "ResourcePaths", resourceConfig, new CacheDependency(configPath), Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0, 0));
                }).ConfigureAwait(false);
            }
            else
            {
                Configuration.Services.GetTraceWriter().Debug(Request, "AssetController", "Getting Resource Configuration From Cache" + DateTime.Now.ToString());
                resourceConfig = contextCache[appName + "ResourcePaths"] as ResourceConfig;
            }
            
            return resourceConfig;
        }

        /// <summary>
        /// Wraps around a file reading utility to read in content from a file and cache into the web application context. 
        /// <seealso cref="AssetController.GetResourceConfig(string)"/> should be invoked with correct application name in order
        /// for this method to work correctly. Content is read in string format.
        /// </summary>
        /// <param name="resourcePath">The file's path to be read in as a string.</param>
        /// <param name="appName">The application name as web application defined as part of the URL.</param>
        /// <returns>The contents of a file in string format.</returns>
        private string GetResourceContent(string resourcePath, string appName)
        {
            // Cache resources so file IO is not continuously happening.
            Cache contextCache = HttpRuntime.Cache;
            string content = "";

            if (contextCache[appName + "_" + resourcePath] == null || !(contextCache[appName + "_" + resourcePath] is string))
            {
                content = FileUtility.GetFileContent(resourcePath);

                // Cache the resource file content. Resource cached with key structured as {app name}_{resource path} ex: kb_C:\\pathtoresource
                Task.Run(() =>
                {
                    contextCache.Insert(appName + "_" + resourcePath, content, new CacheDependency(resourcePath), Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0, 0));
                }).ConfigureAwait(false);
            }
            else
                content = contextCache[appName + "_" + resourcePath] as string;
            
            return content;
        }

        /// <summary>
        /// Wraps around a file reading utility to read in content froma file, no caching occurs. <seealso cref="AssetController.GetResourceConfig(string)"/> 
        /// should be invoked with correct application name in order for this method to work correctly. Content is read in binary format using the <seealso cref="FileStream"/>
        /// object type.
        /// </summary>
        /// <param name="resourcePath">The file's path to be read in.</param>
        /// <returns>The contents of a file given as <seealso cref="FileStream"/></returns>
        private FileStream GetResourceContentStream(string resourcePath)
        {
            FileStream content = FileUtility.GetFileContentStream(resourcePath);
            return content;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestUri"></param>
        /// <returns></returns>
        private string GetRequestResource(Uri requestUri)
        {
            string content = "";
            string[] pathSegments = requestUri.Segments;

            // The URL path is as follows /app/{app name}/ which means that the URL needs at least three segments.
            if (pathSegments.Length <= 2 && pathSegments[1].Length > 0)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Internal Server Error"));
            }
            else {
                string appName = pathSegments[2].Replace("/", "");

                try
                {
                    ResourceConfig resourceConfig = GetResourceConfig(appName);

                    string modifiedPath = requestUri.AbsolutePath.Replace(pathSegments[0] + pathSegments[1] + appName + "/", "");

                    Dictionary<string, string> resourceDict = null;
                    if (pathSegments[1].Equals("js/"))
                        resourceDict = resourceConfig.js;
                    else if (pathSegments[1].Equals("css/"))
                        resourceDict = resourceConfig.css;
                    else
                        resourceDict = resourceConfig.html;

                    if (modifiedPath.Length == 0)
                        modifiedPath = "index";

                    if (!resourceDict.ContainsKey(modifiedPath))
                    {
                        if (modifiedPath.EndsWith("/"))
                            modifiedPath = modifiedPath.TrimEnd(new char[] { '/' });
                        else
                            modifiedPath = modifiedPath + "/";
                    }

                    content = GetResourceContent(resourceDict[modifiedPath], appName);
                }
                catch (Exception)
                {
                    throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, "Resource not found."));
                }
            }
            
            return content;
        }

        private FileStream GetRequestResourceContent(Uri requestUri)
        {
            FileStream content = null;

            string[] pathSegments = requestUri.Segments;

            // The URL path is as follows /asset/{app name}/ which means that the URL needs at least three segments.
            if (pathSegments.Length <= 2 && pathSegments[1].Length > 0)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Internal Server Error"));
            }
            else {
                string appName = pathSegments[2].Replace("/", "");

                try
                {
                    ResourceConfig resourceConfig = GetResourceConfig(appName);
                    string modifiedPath = requestUri.AbsolutePath.Replace(pathSegments[0] + pathSegments[1] + appName + "/", "");
                    Dictionary<string, string> resourceDict = resourceConfig.other;

                    content = GetResourceContentStream(resourceDict[modifiedPath]);
                }
                catch (Exception)
                {
                    throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, "Resource not found."));
                }
            }
            
            return content;
        }

        [HttpGet]
        [ActionName("resource")]
        public IHttpActionResult GetResource(string type)
        {
            Configuration.Services.GetTraceWriter().Debug(Request, "AssetController", "START GetResource: " + DateTime.Now.ToString());
            IHttpActionResult result;

            try
            {
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);

                if (type.Equals("html") || type.Equals("js") || type.Equals("css"))
                {
                    string content = GetRequestResource(Request.RequestUri);
                    response.Content = new StringContent(content, Encoding.UTF8, MimeMapping.GetMimeMapping("temp." + type));
                }
                else
                {
                    Uri requestUri = Request.RequestUri;
                    string[] pathSegments = requestUri.Segments;
                    string fileName = pathSegments[pathSegments.Length - 1];
                    var stream = GetRequestResourceContent(requestUri);
                    response.Content = new StreamContent(stream);
                    response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(MimeMapping.GetMimeMapping(fileName));
                }

                response.RequestMessage = Request;
                response.Headers.CacheControl = new System.Net.Http.Headers.CacheControlHeaderValue()
                {
                    Private = true,
                    MaxAge = new TimeSpan(1, 0, 0, 0)
                };

                result = ResponseMessage(response);
            }
            catch (HttpResponseException e)
            {
                string path = @"~/html/error500.html";
                if (e.Response.StatusCode == HttpStatusCode.NotFound)
                    path = @"~/html/error404.html";
                path = HttpContext.Current.Server.MapPath(path);

                string content = "";
                try
                {
                    content = FileUtility.GetFileContent(path);
                }
                catch (FileNotFoundException)
                {
                    content = "<html><head><title>Server Error</title></head><body><p>We broke.</p></body></html>";
                }

                HttpResponseMessage response = new HttpResponseMessage(e.Response.StatusCode);
                response.Content = new StringContent(content);
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("text/html");
                response.RequestMessage = Request;

                result = ResponseMessage(response);
            }

            Configuration.Services.GetTraceWriter().Debug(Request, "AssetController", "END GetResource: " + DateTime.Now.ToString());
            return result;
        }
    }
}