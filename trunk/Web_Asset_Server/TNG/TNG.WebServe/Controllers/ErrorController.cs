﻿using System.Web.Mvc;

namespace TNG.WebServe.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index()
        {
            return View("Error");
        }

        public ActionResult Error()
        {
            Response.StatusCode = 500;
            return new FilePathResult(System.Web.HttpContext.Current.Server.MapPath(@"~/html/error500.html"), "text/html");
        }

        public ActionResult Error404()
        {
            Response.StatusCode = 404;
            return new FilePathResult(System.Web.HttpContext.Current.Server.MapPath(@"~/html/error404.html"), "text/html");
        }
    }
}