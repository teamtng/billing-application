/*
Script generated by Aqua Data Studio 16.0.9 on Jun-17-2016 10:30:30 AM
Database: null
Schema: <All Schemas>
*/

DROP TABLE "dbo"."TempLkRequest1"
GO

CREATE TABLE "dbo"."TempLkRequest1"  ( 
	"RequestID"    	int NULL,
	"ProductTypeID"	int NULL,
	"ProductType"  	varchar(50) NULL,
	"ProviderID"   	int NULL,
	"ProviderName" 	varchar(128) NULL,
	"SSN"          	varchar(11) NULL,
	"MedicaidNo"   	varchar(25) NULL,
	"RestartID"    	int NULL,
	"Comment"      	varchar(255) NULL 
	)
GO

